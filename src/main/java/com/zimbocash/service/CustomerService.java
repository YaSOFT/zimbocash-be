package com.zimbocash.service;

import com.zimbocash.commons.CustomResponseDto;
import com.zimbocash.commons.InterceptorAuthService;
import com.zimbocash.config.security.AES256Cipher;
import com.zimbocash.config.security.jwt.JwtProvider;
import com.zimbocash.dao.CustomerDomainBannedRepository;
import com.zimbocash.dao.CustomerIPRepository;
import com.zimbocash.dao.CustomerRepository;
import com.zimbocash.dao.LoginAuditRepository;
import com.zimbocash.enums.MissedTransactionType;
import com.zimbocash.enums.Stage;
import com.zimbocash.enums.UserStatus;
import com.zimbocash.external.AmazonSES;
import com.zimbocash.external.PwnedClient;
import com.zimbocash.external.TelegramClient;
import com.zimbocash.facade.ISocketFacade;
import com.zimbocash.model.UserData;
import com.zimbocash.model.ZCLoginResponse;
import com.zimbocash.model.ZCResponse;
import com.zimbocash.model.dtos.ClickDto;
import com.zimbocash.model.dtos.FeedbackDto;
import com.zimbocash.model.dtos.SignUpDto;
import com.zimbocash.model.dtos.SocketBodyDto;
import com.zimbocash.model.entity.Customer;
import com.zimbocash.model.entity.CustomerConfirmation;
import com.zimbocash.model.entity.CustomerIP;
import com.zimbocash.model.entity.CustomerReward;
import com.zimbocash.model.entity.CustomerTransaction;
import com.zimbocash.model.entity.DomainBanned;
import com.zimbocash.model.entity.LoginAudit;
import com.zimbocash.model.entity.Review;
import com.zimbocash.model.entity.Telegram;
import com.zimbocash.util.ChatBot;
import com.zimbocash.util.Constants;
import com.zimbocash.util.PasswordUtil;
import com.zimbocash.util.StatusCode;
import com.zimbocash.util.TronServiceUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.zimbocash.util.Constants.ATTEMPTS_EXCEED;
import static com.zimbocash.util.Constants.INTERNAL_ERROR;
import static com.zimbocash.util.Constants.NOT_ALLOWED_TO_PERFORM_THIS_ACTION;
import static com.zimbocash.util.Constants.NOT_FOUND;
import static com.zimbocash.util.Constants.PHONE_NUMBER_EXIST_ALREADY;
import static com.zimbocash.util.Constants.PHONE_NUMBER_VERIFIED_ALREADY;
import static com.zimbocash.util.Constants.SIMPLE_DATE_FORMAT;
import static com.zimbocash.util.Constants.SUCCESS;
import static com.zimbocash.util.Constants.TRON_NOT_FOUND;
import static com.zimbocash.util.Constants.USER_NOT_FOUND;
import static com.zimbocash.util.Constants.WRONG_PASSWORD;
import static com.zimbocash.util.Constants.WRONG_PASSWORD_PATTERN;
import static com.zimbocash.util.StatusCode.EXPIRED_PASSWORD;
import static com.zimbocash.util.StatusCode.WRONG_CREDENTIALS;
import static com.zimbocash.util.StatusCode.WRONG_PATTERN;

@Service
@RequiredArgsConstructor
@Slf4j
//todo error handling for db and any service and pause execution and return
public class CustomerService {
    private static final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    public static String botToken;
    static volatile Map<String, Object> resetPassword = new LinkedHashMap<>();
    static volatile Map<String, Object> wrongPassword = new LinkedHashMap<>();
    static volatile Map<String, JSONObject> tempEmailCodes = new ConcurrentHashMap<>();
    private final TronServiceUtil tronServiceUtil;
    private final PwnedClient pwnedClient;
    private final TelegramClient telegramClient;
    private final ChatBot chatBot;
    private final AmazonSES amazonSES;
    private final JwtProvider jwtProvider;
    private final AES256Cipher aes256Cipher;
    private final CustomerRepository customerRepository;
    private final CustomerIPRepository customerIPRepository;
    private final LoginAuditRepository loginAuditRepository;
    private final CustomerDomainBannedRepository customerDomainBannedRepository;
    private final ISocketFacade socketFacade;
    @Value("${tron.tokenID}")
    String assetName;
    @Value("${trade-gov.file-name}")
    String fileName;

    public ZCResponse signUp(SignUpDto signUpDto) {
        String email = signUpDto.getEmail().trim().toLowerCase();
        String partLog = "POST /signUp() method -> email,\t";
        ZCResponse response = ZCResponse.builder().build();
        if (isBannedDomain(email)) {
            response.setStatusCode(117);
            log.info("Response status code: " + response.getStatusCode());
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message("banned").build());
            return response;
        }
        Customer customer = Customer.builder()
                .email(email)
                .build();
        if (signUpDto.getPhone() != null) {
            customer.setPhone(signUpDto.getPhone());
        } else {
            customer.setIdNumber(signUpDto.getIdNumber());
        }
        try {
            prepareCustomer(customer, signUpDto.getReferral());
            customerRepository.save(customer);
            //member_id already created for user in prepareCustomer
            String tempCode = PasswordUtil.getRandomPassword(50);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tempCode", tempCode);
            jsonObject.put("dttm", LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
            jsonObject.put("attempts", 0);
            jsonObject.put("signUp", true);
            tempEmailCodes.put(email, jsonObject);
            amazonSES.sendPasswordConfirmAccount(email, tempCode);
            response.setStatus(SUCCESS);
            response.setStatusCode(200);
            response.setSuccess(true);
        } catch (Exception e) {
            response.setStatusCode(StatusCode.INTERNAL_ERROR);
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(e.getMessage()).build());
            response.setStatus(Constants.FAIL);
            response.setObject(e.getMessage());
            return response;
        }
        log.info(partLog.concat("Response status code: {}"), email, response.getStatusCode());
        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(customer.getMemberID()).message("success").build());
        return response;
    }

    public ZCResponse signIn(String email, String password) {
        email = email.toLowerCase();
        ZCLoginResponse response = ZCLoginResponse.ZCRLBuilder().build();
        String partLog = "POST /signIn() method -> email {}";
        int wrongAttempts = 0;
        boolean canTryLogin = true;
        String attemptsKey = email + "_attempts";
        try {
            if (isBannedDomain(email)) {
                response.setStatusCode(117);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message("banned").build());
                log.info("Response status code: " + response.getStatusCode());
                return response;
            }
            //we can't use cache at this level, we don't have yet the member_id
            Customer customer = customerRepository.getCustomerByEmail(email);
            if (customer != null) {
                if (customer.getPassword() == null) {
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(customer.getMemberID()).message("password is null").build());
                    return forgetPassword(email);
                }
                String fullName = customer.getFirstName() + " " + customer.getLastName();
                if (!isContainInExcludedNames(fullName)) {
                    //if the user have password as null, means first time for old user or
                    if (!isPasswordPatternMatch(password)) {
                        response.setStatusCode(WRONG_PATTERN);
                        response.setStatus(WRONG_PASSWORD_PATTERN);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(customer.getMemberID()).message(WRONG_PASSWORD_PATTERN).build());
                        return response;
                    }
                    if (wrongPassword.containsKey(email)) {
                        String resetPass = String.valueOf(wrongPassword.get(email));
                        Date resetPasswordDate = SIMPLE_DATE_FORMAT.parse(resetPass);
                        resetPasswordDate.setTime(resetPasswordDate.getTime() + (3600 * 24 * 1000));

                        if (resetPasswordDate.before(new Date())) {
                            wrongPassword.remove(email);
                            wrongPassword.remove(attemptsKey);
                            canTryLogin = true;
                        } else {
                            if ((int) wrongPassword.get(attemptsKey) >= 2) {
                                canTryLogin = false;
                            }
                        }
                    }
                    if (canTryLogin) {
                        if (aes256Cipher.matches(password.trim(), customer.getPassword())) {
                            //continue work
                            customer = updateCustomerDetailsLogin(customer);
                            String memberID = customer.getMemberID();
                            String refreshToken = jwtProvider.generateToken(memberID, customer.isAdmin(), customer.isManager(), true);
                            String accessToken = jwtProvider.generateToken(memberID, customer.isAdmin(), customer.isManager(), false);
                            response.setAuthResult(UserData.builder()
                                    .accessToken(accessToken)
                                    .refreshToken(refreshToken)
                                    .build());
                            customer.setRefreshToken(refreshToken);
                            initToken(memberID, refreshToken);
                            wrongPassword.remove(email);
                            wrongPassword.remove(attemptsKey);
                            sendTelegramMessage(customer);

                            response.setStatusCode(200);
                            response.setObject(customer);
                            response.setStatus(SUCCESS);
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(customer.getMemberID()).message(SUCCESS).build());
                            response.setSuccess(true);
                        } else {
                            log.error(partLog + ", wrong password ", email);
                            if (!wrongPassword.containsKey(email)) {
                                wrongPassword.put(email, Constants.SIMPLE_DATE_FORMAT.format(new Date()));
                                wrongPassword.put(attemptsKey, wrongAttempts);
                            } else {
                                wrongAttempts = ((int) wrongPassword.get(attemptsKey)) + 1;
                                wrongPassword.replace(attemptsKey, wrongAttempts);
                            }
                            //wrong pass
                            wrongPassword.put(email, Constants.SIMPLE_DATE_FORMAT.format(new Date()));
                            response.setStatusCode(WRONG_CREDENTIALS);
                            response.setStatus(WRONG_PASSWORD);
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(customer.getMemberID()).message(WRONG_PASSWORD).build());
                        }
                    } else {
                        response.setStatusCode(StatusCode.LOCKED_ACCOUNT);
                        response.setStatus(Constants.FAIL);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(customer.getMemberID()).message(Constants.FAIL).build());
                    }
                } else {
                    customer.setEnabled(false);
                    customerRepository.save(customer);
                    response.setStatusCode(StatusCode.USER_NOT_ALLOWED);
                    response.setStatus("USER_NOT_ALLOWED");
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(customer.getMemberID()).message("USER_NOT_ALLOWED").build());
                }
                LoginAudit loginAudit = LoginAudit.builder().
                        customerId(customer.getId())
                        .loginStatus(response.getStatusCode())
                        .dateRecorded(SIMPLE_DATE_FORMAT.parse(SIMPLE_DATE_FORMAT.format(new Date())))
                        .build();
                loginAuditRepository.saveAndFlush(loginAudit);
            } else {
                //no user found
                log.error(partLog + " , user not found", email);
                response.setStatusCode(StatusCode.USER_NOT_FOUND);
                response.setStatus(USER_NOT_FOUND);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(USER_NOT_FOUND).build());
            }
        } catch (Exception e) {
            log.error(partLog + ", error -> {}", email, e.getMessage());
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(e.getMessage()).build());
        }

        log.info(partLog.concat(", Response status code: {}"), email, response.getStatusCode());
        return response;
    }

    private boolean isBannedDomain(String email) {
        email = email.toLowerCase();
        List<DomainBanned> allBannedDomains = customerDomainBannedRepository.findAll();
        String domain = email.substring(email.indexOf('@') + 1);
        List<String> domainWords = Arrays.asList(domain.split("\\."));
        for (DomainBanned domainBanned : allBannedDomains) {
            if (domainBanned.isWord()) {
                if (domainWords.contains(domainBanned.getPattern())) {
                    return true;
                }
            } else {
                if (domain.equalsIgnoreCase(domainBanned.getPattern()))
                    return true;
            }
        }
        return false;
    }


    public ZCResponse forgetPassword(String email) {
        String partLog = "POST /forgetPassword() method -> email {}";
        ZCResponse response = ZCResponse.builder().build();
        try {
            email = email.toLowerCase();
            Customer customer = customerRepository.getCustomerByEmail(email);
            if (customer != null) {
                JSONObject jsonObject = tempEmailCodes.get(email);
                String tempCode = PasswordUtil.getRandomPassword(50);
                if (jsonObject != null) {
                    int attempts = jsonObject.getInt("attempts");
                    LocalDateTime localDateTimeOfTempCode = LocalDateTime.ofEpochSecond(jsonObject.getLong("dttm"), 0, ZoneOffset.UTC);
                    if (LocalDateTime.now().minusHours(24).isBefore(localDateTimeOfTempCode)) {
                        if (attempts > 2) {
                            response.setStatusCode(305);
                            response.setStatus(ATTEMPTS_EXCEED);
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(ATTEMPTS_EXCEED).build());
                        } else {
                            jsonObject.put("attempts", ++attempts);
                            if (customer.getPassword() != null) {
                                amazonSES.sendPasswordResetEmail(email, jsonObject.getString("tempCode"));
                            } else {
                                amazonSES.sendPasswordConfirmAccount(email, jsonObject.getString("tempCode"));
                            }
                            tempEmailCodes.put(email, jsonObject);
                            response.setStatus(SUCCESS);
                            response.setStatusCode(200);
                            response.setSuccess(true);
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(SUCCESS).build());
                        }
                    } else {
                        //if the date after the first one then create new values
                        jsonObject.put("attempts", 1);
                        jsonObject.put("tempCode", tempCode);
                        jsonObject.put("dttm", LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
                        jsonObject.put("signUp", false);
                        tempEmailCodes.put(email, jsonObject);
                        if (customer.getPassword() != null) {
                            amazonSES.sendPasswordResetEmail(email, tempCode);
                        } else {
                            amazonSES.sendPasswordConfirmAccount(email, tempCode);
                        }
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(SUCCESS).build());
                        response.setStatus(SUCCESS);
                        response.setStatusCode(200);
                        response.setSuccess(true);
                    }
                } else {
                    //didn't yet make any forgetPassword
                    jsonObject = new JSONObject();
                    jsonObject.put("tempCode", tempCode);
                    jsonObject.put("dttm", LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
                    jsonObject.put("attempts", 1);
                    jsonObject.put("signUp", false);
                    tempEmailCodes.put(email, jsonObject);
                    if (customer.getPassword() != null) {
                        amazonSES.sendPasswordResetEmail(email, tempCode);
                    } else {
                        amazonSES.sendPasswordConfirmAccount(email, tempCode);
                    }
                    response.setStatus(SUCCESS);
                    response.setStatusCode(200);
                    response.setSuccess(true);
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(SUCCESS).build());
                }
                if (customer.getPassword() == null) {
                    response.setStatusCode(206);
                }
            } else {
                response.setStatus(USER_NOT_FOUND);
                response.setStatusCode(119);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(USER_NOT_FOUND).build());
            }
        } catch (Exception e) {
            response.setObject(e.getMessage());
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(e.getMessage()).build());
        }
        log.info("Response status code: " + response.getStatusCode());
        return response;
    }

    private Customer updateCustomerDetailsLogin(Customer customer) {
        UserStatus userStatus = customer.getStatus();
        if (userStatus != null) {
            if (userStatus.equals(UserStatus.PENDING)) {
                //update customer
                customer.setStatus(UserStatus.VERIFIED);
                customer = customerRepository.save(customer);
                //send email
                amazonSES.sendWelcomeEmail(customer.getEmail(), customer.getMemberID());
            } else {
                customer = customerRepository.save(customer);
            }
        }
        return customer;
    }

    private void prepareCustomer(Customer customer, String referral) throws ParseException {
        customer.setDateRecorded(SIMPLE_DATE_FORMAT.parse(SIMPLE_DATE_FORMAT.format(new Date())));
        customer.setMemberID(PasswordUtil.getMemberID());
        CustomerReward customerReward = CustomerReward.builder().build();
        customer.setCustomerReward(customerReward);
        if (referral != null) {
            Customer customerWhoReferralYou = customerRepository.findCustomerByMemberID(referral).orElse(null);
            if (customerWhoReferralYou != null) {
                customerReward.setWhoReferralYou(referral);
                customer.setCustomerReward(customerReward);
            }
        }
    }

    //acting as verify and rest password too
    public ZCResponse resetPassword(String email, String password, String newPassword) {
        email = email.toLowerCase();
        String partLog = "POST /resetPassword() method -> email {}, ";
        ZCLoginResponse response = ZCLoginResponse.ZCRLBuilder().build();
        boolean isReseated = false;
        try {
            if (checkPwned(newPassword)) {
                if (checkResetPasswordCount(email, resetPassword)) {
                    int updates = customerRepository
                            .updateStatusAndPasswordByEmailAndPassword(UserStatus.VERIFIED, aes256Cipher.encode(newPassword), email, aes256Cipher.encode(password));
                    if (updates > 0) {
                        isReseated = true;
                    } else {
                        //if wrong credentials then we retrieve the password in memory for user and we make same updates
                        JSONObject jsonObject = tempEmailCodes.get(email);
                        if (jsonObject != null) {
                            LocalDateTime localDateTimeOfTempCode = LocalDateTime.ofEpochSecond(jsonObject.getLong("dttm"), 0, ZoneOffset.UTC);
                            if (LocalDateTime.now().minusHours(24).isAfter(localDateTimeOfTempCode)) {
                                //EXPIRED code
                                response.setStatusCode(EXPIRED_PASSWORD);
                                response.setStatus(Constants.FAIL);
                            } else {
                                String tmpCode = jsonObject.getString("tempCode");
                                //match the password (i.e code) with existing one
                                if (tmpCode.equals(password)) {
                                    updates = customerRepository.updateStatusAndPasswordByEmail(UserStatus.VERIFIED, aes256Cipher.encode(newPassword), email);
                                    if (updates > 0) {
                                        isReseated = true;
                                    }
                                } else {
                                    //code not matching, wrong credentials
                                    response.setStatusCode(WRONG_CREDENTIALS);
                                    response.setStatus(WRONG_PASSWORD);
                                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(WRONG_PASSWORD + " -> different tempCode").build());
                                }
                            }
                        } else {
                            //there is no in-memory code, directly wrong credentials
                            response.setStatusCode(WRONG_CREDENTIALS);
                            response.setStatus(WRONG_PASSWORD);
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(WRONG_PASSWORD + " -> there is no in-memory code").build());
                        }
                    }
                } else {
                    //user now should call forget password
                    response.setStatusCode(StatusCode.LOCKED_ACCOUNT);
                    response.setStatus(Constants.FAIL);
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(Constants.FAIL + " -> user now should call forget password").build());
                }
            } else {
                response.setStatusCode(StatusCode.LEAKED_PASSWORD);
                response.setStatus(Constants.FAIL);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(Constants.FAIL + " -> pawned issue").build());
            }
            if (isReseated) {
                //remove the tempcode from memory
                Customer customer = customerRepository.getCustomerByEmail(email);
                JSONObject jsonObject = tempEmailCodes.get(email);
                if (jsonObject != null) {
                    boolean isSignup = jsonObject.getBoolean("signUp");
                    if (isSignup) {
                        amazonSES.sendWelcomeEmail(customer.getEmail(), customer.getMemberID());
                    } else {
                        amazonSES.sendPasswordChanged(email);
                    }
                } else {
                    amazonSES.sendPasswordChanged(email);
                }
                String memberID = customer.getMemberID();
                String refreshToken = jwtProvider.generateToken(memberID, customer.isAdmin(), customer.isManager(), true);
                initToken(memberID, refreshToken);
                tempEmailCodes.remove(email);
                resetPassword.remove(email);
                resetPassword.remove(email + "_attempts");
                response.setAuthResult(UserData.builder()
                        .accessToken(jwtProvider.generateToken(memberID, customer.isAdmin(), customer.isManager(), false))
                        .refreshToken(refreshToken)
                        .build());
                response.setStatus(SUCCESS);
                response.setStatusCode(200);
                response.setSuccess(true);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(SUCCESS).build());
                response.setObject(customer);
            }
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(e.getMessage()).build());
            log.error(e.getMessage());
            response.setObject(e.getMessage());
        }
        log.info(partLog.concat("Response status code: {}"), email, response.getStatusCode());
        return response;
    }

    public ZCResponse updateCustomerAndIdentity(String phone, String memberID) {
        String partLog = "POST /update() method";
        log.info("updateCustomer() method");
        ZCResponse response = ZCResponse.builder().build();

        try {
            Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
            if (customer != null) {
                if (!customer.isPhoneVerified()) {
                    //an error will be occured if the phone is exist
                    customer.setPhone(phone);
                    //update
                    customer = customerRepository.save(customer);
                    response.setObject(customer);
                    response.setStatus(SUCCESS);
                    response.setSuccess(true);
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(SUCCESS).build());
                    response.setStatusCode(200);
                } else {
                    //Phone already verified
                    response.setStatus(PHONE_NUMBER_VERIFIED_ALREADY);
                    response.setStatusCode(120);
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(PHONE_NUMBER_VERIFIED_ALREADY).build());
                }

            } else {
                response.setStatus(USER_NOT_FOUND);
                response.setStatusCode(119);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(USER_NOT_FOUND).build());
            }
        } catch (Exception e) {
            response.setStatus(PHONE_NUMBER_EXIST_ALREADY);
            response.setStatusCode(118);
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
        }
        log.info("Response status code: " + response.getStatusCode());
        return response;
    }

    //each click will be handled here, and update reward_click field.
    //
    public ZCResponse submitClick(ClickDto clickDto) {
        String partLog = "POST /submitClick() method";
        ZCResponse response = ZCResponse.builder().build();
        try {
            if (checkIP(clickDto.getIp())) {
                Customer customer = customerRepository.findCustomerByMemberID(clickDto.getReferral()).orElse(null);
                if (customer != null) {
                    CustomerReward customerReward = customer.getCustomerReward();
                    if (customerReward != null && customerReward.getLinkClicks() < 101) {
                        double reward = 0;
                        //0.1% from signup reward
                        switch (getStage(null)) {
                            case SIX:
                                reward = 4;
                                break;
                            case SEVEN:
                                reward = 4;
                                break;
                            case EIGHT: //from january 2021
                                reward = 0.4;
                                break;
                            default:
                                break;
                        }
                        customerReward.setClickRewardPending(customerReward.getClickRewardPending() + reward);
                        customerReward.setLinkClicks(customerReward.getLinkClicks() + 1);
                        customer.setCustomerReward(customerReward);
                        //update
                        customerRepository.save(customer);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(customer.getMemberID()).message(SUCCESS).build());
                        response.setStatus(SUCCESS);
                        response.setStatusCode(200);
                        response.setSuccess(true);

                    } else {
                        response.setStatus(Constants.LIMIT_REACHED);
                        response.setStatusCode(122);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(customer.getMemberID()).message(Constants.LIMIT_REACHED).build());
                    }
                } else {
                    response.setStatus(NOT_FOUND);
                    response.setStatusCode(104);
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(clickDto.getReferral()).message(NOT_FOUND).build());
                }
            } else {
                response.setStatus("IP_USED");
                response.setStatusCode(121);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(clickDto.getReferral()).message("IP_USED").build());
            }
        } catch (Exception e) {
            response.setObject(e.getMessage());
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(clickDto.getReferral()).message(e.getMessage()).build());
        }
        log.info("Response status code: " + response.getStatusCode());

        return response;
    }

    //todo IP to be figured with customer in audit system, add cache
    private boolean checkIP(String ip) {
        try {
            long count = customerIPRepository.countByIp(ip);
            if (count == 0) {
                customerIPRepository.save(CustomerIP.builder().ip(ip).build());
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    public ZCResponse receiveClickRewards(String memberID) {
        log.info("receiveClickRewards() method");
        String partLog = "POST /receiveClickRewards() method";
        ZCResponse response = ZCResponse.builder().build();
        try {
            Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
            if (customer != null) {
                CustomerConfirmation customerConfirmation = customer.getCustomerConfirmation();
                if (customerConfirmation != null && customerConfirmation.isConfirmed()) {
                    CustomerReward customerReward = customer.getCustomerReward();
                    CustomerTransaction customerTransaction = customer.getCustomerTransaction();
                    if (customerReward != null && customerTransaction != null) {
                        String tronAccountAddress = customerTransaction.getTronAccountAddress();
                        double clickRewardPending = customerReward.getClickRewardPending();
                        if (clickRewardPending != 0 && tronAccountAddress != null) {
                            String txID = tronServiceUtil.issueOrTransfer(null, tronAccountAddress, clickRewardPending, assetName);
                            if (txID != null) {
                                customerReward.setClickRewardPending(0);
                                customer.setCustomerReward(customerReward);
                                //update
                                customerRepository.save(customer);
                                response.setStatus(SUCCESS);
                                response.setStatusCode(200);
                                response.setSuccess(true);
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(SUCCESS).build());
                            } else {
                                tronServiceUtil.saveMissedTransaction(MissedTransactionType.HOT_WALLET.getStatus(),
                                        tronAccountAddress, clickRewardPending, memberID);
                                response.setStatus(INTERNAL_ERROR);
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("txID is null").build());
                                response.setStatusCode(500);
                                response.setSuccess(false);
                            }
                        } else {
                            response.setStatus(TRON_NOT_FOUND);
                            response.setStatusCode(100);
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("clickRewardPending = " + clickRewardPending + "\t, tronAccountAddress = " + tronAccountAddress).build());
                        }
                    } else {
                        response.setStatus(TRON_NOT_FOUND);
                        response.setStatusCode(102);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("customerReward = " + customerReward + "\t, customerTransaction = " + customerTransaction).build());
                    }
                } else {
                    response.setStatus(Constants.NEEDS_ID_UPLOAD);
                    response.setStatusCode(123);
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("customerConfirmation = " + customerConfirmation + "\t, or it's not confirmed").build());
                }
            } else {
                response.setStatusCode(101);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(NOT_FOUND).build());
            }
        } catch (Exception e) {
            response.setObject(e.getMessage());
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
        }

        log.info("Response status code: " + response.getStatusCode());
        return response;
    }


    public Stage getStage(Date date) {
        if (date == null) {
            date = new Date();
        }
        try {
            if (date.before(formatter.parse("2020-07-01"))) {
                return Stage.SIX;
            }
            if (date.before(formatter.parse("2020-10-01"))) {
                return Stage.SEVEN;
            }
            if (date.after(formatter.parse("2021-01-01"))) {
                return Stage.EIGHT;
            }
        } catch (ParseException ignored) {
        }
        return Stage.NINE;
    }

    //get tron balance real in blockchain
    public ZCResponse getTronBalance(String address) {
        String partLog = "GET /get() method, getTronBalance";
        ZCResponse response = ZCResponse.builder().build();
        try {
            response.setObject(tronServiceUtil.getBalance(address, assetName));
            response.setStatus(SUCCESS);
            response.setStatusCode(200);
            response.setSuccess(true);
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(address).message(SUCCESS).build());
        } catch (Exception e) {
            response.setObject(e.getMessage());
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(address).message(e.getMessage()).build());
        }
        log.info("Response status code: " + response.getStatusCode());
        return response;
    }

    public ZCResponse isMemberIDExist(String memberID) {
        String partLog = "GET /checkmemberid method,";

        ZCResponse response = ZCResponse.builder().build();
        try {
            Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
            if (customer != null) {
                CustomerConfirmation customerConfirmation = customer.getCustomerConfirmation();
                Map<String, Object> objectMapResponse = new HashMap<>();
                objectMapResponse.put("confirmed", customerConfirmation != null && customerConfirmation.isConfirmed());
                response.setObject(objectMapResponse);
                response.setStatus(SUCCESS);
                response.setStatusCode(200);
                response.setSuccess(true);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(SUCCESS).build());
            } else {
                response.setStatus(NOT_FOUND);
                response.setStatusCode(101);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(NOT_FOUND).build());
            }
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
            response.setObject(e.getMessage());
        }
        return response;
    }

    public ZCResponse sendFeedback(String memberID, FeedbackDto feedback) {
        InterceptorAuthService.evaluate(memberID);
        String partLog = "POST /feedback method,";
        ZCResponse response = ZCResponse.builder().build();
        try {
            Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
            if (customer != null) {
                customer.setReview(Review.builder()
                        .rating(feedback.getRating())
                        .review(feedback.getReview())
                        .build());
                customerRepository.save(customer);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(SUCCESS).build());
                response.setStatus(SUCCESS);
                response.setStatusCode(200);
                response.setSuccess(true);
            } else {
                response.setStatus(NOT_FOUND);
                response.setStatusCode(101);
            }
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
            if (e instanceof ResponseStatusException) {
                throw e;
            }
            response.setObject(e.getMessage());
        }
        return response;
    }

    public CustomResponseDto refreshToken(String refreshToken, String memberID) {
        String partLog = "PUT /refreshAuth method,";
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();
        //the token used only for USERS
        String refreshTokenNew = jwtProvider.generateToken(memberID, false, false, true);
        customerRepository.updateRefreshTokenByRefreshTokenAndMemberId(refreshTokenNew, refreshToken, memberID);
        customResponseDto.setSuccess(true);
        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(SUCCESS).build());
        customResponseDto.setStatusCode(200);
        customResponseDto.setJwtRefreshToken(refreshTokenNew);
        customResponseDto.setJwtAccessToken(jwtProvider.generateToken(memberID, false, false, false));
        return customResponseDto;
    }

    private void initToken(String memberID, String refreshToken) {
        customerRepository.updateRefreshTokenByMemberId(refreshToken, memberID);
    }

    public ZCResponse getCustomerDetailsByMemberID(String memberID) {
        String partLog = "GET /getCustomer method";
        InterceptorAuthService.evaluate(memberID);
        ZCResponse response = ZCResponse.builder().build();
        try {
            Customer customer = customerRepository.getCustomerByMemberID(memberID);
            response.setObject(customer);
            response.setStatusCode(200);
            response.setSuccess(true);
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(SUCCESS).build());
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
            if (e instanceof ResponseStatusException) {
                throw e;
            }
            response.setObject(e.getMessage());
        }
        return response;
    }

    public ZCResponse getChatbotResponse(String message) {
        String partLog = "GET /chatbot method";
        ZCResponse response = ZCResponse.builder().build();
        response.setStatus(SUCCESS);
        message = chatBot.getChatbotResponse(message);
        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(message).message(SUCCESS).build());
        response.setStatusCode(200);
        response.setSuccess(true);
        response.setObject(message);
        return response;
    }

    private boolean isContainInExcludedNames(String name) {
        boolean contains = false;
        try {
            Path path = Paths.get("/tmp/" + fileName);
            if (path.toFile().exists()) {
                String result = new String(Files.readAllBytes(path));
                if (!result.isEmpty()) {
                    contains = Arrays.asList(result.split("\\r?\\n")).contains(name.trim().toLowerCase());
                }
            }
        } catch (IOException e) {
            socketFacade.send(SocketBodyDto.builder().api("isContainInExcludedNames").memberIDOrEmail(name).message(e.getMessage()).build());
            log.error(e.getMessage());
        }
        return contains;
    }

    private boolean checkResetPasswordCount(String email, Map<String, Object> password) {
        email = email.toLowerCase();
        int attempts = 0;
        String attemptsKey = email + "_attempts";

        try {
            if (!password.containsKey(email)) {
                password.put(email, Constants.SIMPLE_DATE_FORMAT.format(new Date()));
                password.put(attemptsKey, attempts);
                socketFacade.send(SocketBodyDto.builder().api("checkResetPasswordCount").memberIDOrEmail(email).message("can reset").build());
                return true;
            }

            String resetPass = String.valueOf(password.get(email));
            Date resetPasswordDate = SIMPLE_DATE_FORMAT.parse(resetPass);
            resetPasswordDate.setTime(resetPasswordDate.getTime() + (3600 * 24 * 1000));

            if (resetPasswordDate.before(new Date())) {
                password.remove(email);
                password.remove(attemptsKey);
                socketFacade.send(SocketBodyDto.builder().api("checkResetPasswordCount").memberIDOrEmail(email).message("can reset").build());
                return true;
            }

            attempts = ((int) password.get(attemptsKey)) + 1;
            password.replace(attemptsKey, attempts);
            socketFacade.send(SocketBodyDto.builder().api("checkResetPasswordCount").memberIDOrEmail(email).message("attempts " + attempts).build());

        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api("checkResetPasswordCount").memberIDOrEmail(email).message(e.getMessage()).build());
            log.error(e.getMessage());
        }
        return attempts <= 2;
    }

    private boolean checkPwned(String password) {
        List<String> foundResult;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            byte[] encoded = digest.digest(
                    password.getBytes(StandardCharsets.UTF_8));
            String passwordHex = PasswordUtil.bytesToHex(encoded);
            String response = pwnedClient.isPasswordPwned(passwordHex.substring(0, 5));
            if (response != null) {
                foundResult = Arrays.asList(response.split("\\r?\\n"));
                for (String s : foundResult) {
                    int ind = s.indexOf(":");
                    String subString = "";
                    if (ind != -1) {
                        subString = s.substring(0, ind);
                    }
                    if (passwordHex.toLowerCase().equals((passwordHex.substring(0, 5) + subString).toLowerCase())) {
                        return false;
                    }
                }
            }
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api("checkPwned").memberIDOrEmail(password).message(e.getMessage()).build());
            log.error(e.getMessage());
        }
        return true;
    }

    public Object moveWallet(String memberID, String receiverMemberID) {
        String partLog = "POST /moveWallet method";
        log.info("moveWallet() method -> from member {} to member {}", memberID, receiverMemberID);
        ZCResponse response = ZCResponse.builder().build();
        Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
        if (customer != null && customer.isPhoneVerified() && customer.isIdFailed() && customer.isEnabled() && customer.isNewUser()
                && !customer.isAdmin() && !customer.isManager()) {
            CustomerTransaction customerTransaction = customer.getCustomerTransaction();
            if (customerTransaction != null) {
                double realBalance = tronServiceUtil.getBalance(customerTransaction.getTronAccountAddress(), assetName);
                if (realBalance > 0) {
                    Customer customerReceiver = customerRepository.findCustomerByMemberID(receiverMemberID).orElse(null);
                    if (customerReceiver != null && customerReceiver.isPhoneVerified() && !customerReceiver.isAdmin() && !customerReceiver.isManager()
                            && !customerReceiver.isIdFailed()) {
                        if (customerReceiver.getMovedZASHToYou() != 0) {
                            response.setStatus(NOT_ALLOWED_TO_PERFORM_THIS_ACTION);
                            response.setStatusCode(406);
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("receiver " + receiverMemberID + " already received").build());
                            return response;
                        }
                        CustomerTransaction customerTransactionReceiver = customerReceiver.getCustomerTransaction();
                        CustomerConfirmation customerConfirmationReceiver = customerReceiver.getCustomerConfirmation();
                        if (customerTransactionReceiver != null && customerConfirmationReceiver != null && customerConfirmationReceiver.isConfirmed()) {
                            double amount = realBalance / 1_000_000;
                            String txID = tronServiceUtil.issueOrTransfer(customerTransaction.getTronPublicKey(),
                                    customerTransactionReceiver.getTronAccountAddress(), amount, assetName);
                            if (txID != null) {
                                customerReceiver.setMovedZASHToYou(realBalance);
                                customerRepository.save(customerReceiver);

                                //block account after moving zash
                                customer.setBalance(0);
                                customer.setEnabled(false);
                                customerRepository.save(customer);
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(SUCCESS).build());
                                response.setStatus(SUCCESS);
                                response.setSuccess(true);
                                response.setStatusCode(200);
                                amazonSES.sendZashMovedToSender(customer.getEmail(), customerReceiver.getMemberID(), amount, txID);
                                amazonSES.sendZashMovedToReceiver(customerReceiver.getEmail(), customerReceiver.getFirstName(), amount, txID);
                            } else {
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("txID is null").build());
                                tronServiceUtil.saveMissedTransaction(customerTransaction.getTronPublicKey(),
                                        customerTransactionReceiver.getTronAccountAddress(), amount, receiverMemberID);
                                response.setStatus(INTERNAL_ERROR);
                                response.setStatusCode(500);
                            }
                        } else {
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("receiver " + receiverMemberID + " either have tron null or not confirmed yet").build());
                        }
                    } else {
                        response.setStatus(NOT_ALLOWED_TO_PERFORM_THIS_ACTION);
                        response.setStatusCode(405);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("the receiver " + receiverMemberID + " either not confirmed or admin or manager or failed id or phone not verified").build());
                    }
                } else {
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("balance is 0").build());
                }
            } else {
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("tronaccount is null").build());
            }
        } else {
            response.setStatus(NOT_ALLOWED_TO_PERFORM_THIS_ACTION);
            response.setStatusCode(407);
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("the sender either not failed or old user or admin or manager or phone not verified").build());
        }
        return response;
    }

    private void sendTelegramMessage(Customer customer) {
        Telegram telegram = customer.getTelegram();
        if (telegram != null && telegram.isVerified()) {
            telegramClient.sendMessage(botToken,
                    String.valueOf(telegram.getChatID()),
                    "Just confirming you’ve logged into your ZIMBOCASH account. If this was not you, please change your password.");
        }
    }

    private boolean isPasswordPatternMatch(String password) {
        Pattern pattern = Pattern.compile("^(?=.*[0-9])(?=.*[a-zA-Z])(.*?).{12,}$");
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }
}
