package com.zimbocash.service;

import com.google.common.collect.Sets;
import com.zimbocash.dao.CustomerRepository;
import com.zimbocash.enums.DocumentType;
import com.zimbocash.enums.TestType;
import com.zimbocash.external.AWSHttp;
import com.zimbocash.external.AmazonSES;
import com.zimbocash.facade.ISocketFacade;
import com.zimbocash.model.ZCResponse;
import com.zimbocash.model.dtos.CustomerDto;
import com.zimbocash.model.dtos.SocketBodyDto;
import com.zimbocash.model.entity.ConfirmationWord;
import com.zimbocash.model.entity.Customer;
import com.zimbocash.model.entity.CustomerConfirmation;
import com.zimbocash.model.entity.CustomerTransaction;
import com.zimbocash.scheduler.TriggerSchedulers;
import com.zimbocash.util.Constants;
import com.zimbocash.util.PasswordUtil;
import com.zimbocash.util.TronServiceUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.zimbocash.util.Constants.ACCOUNT_SUSPENDED_PERMANENTLY;
import static com.zimbocash.util.Constants.ACCOUNT_SUSPENDED_PERMANENTLY_WITH_MOVE_ZASH_ALLOWED;
import static com.zimbocash.util.Constants.ALREADY_CONFIRMED;
import static com.zimbocash.util.Constants.ATTEMPTS_EXCEED;
import static com.zimbocash.util.Constants.FAIL;
import static com.zimbocash.util.Constants.ID_UPLOAD_SUCCESS;
import static com.zimbocash.util.Constants.INTERNAL_ERROR;
import static com.zimbocash.util.Constants.NEEDS_ID_UPLOAD;
import static com.zimbocash.util.Constants.NEEDS_LIVENESS;
import static com.zimbocash.util.Constants.NEEDS_MANUAL_REVIEW;
import static com.zimbocash.util.Constants.SIMPLE_DATE_FORMAT;
import static com.zimbocash.util.Constants.SIMPLE_LESS_DATE_FORMAT;
import static com.zimbocash.util.Constants.SUCCESS;
import static com.zimbocash.util.Constants.USER_NOT_FOUND;

@Service
@RequiredArgsConstructor
@Slf4j
public class KYCService {
    private static final Map<String, JSONObject> updateDetails = new HashMap<>();
    private static final Map<String, JSONObject> updateIDDetails = new HashMap<>();
    private static final Map<String, JSONObject> getUrl = new HashMap<>();
    public static volatile String hotWalletAddress;
    private final CustomerRepository customerRepository;
    private final AWSHttp awsHttp;
    private final AmazonSES amazonSES;
    private final TransactionService transactionService;
    private final TronServiceUtil tronServiceUtil;
    private final TriggerSchedulers schedulers;
    private final ISocketFacade socketFacade;
    @Value("${tron.tokenID}")
    String assetName;
    @Value("${email.port}")
    String emailPort;
    @Value("${email.host}")
    String emailHost;

    public ZCResponse getImageUrl(String memberID, DocumentType documentType) {
        log.info("getImageUrl() method");
        String partLog = "GET /api/validation/getimageurl method";
        ZCResponse response = ZCResponse.builder().build();
        try {
            JSONObject jsonObject = getUrl.get(memberID);
            long attempts = 1;
            if (jsonObject != null) {
                attempts = jsonObject.getLong("attempts");
                LocalDateTime localDateTimeOfUpdateDetails = LocalDateTime.ofEpochSecond(jsonObject.getLong("dttm"), 0, ZoneOffset.UTC);
                if (LocalDateTime.now().minusHours(24).isBefore(localDateTimeOfUpdateDetails)) {
                    if (attempts > 3) {
                        response.setStatusCode(305);
                        response.setStatus(ATTEMPTS_EXCEED);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(ATTEMPTS_EXCEED).build());
                        return response;
                    } else {
                        jsonObject.put("attempts", ++attempts);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(String.valueOf(attempts)).build());
                    }
                } else {
                    jsonObject.put("attempts", 1);
                    jsonObject.put("dttm", LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("TIME AFTER ATTEMPTS 1").build());
                }
            } else {
                jsonObject = new JSONObject();
                jsonObject.put("dttm", LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
                jsonObject.put("attempts", attempts);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("url not found , new ATTEMPTS 1 and time").build());
            }
            getUrl.put(memberID, jsonObject);

            Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);

            if (customer != null) {
                Map<String, Object> objectMapResponse = new HashMap<>();
                String idNumber = Optional.ofNullable(customer.getIdNumber()).orElse("").replaceAll("\\s*", "");
                if (!customer.isPhoneVerified() || idNumber.isEmpty() || customer.getFirstName() == null || customer.getLastName() == null) {
                    response.setStatus(FAIL);
                    response.setStatusCode(400);
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("customer doesn't match criterias -> phone not verified or idnumber|name|lastname null").build());
                    return response;
                }

                CustomerConfirmation customerConfirmation = customer.getCustomerConfirmation();
                if (customerConfirmation != null) {
                    if (!customerConfirmation.isConfirmed() && !customer.isIdFailed()) {
                        objectMapResponse.put("selfieImageURL", awsHttp.generateUploadPreSignedUrl(idNumber
                                .replaceAll("(CIT|REG|ALIEN)*", "").replaceAll("\\s*", "") + "_liveness.jpeg"));
                        objectMapResponse.put("idImageURl", awsHttp.generateUploadPreSignedUrl(idNumber
                                .replaceAll("(CIT|REG|ALIEN)*", "").replaceAll("\\s*", "") + (documentType.getType()
                                .equalsIgnoreCase(DocumentType.ID_CARD.getType()) ? "" : documentType.getType()) + "_front.jpeg"));
                    } else {
                        response.setStatus(ALREADY_CONFIRMED);
                        response.setStatusCode(201);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("customer confirmed or id_failed").build());
                        return response;
                    }

                } else {
                    objectMapResponse.put("idImageURl", awsHttp.generateUploadPreSignedUrl(idNumber
                            .replaceAll("(CIT|REG|ALIEN)*", "").replaceAll("\\s*", "") + (documentType.getType()
                            .equalsIgnoreCase(DocumentType.ID_CARD.getType()) ? "" : documentType.getType()) + "_front.jpeg"));
                }
                response.setObject(objectMapResponse);
                response.setStatus(SUCCESS);
                response.setStatusCode(200);
                response.setSuccess(true);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(SUCCESS).build());

            } else {
                response.setStatus(USER_NOT_FOUND);
                response.setStatusCode(119);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(USER_NOT_FOUND).build());
            }
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
            response.setStatusCode(105);
            log.error(e.getMessage());
        }
        log.info("Response status code: " + response.getStatusCode());
        return response;
    }

    //1-1

    /**
     * @implSpec update customer KYC, when user try to upload his id, intercepted by this method, fail on 3 wrong attempts and lock updating for 24 hours
     * , the phone should be verified to let user access here
     */
    public ZCResponse updateCustomerKYC(CustomerDto customerDto, String memberID) {
        log.info("updateCustomerKYC() method " + memberID);
        ZCResponse response = ZCResponse.builder().build();
        String partLog = "POST /api/validation/updatekyc method";
        try {
            JSONObject jsonObject = updateDetails.get(memberID);
            long attempts = 1;
            if (jsonObject != null) {
                attempts = jsonObject.getLong("attempts");
                LocalDateTime localDateTimeOfUpdateDetails = LocalDateTime.ofEpochSecond(jsonObject.getLong("dttm"), 0, ZoneOffset.UTC);
                if (LocalDateTime.now().minusHours(24).isBefore(localDateTimeOfUpdateDetails)) {
                    if (attempts > 2) {
                        response.setStatusCode(305);
                        response.setStatus(ATTEMPTS_EXCEED);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(ATTEMPTS_EXCEED).build());
                        return response;
                    } else {
                        jsonObject.put("attempts", ++attempts);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(String.valueOf(attempts)).build());
                    }
                } else {
                    jsonObject.put("attempts", 1);
                    jsonObject.put("dttm", LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("time is after new attempts 1 and time").build());
                }
            } else {
                jsonObject = new JSONObject();
                jsonObject.put("dttm", LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
                jsonObject.put("attempts", attempts);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("new attempts 1 and time").build());
            }
            updateDetails.put(memberID, jsonObject);

            Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
            if (customer != null) {
                if (customer.isPhoneVerified()) {
                    Map<String, Object> objectMapResponse = new HashMap<>();
                    String idNumber = customerDto.getIdNumber().replaceAll("\\s*", "").toUpperCase();
                    customer.setFirstName(customerDto.getFirstName());
                    customer.setLastName(customerDto.getLastName());
                    customer.setIdNumber(idNumber);
                    customer.setBirthDate(SIMPLE_LESS_DATE_FORMAT.parse(customerDto.getBirthDate()));
                    customerRepository.save(customer);
                    objectMapResponse.put("idImageURl", awsHttp.generateUploadPreSignedUrl(idNumber
                            .replaceAll("(CIT|REG|ALIEN)*", "").replaceAll("\\s*", "") + "_front.jpeg"));
                    response.setObject(objectMapResponse);
                    schedulers.reminder(memberID, customer.getEmail(), customerDto.getFirstName(), customerDto.getLastName(), customer.getPhone());
                    response.setStatus(SUCCESS);
                    response.setStatusCode(200);
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(SUCCESS).build());
                    response.setSuccess(true);
                } else {
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("phone not verified").build());
                }
            } else {
                response.setStatus(USER_NOT_FOUND);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(USER_NOT_FOUND).build());
                response.setStatusCode(119);
            }

        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
            response.setObject(e.getMessage());
        }
        log.info("Response status code: " + response.getStatusCode());
        return response;
    }

    /**
     * @implSpec check user match information from id picture with information of user in db, if not match capture again one other time, maxAttempts = 2,
     * , if match then go to selfie, otherwise we put id on hold for 5 days
     */
    // 1-2
    public ZCResponse uploadId(String memberID, DocumentType documentType) {
        String partLog = "POST /api/validation/rekognition/id method";
        //max 2 attempts then push to liveness, either fail or success
        log.info("uploadId() method");
        ZCResponse response = ZCResponse.builder().build();
        Map<String, Object> objectMapResponse = new HashMap<>();
        try {
            JSONObject jsonObject = updateIDDetails.get(memberID);
            long attempts = 1;
            boolean doReset = false;
            if (jsonObject != null) {
                attempts = jsonObject.getLong("attempts");
                if (attempts > 1) {
                    //max of attempts exceed then FE should show the liveness screen
                    response.setStatusCode(305);
                    response.setStatus(ATTEMPTS_EXCEED);
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(ATTEMPTS_EXCEED).build());
                    return response;
                } else {
                    jsonObject.put("attempts", ++attempts);
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(String.valueOf(attempts)).build());
                }
            } else {
                doReset = true;
            }

            Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
            if (customer != null) {
                String idNumber = Optional.ofNullable(customer.getIdNumber()).orElse("").replaceAll("\\s*", "");
                if (idNumber.isEmpty() || customer.getFirstName() == null || customer.getLastName() == null) {
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("idNumber or firtname or lastname is null").build());
                    return response;
                }
                CustomerConfirmation customerConfirmation = customer.getCustomerConfirmation();
                final String[] word = {PasswordUtil.getLivenessWord()};
                Date dateRecorded = SIMPLE_DATE_FORMAT.parse(SIMPLE_DATE_FORMAT.format(new Date()));
                if (customerConfirmation == null) {
                    customerConfirmation = CustomerConfirmation.builder()
                            .confirmationWords(
                                    Sets.newHashSet(ConfirmationWord.builder().word(word[0]).dateRecorded(dateRecorded).build()))
                            .createdDate(SIMPLE_DATE_FORMAT.parse(SIMPLE_DATE_FORMAT.format(new Date())))
                            .build();
                    customer.setCustomerConfirmation(customerConfirmation);
                    customer = customerRepository.save(customer);
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("new customerConfirmation created").build());
                }

                customerConfirmation = customer.getCustomerConfirmation();
                //if the user not yet confirmed and not yet id_failed
                if (customerConfirmation != null && !customerConfirmation.isConfirmed() && !customer.isIdFailed()) {
                    if (customerConfirmation.isRequiredToUploadID()) {
                        Set<TestType> testFailsMachine = customerConfirmation.getTestFailsMachine();
                        boolean canVerify = (testFailsMachine == null || !testFailsMachine.contains(TestType.TEST5));
                        //if he is not yet available for manual review
                        if (canVerify) {
                            if (doReset) {
                                jsonObject = new JSONObject();
                                jsonObject.put("attempts", attempts);
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("new attempts 1").build());
                            }
                            updateIDDetails.put(memberID, jsonObject);

                            Set<ConfirmationWord> confirmationWords = customerConfirmation.getConfirmationWords();
                            boolean idMatch = false;
                            boolean firstNameMatch = false;
                            boolean lastNameMatch = false;
                            val textDetectionsID = awsHttp.getTextDetections(idNumber + (documentType.getType()
                                    .equalsIgnoreCase(DocumentType.ID_CARD.getType()) ? "" : documentType.getType()) + "_front.jpeg", 0.75F);
                            for (val text : textDetectionsID) {
                                String detectedTest = text.getDetectedText();
                                if (idMatch && firstNameMatch && lastNameMatch) {
                                    break;
                                }
                                if (!idMatch && contains(detectedTest, idNumber, true)) {
                                    idMatch = true;
                                }
                                if (!firstNameMatch && contains(detectedTest, customer.getFirstName(), false)) {
                                    firstNameMatch = true;
                                }
                                if (!lastNameMatch && contains(detectedTest, customer.getLastName(), false)) {
                                    lastNameMatch = true;
                                }
                            }

                            boolean moveToLiveness = false;

                            // if id verification fail
                            if (!(idMatch && firstNameMatch && lastNameMatch)) {
                                if (testFailsMachine == null) {
                                    testFailsMachine = new HashSet<>();
                                }
                                if (!testFailsMachine.contains(TestType.TEST2)) {
                                    testFailsMachine.add(TestType.TEST2);
                                    customerConfirmation.setTestFailsMachine(testFailsMachine);
                                }
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("idMatch or firstNameMatch or lastNameMatch not valid").build());
//                                if (attempts <= 1) {
//                                    //if fail then return required to upload id as true
//                                    customerConfirmation.setRequiredToUploadID(true);
//                                    response.setStatusCode(194);
//                                    response.setStatus(FAIL);
//                                } else {
                                moveToLiveness = true;
                                socketFacade.send(SocketBodyDto.builder().api(partLog)
                                        .memberIDOrEmail(memberID).message("moveToLiveness").build());
//                                }
                            } else {
                                moveToLiveness = true;
                            }
                            //here fe should return next screen liveness
                            if (moveToLiveness) {
                                schedulers.reminder(memberID, customer.getEmail(), customer.getFirstName(), customer.getLastName(), customer.getPhone());
                                //the word should be same returned every 24 hours, or generate new one after 24 hours
                                // if the word date less than one day then give it back otherwise new one created and saved in db
                                ConfirmationWord confirmationWord = confirmationWords.stream()
                                        // max of the date for latest ConfirmationWord
                                        .max(Comparator.comparing(ConfirmationWord::getDateRecorded))
                                        .orElse(ConfirmationWord.builder().word(word[0]).dateRecorded(dateRecorded).build());

                                Date confirmationWordDateRecorded = confirmationWord.getDateRecorded();
                                confirmationWordDateRecorded.setTime(confirmationWordDateRecorded.getTime() + (3600 * 24 * 1000));
                                if (confirmationWordDateRecorded.after(new Date())) {
                                    //return same word of latest date, and do not change db for this case
                                    word[0] = confirmationWord.getWord();
                                } else {
                                    //if the date more than one day, return new word, change date , save to db new record
                                    confirmationWord = ConfirmationWord.builder().word(word[0]).dateRecorded(dateRecorded).build();
                                    confirmationWords.add(confirmationWord);
                                    //basically no need of setters, keep for safe use
                                    customerConfirmation.setConfirmationWords(confirmationWords);
                                }

                                customerConfirmation.setRequiredToUploadID(false);
                                socketFacade.send(SocketBodyDto.builder().api(partLog)
                                        .memberIDOrEmail(memberID).message("moveToLiveness\t" + ID_UPLOAD_SUCCESS).build());
                                response.setStatusCode(200);
                                response.setStatus(ID_UPLOAD_SUCCESS);
                                response.setSuccess(true);
                                objectMapResponse.put("word", word[0]);
                                customer.setDocumentType(documentType);
                                objectMapResponse.put("selfieImageURL", awsHttp.generateUploadPreSignedUrl(idNumber
                                        .replaceAll("(CIT|REG|ALIEN)*", "").replaceAll("\\s*", "") + "_liveness.jpeg"));
                                response.setObject(objectMapResponse);
                            }
                            customer.setCustomerConfirmation(customerConfirmation);
                            customerRepository.save(customer);
                        } else {
                            //eligible for manual review
                            response.setStatus(NEEDS_MANUAL_REVIEW);
                            response.setStatusCode(301);
                            response.setSuccess(false);
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(NEEDS_MANUAL_REVIEW).build());
                        }
                    } else {
                        //return the latest word used of last time success upload id
                        //waiting for liveness
                        customerConfirmation.getConfirmationWords().stream().max(Comparator.comparing(ConfirmationWord::getDateRecorded))
                                .ifPresent(confirmationWord -> {
                                    objectMapResponse.put("word", confirmationWord.getWord());
                                    objectMapResponse.put("selfieImageURL", awsHttp.generateUploadPreSignedUrl(idNumber
                                            .replaceAll("(CIT|REG|ALIEN)*", "").replaceAll("\\s*", "") + "_liveness.jpeg"));
                                });
                        response.setObject(objectMapResponse);
                        response.setStatus(NEEDS_LIVENESS);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(NEEDS_LIVENESS).build());
                        response.setStatusCode(202);
                        response.setSuccess(false);
                    }
                } else {
                    //customer already confirmed
                    response.setStatus(ALREADY_CONFIRMED);
                    response.setStatusCode(201);
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(ALREADY_CONFIRMED).build());
                }
            } else {
                response.setStatus(USER_NOT_FOUND);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(USER_NOT_FOUND).build());
                response.setStatusCode(119);
            }
        } catch (Exception sqlException) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(sqlException.getMessage()).build());
            response.setStatusCode(105);
            response.setSuccess(false);
            response.setObject(null);
            response.setStatus(INTERNAL_ERROR);
            log.error(sqlException.getMessage(), sqlException);
        }

        log.info("Response status code: " + response.getStatusCode());

        return response;
    }


    public ZCResponse uploadLiveness(String memberID, DocumentType documentType) {
        log.info("uploadLiveness() method");
        ZCResponse response = ZCResponse.builder().build();
        String partLog = "POST /api/validation/rekognition/liveness method";
        try {
            Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
            //if test1 fail, directly lock account
            //if test1 pass: then
            //run test4 :
            // if fail : ask re_upload selfie, increase attempts by 1
            // if success : run test3
            // if test3 fail ask re_upload id, reset details, 3 attempts fail then push to manual review
            if (customer != null) {
                CustomerConfirmation customerConfirmation = customer.getCustomerConfirmation();
                CustomerTransaction customerTransaction = customer.getCustomerTransaction();
                if (customerTransaction != null && customerConfirmation != null) {
                    //user is already upload id, now trying to upload the liveness and check further
                    if (customerConfirmation.isRequiredToUploadLiveness() && !customerConfirmation.isRequiredToUploadID()) {
                        if (!customerConfirmation.isConfirmed() && !customer.isIdFailed()) {
                            int attempts = customerConfirmation.getAttempts();
                            Set<TestType> testFailsMachine = customerConfirmation.getTestFailsMachine();
                            boolean canVerify = (testFailsMachine != null ? !testFailsMachine.contains(TestType.TEST5) : (attempts < 3));
                            if (canVerify) {
                                String idNumber = customer.getIdNumber();
                                attempts += 1;
                                if (testFailsMachine == null) {
                                    testFailsMachine = new HashSet<>();
                                }

                                customerConfirmation.setRequiredToUploadLiveness(false);

                                //get similarity from liveness and word
                                val textDetectionsLiveness = awsHttp.getTextDetections(idNumber.replaceAll("\\s*", "")
                                        + "_liveness.jpeg", 0.75F);
                                boolean livenessMatch = customerConfirmation.getConfirmationWords()
                                        .stream().max(Comparator.comparing(ConfirmationWord::getDateRecorded))
                                        .map(confirmationWord -> textDetectionsLiveness.stream().anyMatch(textDetection -> equals(textDetection.getDetectedText(), confirmationWord.getWord())))
                                        .orElse(false);
                                double similarity;
                                double realBalance;
//                                if (attempts == 3) {
                                similarity =
                                        awsHttp.getSimilarity(idNumber.replaceAll("\\s*", "") + (documentType.getType()
                                                        .equalsIgnoreCase(DocumentType.ID_CARD.getType()) ? "" : documentType.getType()) + "_front.jpeg",
                                                idNumber.replaceAll("\\s*", "") + "_liveness.jpeg");
                                realBalance = tronServiceUtil.getBalance(customerTransaction.getTronAccountAddress(), assetName);
//                                }

                                if (livenessMatch) {
//                                    if (attempts != 3) {
//                                        similarity =
//                                                awsHttp.getSimilarity(idNumber.replaceAll("\\s*", "") + (documentType.getType()
//                                                                .equalsIgnoreCase(DocumentType.ID_CARD.getType()) ? "" : documentType.getType()) + "_front.jpeg",
//                                                        idNumber.replaceAll("\\s*", "") + "_liveness.jpeg");
//                                    }
                                    if (similarity > 99.9999d) {
                                        testFailsMachine.add(TestType.TEST5);
                                        testFailsMachine.add(TestType.TEST1);
                                        updateIDDetails.remove(memberID);
                                        customer.setIdFailed(true);
                                        if (realBalance <= 0) {
                                            amazonSES.sendAccountBlocked(customer.getEmail());

                                            //block account
                                            customer.setEnabled(false);
                                            response.setStatus(ACCOUNT_SUSPENDED_PERMANENTLY);
                                            response.setStatusCode(405);
                                        } else {
                                            if (customer.isNewUser()) {
                                                response.setStatus(ACCOUNT_SUSPENDED_PERMANENTLY_WITH_MOVE_ZASH_ALLOWED);
                                                response.setStatusCode(404);
                                                //start scheduler and fees 2.5%
                                                schedulers.takeFeesAndEmail(memberID, realBalance, customer.getEmail());
                                            } else {
                                                customer.setEnabled(false);
                                                //return all zash in his account to hotwallet
                                                String txID = tronServiceUtil.issueOrTransfer(customerTransaction.getTronPublicKey(),
                                                        hotWalletAddress, realBalance, assetName);
                                                if (txID != null) {
                                                    customer.setBalance(0);
                                                    amazonSES.sendAccountBlocked(customer.getEmail());
                                                } else {
                                                    tronServiceUtil.saveMissedTransaction(customerTransaction.getTronPublicKey(),
                                                            hotWalletAddress, realBalance / Constants.DECIMAL_RATIO, null);
                                                }
                                                response.setStatus(ACCOUNT_SUSPENDED_PERMANENTLY);
                                                response.setStatusCode(405);
                                            }
                                        }
                                        updateIDDetails.remove(memberID);
                                        customerConfirmation.setRequiredToUploadLiveness(true);
                                        customerConfirmation.setRequiredToUploadID(true);
                                        response.setSuccess(false);
                                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("customer id_failed similarity\t"
                                                + similarity).build());
                                    } else if (similarity > 0) {
                                        //4 tests match, eligible for manual review
                                        //when we are here means test1 and test2 and test3 and test 4 is passed, then pass to manual review
                                        testFailsMachine.add(TestType.TEST5);
                                        updateIDDetails.remove(memberID);
                                        customerConfirmation.setRequiredToUploadLiveness(false);
                                        response.setStatus(NEEDS_MANUAL_REVIEW);
                                        response.setStatusCode(200);
                                        response.setSuccess(true);
                                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(SUCCESS).build());

                                    } else {
                                        //idcard and liveness pictures are not same
//                                        JSONObject jsonObject = updateIDDetails.get(memberID);
//                                        if (jsonObject != null) {
//                                            if (!jsonObject.has("anotherChance")) {
//                                                jsonObject.put("attempts", 1);
//                                                jsonObject.put("anotherChance", true);
//                                                --attempts;
//                                                updateIDDetails.put(memberID, jsonObject);
//                                                //ask user to re_uplaod id one more time, attempts should be -1 if it 3, so user can re_upload their id and selfie again, 4 attempts in total if test3 fail
//                                                customerConfirmation.setRequiredToUploadID(true);
//                                                customerConfirmation.setRequiredToUploadLiveness(true);
//                                                response.setStatus(NEEDS_ID_UPLOAD);
//                                                response.setStatusCode(193);
//                                                response.setSuccess(false);
//                                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("anotherChance").build());
//                                            }
//                                        }

//                                        if (response.getStatusCode() != 193) {
//                                            if (attempts < 3) {
//                                                response.setObject("attempts:" + attempts);
//                                                response.setStatusCode(133);
//                                                response.setStatus(FAIL);
//                                                customerConfirmation.setRequiredToUploadLiveness(true);
//                                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("attempts " + attempts).build());
//                                            } else {
                                        response.setStatusCode(301);
                                        response.setStatus(NEEDS_MANUAL_REVIEW);
                                        testFailsMachine.add(TestType.TEST5);
                                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(ATTEMPTS_EXCEED).build());
                                        updateIDDetails.remove(memberID);
//                                            }
//                                        }else {
                                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("idcard and liveness pictures are not same").build());
//                                        }
                                        testFailsMachine.add(TestType.TEST3);
                                    }

                                    customerConfirmation.setTestFailsMachine(testFailsMachine);
                                    customerConfirmation.setAttempts(attempts);
                                    customer.setCustomerConfirmation(customerConfirmation);
                                    customerRepository.save(customer);
                                } else {
                                    testFailsMachine.add(TestType.TEST4);
                                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("liveness match fail").build());
//                                    if (attempts < 3) {
//                                        response.setObject("attempts:" + attempts);
//                                        response.setStatusCode(132);
//                                        response.setStatus(FAIL);
//                                        customerConfirmation.setRequiredToUploadLiveness(true);
//                                    } else {
                                    testFailsMachine.add(TestType.TEST5);
                                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(ATTEMPTS_EXCEED).build());
                                    if (similarity > 99.999d) {
                                        updateIDDetails.remove(memberID);
                                        testFailsMachine.add(TestType.TEST1);
                                        customer.setIdFailed(true);
                                        if (realBalance <= 0) {
                                            amazonSES.sendAccountBlocked(customer.getEmail());
                                            //block account
                                            customer.setEnabled(false);
                                            response.setStatus(ACCOUNT_SUSPENDED_PERMANENTLY);
                                            response.setStatusCode(405);
                                        } else {
                                            if (customer.isNewUser()) {
                                                response.setStatus(ACCOUNT_SUSPENDED_PERMANENTLY_WITH_MOVE_ZASH_ALLOWED);
                                                response.setStatusCode(404);
                                                //start scheduler and fees 2.5%
                                                schedulers.takeFeesAndEmail(memberID, realBalance, customer.getEmail());
                                            } else {
                                                customer.setEnabled(false);
                                                //return all zash in his account to hotwallet
                                                String txID = tronServiceUtil.issueOrTransfer(customerTransaction.getTronPublicKey(),
                                                        hotWalletAddress, realBalance, assetName);
                                                if (txID != null) {
                                                    customer.setBalance(0);
                                                    amazonSES.sendAccountBlocked(customer.getEmail());
                                                } else {
                                                    tronServiceUtil.saveMissedTransaction(customerTransaction.getTronPublicKey(),
                                                            hotWalletAddress, realBalance / Constants.DECIMAL_RATIO, null);
                                                }
                                                response.setStatus(ACCOUNT_SUSPENDED_PERMANENTLY);
                                                response.setStatusCode(405);
                                            }
                                        }
                                        customerConfirmation.setRequiredToUploadLiveness(true);
                                        customerConfirmation.setRequiredToUploadID(true);
                                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("similarity equals blocked customer").build());
                                    } else {
                                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("similarity not equals " + ATTEMPTS_EXCEED).build());
                                        response.setStatusCode(194);
                                        response.setStatus(ATTEMPTS_EXCEED);
                                    }
//                                    }

                                    customerConfirmation.setTestFailsMachine(testFailsMachine);
                                    //when set attempts increased then required to upload liveness to true
                                    customerConfirmation.setAttempts(attempts);
                                    customer.setCustomerConfirmation(customerConfirmation);
                                    customerRepository.save(customer);
                                }
                            } else {
                                //exceed n attempts from algo to validate the id, forward to manual review
                                response.setStatus(NEEDS_MANUAL_REVIEW);
                                response.setStatusCode(301);
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(NEEDS_MANUAL_REVIEW).build());
                                response.setSuccess(false);
                            }
                        } else {
                            //customer already confirmed
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(ALREADY_CONFIRMED + " or id failed").build());
                            response.setStatus(ALREADY_CONFIRMED);
                            response.setStatusCode(201);
                        }
                    } else {
                        if (customerConfirmation.isRequiredToUploadLiveness()) {
                            response.setStatus(NEEDS_ID_UPLOAD);
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("isRequiredToUploadLiveness").build());
                            response.setStatusCode(202);
                        } else {
                            response.setStatus(NEEDS_MANUAL_REVIEW);
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(NEEDS_MANUAL_REVIEW).build());
                            response.setStatusCode(301);
                        }
                        response.setSuccess(false);
                    }
                } else {
                    //id picture still not yet uploaded
                    response.setStatus(NEEDS_ID_UPLOAD);
                    response.setStatusCode(193);
                    response.setSuccess(false);
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(NEEDS_ID_UPLOAD).build());
                }
            } else {
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(USER_NOT_FOUND).build());
                response.setStatus(USER_NOT_FOUND);
                response.setStatusCode(119);
            }
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
            response.setStatusCode(105);
            log.error(e.getMessage());
        }

        log.info("Response status code: " + response.getStatusCode());
        return response;
    }

    //    @CacheResult(cacheName = "stringMatch")
    public boolean contains(String source, String target, boolean ignoreDash) {
        if (!ignoreDash) {
            source = StringUtils.replaceAll(source, "-", " ");
            target = StringUtils.replaceAll(target, "-", " ");
            String finalSource = source;
            return Arrays.stream(target.split("\\s")).filter(s -> !s.isEmpty()).anyMatch(s -> Arrays.stream(finalSource.split("\\s"))
                    .filter(s1 -> !s1.isEmpty())
                    .anyMatch(s1 -> s1.equalsIgnoreCase(s)));
        } else {
            //id_number check
            //remove - and white space from source at target
            //match at least the first 4
            source = StringUtils.replaceAll(source, "-", "").replaceAll("\\s*", "")
                    .replaceAll("(CIT|REG|ALIEN)*(M|F)", ""); //55555555A55
            target = StringUtils.replaceAll(target, "-", "").replaceAll("\\s*", "")
                    .replaceAll("(CIT|REG|ALIEN)*(M|F)", ""); //55555555A55
            return source.contains(target);
        }
    }

    //    @CacheResult(cacheName = "stringEquals")
    public boolean equals(String word1, String word2) {
        try {
            String[] s1 = word1.trim().toLowerCase().split("");
            String[] s2 = word2.trim().toLowerCase().split("");
            ArrayList<String> diff = compareArrays(s1, s2);

            if (s1.length == s2.length) {
                return diff.size() <= 2;
            } else if (s1.length == s2.length + 1 || s1.length == s2.length - 1) {
                return diff.size() <= 1;
            } else {
                return false;
            }
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api("equals").memberIDOrEmail("compare").message(e.getMessage()).build());
            return false;
        }
    }

    //    @CacheResult(cacheName = "compareArrays")
    public ArrayList<String> compareArrays(String[] a1, String[] a2) {
        ArrayList<String> diff = new ArrayList<String>();

        boolean[] rep = new boolean[a2.length];

        Arrays.fill(rep, false);

        for (String str : a1) {
            if (!Arrays.asList(a2).contains(str)) {
                diff.add(str);
            } else {
                rep[Arrays.asList(a2).indexOf(str)] = true;
            }
        }

        for (int i = 0; i < a2.length; i++) {
            if (!rep[i]) {
                diff.add(a2[i]);
            }
        }
        return diff;
    }

    public void deleteAttemptsAfterUnblock(String memberID) {
        updateIDDetails.remove(memberID);
        updateDetails.remove(memberID);
        getUrl.remove(memberID);
    }
}
