package com.zimbocash.service;

import com.zimbocash.commons.CustomResponseDto;
import com.zimbocash.config.security.AuthenticationInterceptor;
import com.zimbocash.dao.CustomerConfirmationRepository;
import com.zimbocash.dao.CustomerDomainBannedRepository;
import com.zimbocash.dao.CustomerRepository;
import com.zimbocash.dao.CustomerReviewsRepository;
import com.zimbocash.dao.CustomerRewardRepository;
import com.zimbocash.dao.DuplicatedImageRepository;
import com.zimbocash.enums.AdminAction;
import com.zimbocash.enums.DocumentType;
import com.zimbocash.enums.MissedTransactionType;
import com.zimbocash.enums.TestAction;
import com.zimbocash.enums.TestType;
import com.zimbocash.enums.UserStatus;
import com.zimbocash.external.AWSHttp;
import com.zimbocash.external.AmazonSES;
import com.zimbocash.external.TelegramClient;
import com.zimbocash.facade.ICustomerFacade;
import com.zimbocash.facade.ISocketFacade;
import com.zimbocash.model.CustomerListRS;
import com.zimbocash.model.ReviewListRS;
import com.zimbocash.model.ZCResponse;
import com.zimbocash.model.dtos.ApproveOrUnblockDto;
import com.zimbocash.model.dtos.CustomerKYCManageDto;
import com.zimbocash.model.dtos.CustomerPaginationDto;
import com.zimbocash.model.dtos.DomainBannedDto;
import com.zimbocash.model.dtos.ManagerOrAdminDto;
import com.zimbocash.model.dtos.SocketBodyDto;
import com.zimbocash.model.entity.AdminComment;
import com.zimbocash.model.entity.ConfirmationWord;
import com.zimbocash.model.entity.Customer;
import com.zimbocash.model.entity.CustomerConfirmation;
import com.zimbocash.model.entity.CustomerReward;
import com.zimbocash.model.entity.CustomerTransaction;
import com.zimbocash.model.entity.DomainBanned;
import com.zimbocash.model.entity.DuplicatedImage;
import com.zimbocash.model.entity.Telegram;
import com.zimbocash.model.entity.TypesList;
import com.zimbocash.scheduler.TriggerSchedulers;
import com.zimbocash.util.Constants;
import com.zimbocash.util.PasswordUtil;
import com.zimbocash.util.StatusCode;
import com.zimbocash.util.TronServiceUtil;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.http.HttpStatus;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.persistence.Column;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.zimbocash.util.Constants.FAIL;
import static com.zimbocash.util.Constants.FIRST_PAYMENT_REWARD;
import static com.zimbocash.util.Constants.NOT_FOUND;
import static com.zimbocash.util.Constants.SIMPLE_DATE_FORMAT;
import static com.zimbocash.util.Constants.SUCCESS;
import static com.zimbocash.util.Constants.TELEGRAM_REWARD;
import static com.zimbocash.util.Constants.USER_NOT_FOUND;

@Component
@RequiredArgsConstructor
@Slf4j
public class AdminService {
    public static volatile String botToken;
    public static volatile String hotWalletAddress;
    private final AWSHttp awsHttp;
    private final AmazonSES amazonSES;
    private final CustomerRepository customerRepository;
    private final CustomerRewardRepository customerRewardRepository;
    private final CustomerReviewsRepository customerReviewsRepository;
    private final CustomerConfirmationRepository customerConfirmationRepository;
    private final CustomerDomainBannedRepository customerDomainBannedRepository;
    private final DuplicatedImageRepository duplicatedImageRepository;
    private final TronServiceUtil tronServiceUtil;
    private final TransactionService transactionService;
    private final TriggerSchedulers schedulers;
    private final TelegramClient telegramClient;
    private final KYCService kycService;
    private final ISocketFacade socketFacade;
    @Value("${email.port}")
    String emailPort;
    @Value("${email.port}")
    String emailHost;
    @Value("${tron.tokenID}")
    String assetName;
    @Autowired
    private Scheduler scheduler;

    public CustomResponseDto adminUpdateCustomer(Customer customer) {
        String partLog = "POST /admin/update method";
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();
        try {
            Customer customerFromDB = customerRepository.findCustomerByMemberID(customer.getMemberID()).orElse(null);
            if (customerFromDB != null) {
                customerRepository.save(mergeCustomers(customer, customerFromDB));
            }
            customResponseDto.setSuccess(true);
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(customer.getEmail()).message("updated").build());
        } catch (Exception e) {
            log.error(e.getMessage());
            customResponseDto.setSuccess(false);
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(customer.getEmail()).message(e.getMessage()).build());
            customResponseDto.setMessage(e.getMessage());
        }
        return customResponseDto;
    }

    //    @CacheResult(cacheName = "mergeCustomers")
    public Customer mergeCustomers(Customer customer, Customer customerFromDB)
            throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Field[] fields = Customer.class.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Column.class)) {
                String value = BeanUtils.getProperty(customer, field.getName());
                if (!field.getName().equals("id") && !field.getName().equals("email") && !field.getName().equals("phoneVerified")
                        && value != null
                        && (field.getType() != int.class || Integer.parseInt(value) != 0)
                        && (field.getType() != double.class || Double.parseDouble(value) != 0D)) {
                    BeanUtils.setProperty(customerFromDB, field.getName(), value);
                }
            }
        }
        return customerFromDB;
    }

    public String getSortColumn(String sortColumn) {
        if (sortColumn != null && !sortColumn.isEmpty()) {
            return sortColumn;
        }
        return "id";
    }

    public CustomerListRS getCustomerList(CustomerPaginationDto query) {
        CustomerListRS response = CustomerListRS.builder().build();
        String partLog = "POST /admin/customers method";
        try {
            String sortDirection = Optional.ofNullable(query.getSortDirection().getValue()).orElse("ASC");
            if (query.getSearchTerm() != null && !query.getSearchTerm().isEmpty()) {
                response.setCustomers(customerRepository.listAllCustomers(query.getSearchTerm(), PageRequest.of(query.getPage(), query.getPageSize(), Sort.Direction.valueOf(sortDirection), getSortColumn(query.getSortColumn()))));
                response.setTotal(customerRepository.countAllCustomers(query.getSearchTerm()));
            } else {
                Page<Customer> customers = customerRepository.findAll(PageRequest.of(query.getPage(), query.getPageSize(), Sort.Direction.valueOf(sortDirection), getSortColumn(query.getSortColumn())));
                response.setCustomers(customers.getContent());
                response.setTotal(customers.getTotalElements());
            }
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(query.toString()).message(SUCCESS).build());
            response.setSuccess(true);
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(query.toString()).message(e.getMessage()).build());
            response.setSuccess(false);
        }
        log.info("Response success: " + response.isSuccess());
        return response;
    }

    public ReviewListRS getReviews() {
        String partLog = "GET /admin/reviews method";
        ReviewListRS response = ReviewListRS.builder().build();
        try {
            response.setReviews(customerReviewsRepository.findAll(Sort.by(Sort.Direction.DESC, "id")));
            response.setSuccess(true);
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail("LIST").message(SUCCESS).build());
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail("LIST").message(e.getMessage()).build());
            response.setSuccess(false);
        }
        return response;
    }

    public ZCResponse getDetails(String memberID) {
        String partLog = "GET /admin/details/{member_id} method";
        ZCResponse response = ZCResponse.builder().build();
        try {
            Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
            if (customer != null && customer.getCustomerReward() != null) {
                Map<String, Object> objectMapResponse = new HashMap<>();
                objectMapResponse.put("referredBy", customer.getCustomerReward().getWhoReferralYou());
                objectMapResponse.put("referralsAll", customerRewardRepository.countCustomerRewardByWhoReferralYou(memberID));
                objectMapResponse.put("status", customer.getStatus());
                objectMapResponse.put("comments", customer.getAdminComments());
                objectMapResponse.put("balance", tronServiceUtil.getBalance(customer.getCustomerTransaction().getTronAccountAddress(), assetName));
                response.setObject(objectMapResponse);
                response.setStatusCode(200);
                response.setSuccess(true);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(SUCCESS).build());
            }
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
            response.setObject(e.getMessage());
        }
        return response;
    }

    public ZCResponse banOrUnBanDomain(DomainBannedDto domainBannedDto) {
        String partLog = "PUT /admin/domains BANUNPAB method";
        boolean isBan = domainBannedDto.isBan();
        String domain = domainBannedDto.getPattern();
        ZCResponse response = ZCResponse.builder().build();
        DomainBanned domainEntity = DomainBanned.builder().pattern(domain).word(domainBannedDto.isWord()).build();
        try {
            if (isBan) {
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(domain).message("BANNED").build());
                customerDomainBannedRepository.save(domainEntity);
            } else {
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(domain).message("UNBANNED").build());
                customerDomainBannedRepository.delete(domainEntity);
            }
            response.setStatusCode(200);
            response.setSuccess(true);
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(domain).message(e.getMessage()).build());
            response.setObject(e.getMessage());
        }
        return response;
    }

    public ZCResponse takeActionForUser(String email, boolean enable, String comment) {
        ZCResponse response = ZCResponse.builder().build();
        String partLog = "POST /admin/disable|enable method";
        try {
            Customer customer = customerRepository.getCustomerByEmail(email);
            if (customer != null) {
                List<AdminComment> adminComments = customer.getAdminComments();
                adminComments.add(AdminComment.builder()
                        .adminAction(enable ? AdminAction.ENABLED : AdminAction.DISABLED)
                        .comment(comment)
                        .addByWho(AuthenticationInterceptor.getLoggedInUserMemberId())
                        .build());
                if (enable) {
                    amazonSES.sendEnableUser(customer.getEmail());
                } else {
                    CustomerConfirmation customerConfirmation = customer.getCustomerConfirmation();
                    amazonSES.sendDisableUSer(customer.getEmail(), (customerConfirmation != null ? customer.getFirstName() : ""));
                }
                customer.setAdminComments(adminComments);
                customer.setEnabled(enable);
                customerRepository.save(customer);
                response.setStatusCode(200);
                response.setSuccess(true);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(enable ? "enabled" : "disabled").build());
            }
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(e.getMessage()).build());
            response.setObject(e.getMessage());
        }
        return response;
    }

    /**********************************/
    //get next for manual review
    public Object getNext(String mangerMemberID) {
        String partLog = "GET /kyc/next method";
        // when user not yet confirmed and not blocked,
        // return every user their attempts more than 3
        // and is not handled already by this manager
        ZCResponse response = ZCResponse.builder().build();
        Optional.ofNullable(customerRepository.nativeQuery("select result.id, result.member_id, result.id_number, result.first_name, result.last_name, result.customer_conf_id, result.document_type from(select zc.member_id, zc.id, zc.first_name, zc.last_name, zc.id_number , zc.document_type, zc.customer_conf_id, ROW_NUMBER() OVER(PARTITION BY zc.member_id) mm from zc_customers_conf zcf left join zc_customers zc on zc.customer_conf_id = zcf.id left join approved ap on zcf.id = ap.CustomerConfirmation_id left join declined dc on zcf.id = dc.CustomerConfirmation_id left join not_sure ns on zcf.id = ns.CustomerConfirmation_id left join TestFailsMachine tfm on zcf.id = tfm.CustomerConfirmation_id where(tfm.type_failed = 'TEST5') and zc.enabled is true and zc.manager is false and zc.admin is false and zc.id_failed is false and zcf.confirmed is false and zcf.eligible_duplication is false and (ap.CustomerConfirmation_id not in (select app.CustomerConfirmation_id from approved app where app.manager_member_id = '" + mangerMemberID + "' and app.manager_member_id is not null) or ap.CustomerConfirmation_id is null) and(dc.CustomerConfirmation_id not in (select dcc.CustomerConfirmation_id from declined dcc where dcc.manager_member_id = '" + mangerMemberID + "' and dcc.manager_member_id is not null) or dc.CustomerConfirmation_id is null)and(ns.CustomerConfirmation_id not in (select nss.CustomerConfirmation_id from not_sure nss where nss.manager_member_id ='" + mangerMemberID + "' and nss.manager_member_id is not null) or ns.CustomerConfirmation_id is null)) result where mm = 1 limit 1;").get(0)).ifPresent(o -> {
            Object[] objects = (Object[]) o;
            if (objects.length > 6) {
                String idNumber = objects[2] != null ? objects[2].toString() : "";
                try {
                    Map<String, Object> objectMapResponse = new HashMap<>();
                    objectMapResponse.put("id", Long.parseLong(objects[0] != null ? objects[0].toString() : "0"));
                    objectMapResponse.put("memberID", objects[1] != null ? objects[1].toString() : "");
                    objectMapResponse.put("firstName", objects[3] != null ? objects[3].toString() : "");
                    objectMapResponse.put("lastName", objects[4] != null ? objects[4].toString() : "");
                    objectMapResponse.put("idNumber", idNumber);

                    String documentType = objects[6] != null ? objects[6].toString() : "ID_CARD";
                    //when id_number latest uploaded in db then we put the flag is it passport or id_number or driver_licence or metal_id
                    objectMapResponse.put("idImageURl", awsHttp.generateDownloadPreSignedUrl(idNumber.replaceAll("\\s*", "")
                            + (documentType.equalsIgnoreCase(DocumentType.ID_CARD.getType()) ? "" : documentType) + "_front.jpeg"));
                    objectMapResponse.put("selfieImageURL", awsHttp.generateDownloadPreSignedUrl(idNumber.replaceAll("\\s*", "") + "_liveness.jpeg"));
                    customerConfirmationRepository.findById(Long.parseLong(objects[5] != null ? objects[5].toString() : "0")).ifPresent(customerConfirmation -> {
                        objectMapResponse.put("testFailsMachine", customerConfirmation.getTestFailsMachine());
                        //return only the last word to manual review
                        customerConfirmation.getConfirmationWords().stream()
                                .max(Comparator.comparing(ConfirmationWord::getDateRecorded))
                                .ifPresent(confirmationWord -> objectMapResponse.put("confirmationWords", Collections.singleton(confirmationWord.getWord())));
                    });
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail("next").message(SUCCESS).build());
                    response.setObject(objectMapResponse);
                    response.setStatus(SUCCESS);
                    response.setStatusCode(200);
                    response.setSuccess(true);
                } catch (Exception e) {
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail("next").message(e.getMessage()).build());
                    log.error(e.getMessage());
                }
            }
        });
        return response;
    }

    @SneakyThrows
    public Object takeActionKYC(CustomerKYCManageDto customerKYCManageDto, String managerMemberID) {
        String partLog = "POST /kyc/takeaction method";
        String memberID = customerKYCManageDto.getMemberID();
        try {
            Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
            if (customer != null && customer.isEnabled()) {
                CustomerConfirmation customerConfirmation = customer.getCustomerConfirmation();
                CustomerTransaction customerTransaction = customer.getCustomerTransaction();
                if (customerTransaction != null && customerConfirmation != null && !customerConfirmation.isConfirmed()) {
                    Set<TestType> testFailsMachine = customerConfirmation.getTestFailsMachine();
                    Map<TestType, TestAction> managerTypeActionMap = customerKYCManageDto.getTestAction();
                    //should not repeat action for same manager
                    //should check with same key and same value
                    //accept only if all test received match the testFailsMachine
                    if (managerTypeActionMap.keySet().containsAll(testFailsMachine)) {
                        Map<String, TypesList> alreadyApproved = customerConfirmation.getApproved();
                        Map<String, TypesList> alreadyDeclined = customerConfirmation.getDecline();
                        Map<String, TypesList> alreadyNotSure = customerConfirmation.getNotSure();
                        if (!alreadyApproved.containsKey(managerMemberID) && !alreadyDeclined.containsKey(managerMemberID)
                                && !alreadyNotSure.containsKey(managerMemberID)) {
                            managerTypeActionMap
                                    .entrySet()
                                    .stream()
                                    .filter(testTypeTestActionEntry -> !testTypeTestActionEntry.getKey().equals(TestType.TEST1))
                                    .forEach(testTypeTestActionEntry -> {
                                        TestType type = testTypeTestActionEntry.getKey();
                                        TestAction action = testTypeTestActionEntry.getValue();
                                        if (action.equals(TestAction.APPROVE)) {
                                            List<TestType> testTypes = new ArrayList<>();
                                            TypesList typesList = TypesList.builder().build();
                                            if (alreadyApproved.containsKey(managerMemberID)) {
                                                typesList = alreadyApproved.get(managerMemberID);
                                                testTypes = typesList.getTestTypes();
                                            }
                                            testTypes.add(type);
                                            typesList.setTestTypes(testTypes);
                                            alreadyApproved.put(managerMemberID, typesList);
                                        } else if (action.equals(TestAction.DECLINE)) {
                                            List<TestType> testTypes = new ArrayList<>();
                                            TypesList typesList = TypesList.builder().build();
                                            if (alreadyDeclined.containsKey(managerMemberID)) {
                                                typesList = alreadyDeclined.get(managerMemberID);
                                                testTypes = typesList.getTestTypes();
                                            }
                                            testTypes.add(type);
                                            typesList.setTestTypes(testTypes);
                                            alreadyDeclined.put(managerMemberID, typesList);
                                        } else if (action.equals(TestAction.NOT_SURE)) {
                                            List<TestType> testTypes = new ArrayList<>();
                                            TypesList typesList = TypesList.builder().build();
                                            if (alreadyNotSure.containsKey(managerMemberID)) {
                                                typesList = alreadyNotSure.get(managerMemberID);
                                                testTypes = typesList.getTestTypes();
                                            }
                                            testTypes.add(type);
                                            typesList.setTestTypes(testTypes);
                                            alreadyNotSure.put(managerMemberID, typesList);
                                        }
                                    });

                            //basically it should update without setters, but we keep it in case
                            customerConfirmation.setApproved(alreadyApproved);
                            customerConfirmation.setDecline(alreadyDeclined);
                            customerConfirmation.setNotSure(alreadyNotSure);

                            customerConfirmation = customerConfirmationRepository.save(customerConfirmation);

                            final long[] numberOfApprovedTest2 = {0};
                            final long[] numberOfApprovedTest3 = {0};
                            final long[] numberOfApprovedTest4 = {0};
                            final long[] numberOfApprovedTest5 = {0};
                            final long[] numberOfDeclinedTest2 = {0};
                            final long[] numberOfDeclinedTest3 = {0};
                            final long[] numberOfDeclinedTest4 = {0};
                            final long[] numberOfDeclinedTest5 = {0};
                            final long[] numberOfNotSureTest2 = {0};
                            final long[] numberOfNotSureTest3 = {0};
                            final long[] numberOfNotSureTest4 = {0};
                            final long[] numberOfNotSureTest5 = {0};
                            alreadyApproved
                                    .forEach((key, value) -> {
                                        List<TestType> testTypes = value.getTestTypes();
                                        if (testTypes.contains(TestType.TEST2)) {
                                            ++numberOfApprovedTest2[0];
                                        }
                                        if (testTypes.contains(TestType.TEST3)) {
                                            ++numberOfApprovedTest3[0];
                                        }
                                        if (testTypes.contains(TestType.TEST4)) {
                                            ++numberOfApprovedTest4[0];
                                        }
                                        if (testTypes.contains(TestType.TEST5)) {
                                            ++numberOfApprovedTest5[0];
                                        }
                                    });

                            alreadyDeclined
                                    .forEach((key, value) -> {
                                        List<TestType> testTypes = value.getTestTypes();
                                        if (testTypes.contains(TestType.TEST2)) {
                                            ++numberOfDeclinedTest2[0];
                                        }
                                        if (testTypes.contains(TestType.TEST3)) {
                                            ++numberOfDeclinedTest3[0];
                                        }
                                        if (testTypes.contains(TestType.TEST4)) {
                                            ++numberOfDeclinedTest4[0];
                                        }
                                        if (testTypes.contains(TestType.TEST5)) {
                                            ++numberOfDeclinedTest5[0];
                                        }
                                    });

                            alreadyNotSure
                                    .forEach((key, value) -> {
                                        List<TestType> testTypes = value.getTestTypes();
                                        if (testTypes.contains(TestType.TEST2)) {
                                            ++numberOfNotSureTest2[0];
                                        }
                                        if (testTypes.contains(TestType.TEST3)) {
                                            ++numberOfNotSureTest3[0];
                                        }
                                        if (testTypes.contains(TestType.TEST4)) {
                                            ++numberOfNotSureTest4[0];
                                        }
                                        if (testTypes.contains(TestType.TEST5)) {
                                            ++numberOfNotSureTest5[0];
                                        }
                                    });

                            numberOfDeclinedTest2[0] += numberOfNotSureTest2[0] / 3;
                            numberOfDeclinedTest3[0] += numberOfNotSureTest3[0] / 3;
                            numberOfDeclinedTest4[0] += numberOfNotSureTest4[0] / 3;
                            numberOfDeclinedTest5[0] += numberOfNotSureTest5[0] / 3;
                            double realBalance = tronServiceUtil.getBalance(customerTransaction.getTronAccountAddress(), assetName);
                            if (numberOfDeclinedTest2[0] > 1 || numberOfDeclinedTest3[0] > 1 || numberOfDeclinedTest4[0] > 1 || numberOfDeclinedTest5[0] > 1) {
                                customer.setIdFailed(true);
                                //block account only when his balance is 0
                                if (realBalance <= 0) {
                                    amazonSES.sendAccountBlocked(customer.getEmail());
                                    //block account
                                    customer.setEnabled(false);
                                } else {
                                    if (customer.isNewUser()) {
                                        //start scheduler and fees 2.5%
                                        schedulers.takeFeesAndEmail(memberID, realBalance, customer.getEmail());
                                    } else {
                                        customer.setEnabled(false);
                                        //return all zash in his account to hotwallet
                                        String txID = tronServiceUtil.issueOrTransfer(customerTransaction.getTronPublicKey(),
                                                hotWalletAddress, realBalance, assetName);
                                        if (txID != null) {
                                            customer.setBalance(0);
                                            amazonSES.sendAccountBlocked(customer.getEmail());
                                        } else {
                                            tronServiceUtil.saveMissedTransaction(customerTransaction.getTronPublicKey(),
                                                    hotWalletAddress, realBalance / Constants.DECIMAL_RATIO, null);
                                        }
                                    }
                                }
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("FAIL ID").build());
                                customerRepository.save(customer);
                                return CustomResponseDto.ok();
                            }

                            boolean test2Success = !testFailsMachine.contains(TestType.TEST2) || (numberOfApprovedTest2[0] > 1);
                            boolean test3Success = !testFailsMachine.contains(TestType.TEST3) || (numberOfApprovedTest3[0] > 1);
                            boolean test4Success = !testFailsMachine.contains(TestType.TEST4) || (numberOfApprovedTest4[0] > 1);
                            boolean test5Success = !testFailsMachine.contains(TestType.TEST5) || (numberOfApprovedTest5[0] > 1);
                            String idNumber = customer.getIdNumber().replaceAll("\\s*", "");
                            if (test2Success && test3Success && test4Success && test5Success) {
                                String documentType = customer.getDocumentType().getType();
                                boolean checkImageDuplicationID =
                                        awsHttp.searchForDuplicateFace(
                                                idNumber.replaceAll("(CIT|REG|ALIEN)*", "")
                                                        .replaceAll("\\s*", "") + (documentType.equalsIgnoreCase(DocumentType.ID_CARD.getType()) ? "" : documentType) + "_front.jpeg", memberID,
                                                "_front", idNumber, true);
                                boolean checkImageDuplicationSelfie =
                                        awsHttp.searchForDuplicateFace(
                                                idNumber.replaceAll("(CIT|REG|ALIEN)*", "")
                                                        .replaceAll("\\s*", "") + "_liveness.jpeg", memberID,
                                                "_liveness", idNumber, true);
                                //check for duplication from aws, if fail duplication then show it to manager
                                if (checkImageDuplicationID && checkImageDuplicationSelfie) {
                                    amazonSES.sendAccountConfirmed(customer.getEmail());
                                    customerConfirmation.setConfirmed(true);
                                    customer.setCustomerConfirmation(customerConfirmation);
                                    customer.setEnabled(true);
                                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("CONFIRMED").build());
                                    customerRepository.save(customer);
                                    issueRewardAndEmails(customer);
                                } else {
                                    //only this test will allow duplication to be open
                                    customerConfirmation.setEligibleForManageDuplication(true);
                                    customer.setCustomerConfirmation(customerConfirmation);
                                    customerRepository.save(customer);
                                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("ELIGIBLE FOR DUPLICATION").build());
                                }
                                return CustomResponseDto.ok();
                            } else {
                                String documentType = customer.getDocumentType().getType();
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("STILL WAITING FOR OTHER MANAGERS").build());
                                //add him for duplication as the first one still have test5Success fail
                                awsHttp.searchForDuplicateFace(
                                        idNumber.replaceAll("(CIT|REG|ALIEN)*", "")
                                                .replaceAll("\\s*", "") +
                                                (documentType.equalsIgnoreCase(DocumentType.ID_CARD.getType()) ? "" : documentType) + "_front.jpeg", memberID, "_front", idNumber, false);
                                awsHttp.searchForDuplicateFace(
                                        idNumber.replaceAll("(CIT|REG|ALIEN)*", "")
                                                .replaceAll("\\s*", "") + "_liveness.jpeg", memberID, "_liveness", idNumber, false);
                            }
                            customerRepository.save(customer);
                        }
                    }
                }
            }
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
        }
        return CustomResponseDto.ok();
    }

    private void issueRewardAndEmails(Customer customer) {
        String partLog = "internal method /issueRewardAndEmails";
        String memberID = customer.getMemberID();
        log.info("issuing reward for memberID\t" + memberID);
        CustomerReward customerReward = customer.getCustomerReward();
        CustomerTransaction customerTransaction = customer.getCustomerTransaction();
        if (customerReward != null && customerTransaction != null) {
            //means he have already a tron account created from the system
            //1
            try {
                if (customerReward.getTelegramReward() == 0) {
                    //issue telegram reward
                    Telegram telegram = customer.getTelegram();
                    if (telegram != null && telegram.isVerified()) {
                        String txID = tronServiceUtil
                                .issueOrTransfer(null, customerTransaction.getTronAccountAddress(), TELEGRAM_REWARD, assetName);
                        if (txID != null) {
                            //add TELEGRAM_REWARD zash to their balance, it's already in blockchain address, just update the db and cache
                            customer.setBalance(customer.getBalance() + TELEGRAM_REWARD);
                            customerReward.setTelegramReward(TELEGRAM_REWARD);
                            telegramClient.sendMessage(botToken,
                                    String.valueOf(telegram.getChatID()), "Congratulations! You have verified your telegram account, " +
                                            "and ZIMBOCASH award you with 100 ZASH");
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("issued telegram").build());
                        } else {
                            tronServiceUtil.saveMissedTransaction(MissedTransactionType.HOT_WALLET.getStatus(),
                                    customerTransaction.getTronAccountAddress(), TELEGRAM_REWARD, memberID);
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("issuing telegram missing transaction").build());
                        }
                    }
                }
            } catch (Exception e) {
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
                log.error(e.getMessage());
            }
            try {
                //2
                double signUpReward = ICustomerFacade.getSignUpReward(customer.getDateRecorded());
                if (customerReward.getSignUpReward() == 0 && signUpReward > 0 && customer.isNewUser()) {
                    //issue signup reward
                    String txID = tronServiceUtil
                            .issueOrTransfer(null, customerTransaction.getTronAccountAddress(), signUpReward, assetName);
                    if (txID != null) {
                        //add signUpReward zash to their balance, it's already in blockchain address, just update the db and cache
                        customer.setBalance(customer.getBalance() + signUpReward);
                        customerReward.setSignUpReward(signUpReward);
                        amazonSES.sendReceivedReward(customer.getEmail(), String.valueOf((long) signUpReward), customer.getMemberID(), customer.getFirstName());
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("issued signup reward : " + signUpReward).build());
                    } else {
                        tronServiceUtil.saveMissedTransaction(MissedTransactionType.HOT_WALLET.getStatus(),
                                customerTransaction.getTronAccountAddress(), signUpReward, memberID);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("issuing signup reward : " + signUpReward + " missing transaction").build());
                    }
                }
            } catch (Exception e) {
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
                log.error(e.getMessage());
            }
            //3-1
            //user b sign up , who_referral_you updates with user a.
            // user b verify his id:
            // if user a verified id get referral_reward = (depends on time when user b signup / 10) and issued it to user a,
            // and update referral_reward_issued= amount(that user a get) for user b
            try {

                String whoReferralYou = customerReward.getWhoReferralYou();
                if (whoReferralYou != null && customerReward.getReferralRewardIssuedAmount() == 0) {
                    Customer customerWhoReferralYou = customerRepository.findCustomerByMemberID(whoReferralYou).orElse(null);
                    if (customerWhoReferralYou != null) {
                        CustomerConfirmation whoReferralYouCustomerConfirmation = customerWhoReferralYou.getCustomerConfirmation();
                        CustomerTransaction whoReferralYouCustomerTransaction = customerWhoReferralYou.getCustomerTransaction();
                        if (whoReferralYouCustomerConfirmation != null && whoReferralYouCustomerConfirmation.isConfirmed()
                                && whoReferralYouCustomerTransaction != null) {
                            double referralReward = ICustomerFacade.getSignUpReward(customer.getDateRecorded()) / 10;
                            String txID = tronServiceUtil
                                    .issueOrTransfer(null, whoReferralYouCustomerTransaction.getTronAccountAddress(), referralReward, assetName);
                            if (txID != null) {
                                customerReward.setReferralRewardIssuedAmount(referralReward);
                                //add REFERRAL_REWARD zash to whoreferralyou balance, it's already in blockchain address, just update the db and cache
                                customerWhoReferralYou.setBalance(customerWhoReferralYou.getBalance() + referralReward);
                                customerRepository.save(customerWhoReferralYou);
                                amazonSES.sendRewardReferral(customerWhoReferralYou.getEmail(), String.valueOf((long) referralReward),
                                        customerWhoReferralYou.getMemberID(), customerWhoReferralYou.getFirstName());
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("issued referral reward :" +
                                        referralReward).build());

                            } else {
                                tronServiceUtil.saveMissedTransaction(MissedTransactionType.HOT_WALLET.getStatus(),
                                        whoReferralYouCustomerTransaction.getTronAccountAddress(),
                                        referralReward, whoReferralYou);
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("issueing referral reward :" +
                                        referralReward + " missing transaction").build());
                            }
                        }
                    }
                }
            } catch (Exception e) {
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
                log.error(e.getMessage());
            }

            //3-2
            // when user a verify his id: fetch referral of reward table, and find the referral if it match user a then get the date_recorded of user b and
            // calculate the reward,
            // and check if referral_reward_issued = 0 (0 means not yet issued)
            // for user b, then issue to user a and change referral_reward_issued = amount(that user a get) for user b.
            try {

                final boolean[] isSend = {false};

                Optional.ofNullable(customerRepository.nativeQuery("select distinct zc.member_id from zc_customers zc\n" +
                        "                  inner join zc_customers_rewards zcrw on zc.customer_reward_id = zcrw.id\n" +
                        "                  inner join zc_customers_conf zccf on zc.customer_conf_id = zccf.id\n" +
                        "where\n" +
                        "      zc.admin is false and zc.manager is false and zc.enabled is true\n" +
                        "  and zc.member_id != '" + customer.getMemberID() + "' and zc.phone_verified is true and zccf.confirmed is true and zcrw" +
                        ".who_referral_you = '"
                        + customer.getMemberID() + "' and zcrw.referral_reward_amount = 0;"))
                        .ifPresent(objectList -> objectList.forEach(o -> {
                            String userBMemberID = o.toString();
                            Customer userBCustomer = customerRepository.findCustomerByMemberID(userBMemberID).orElse(null);
                            if (userBCustomer != null) {
                                CustomerReward userBCustomerReward = userBCustomer.getCustomerReward();
                                if (userBCustomerReward != null) {
                                    double referralReward = ICustomerFacade.getSignUpReward(userBCustomer.getDateRecorded()) / 10;
                                    String txID = tronServiceUtil
                                            .issueOrTransfer(null, customerTransaction.getTronAccountAddress(), referralReward, assetName);
                                    if (txID != null) {
                                        //add referralReward zash to this customer balance, it's already in blockchain address, just update the db and cache
                                        customer.setBalance(customer.getBalance() + referralReward);
                                        userBCustomerReward.setReferralRewardIssuedAmount(referralReward);
                                        customerRepository.save(userBCustomer);
                                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID)
                                                .message("issued who referral you reward : " + referralReward).build());
                                        isSend[0] = true;
                                    } else {
                                        tronServiceUtil.saveMissedTransaction(MissedTransactionType.HOT_WALLET.getStatus(),
                                                customerTransaction.getTronAccountAddress(),
                                                referralReward, memberID);
                                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID)
                                                .message("issued who referral you reward : " + referralReward + " missing transaction").build());

                                    }
                                }
                            }
                        }));
                if (isSend[0]) {
                    amazonSES.sendRewardReferralFromUserB(customer.getEmail(), customer.getMemberID(), customer.getFirstName());
                }
            } catch (Exception e) {
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
                log.error(e.getMessage());
            }


            //4
            //if b first time to receive payment , i.e first_payment_reward_issued = 0
            // when a pay b :
            // if a is verified then
            // (if b verified then issue and change first_payment_reward_issued = 100 (amount that user a will get),
            // sender_first_time_payment to user a in reward table of user b,
            // if b not verified then put a in reward table of b)
            // when b verified then issue to a (means find a from b reward table then issue him
            // and make first_payment_reward_issued = 100 (amount that user a will get) of b),

            //if a is not verified then (put a in reward table of user b)
            // when b is verified then find sender_first_time_payment from b reward table and check first_payment_reward_issued = 0 (amount to user a not yet
            // issued)
            //if a is verified then issue and change sender_first_time_payment and first_payment_reward_issued = 100 (amount that user a will get) of user b
            // table

            // when a is verified find sender_first_time_payment of b and check if b is verified then issue to a
            // and change first_payment_reward_issued = 100 (amount that user a will get) of user b table

            //4-1
            try {

                String senderFirstTimePayment = customerReward.getSenderFirstTimePayment();
                double firstPaymentRewardIssued = customerReward.getFirstPaymentRewardIssued();
                if (firstPaymentRewardIssued == 0 && senderFirstTimePayment != null) {
                    Customer userACustomer = customerRepository.findCustomerByMemberID(senderFirstTimePayment).orElse(null);
                    if (userACustomer != null && userACustomer.isPhoneVerified()) {
                        CustomerTransaction userACustomerTransaction = userACustomer.getCustomerTransaction();
                        CustomerConfirmation userACustomerConfirmation = userACustomer.getCustomerConfirmation();
                        if (userACustomerTransaction != null && userACustomerConfirmation != null && userACustomerConfirmation.isConfirmed()) {
                            String txID = tronServiceUtil
                                    .issueOrTransfer(null, userACustomerTransaction.getTronAccountAddress(), FIRST_PAYMENT_REWARD, assetName);
                            if (txID != null) {
                                //add FIRST_PAYMENT_REWARD zash to user a balance, it's already in blockchain address, just update the db and cache
                                userACustomer.setBalance(userACustomer.getBalance() + FIRST_PAYMENT_REWARD);
                                customerRepository.save(userACustomer);
                                amazonSES.sendFirstPaymentReward(customer.getEmail(), customer.getFirstName(), customer.getMemberID());
                                customerReward.setFirstPaymentRewardIssued(FIRST_PAYMENT_REWARD);
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(
                                        "issued first payment reward : " + FIRST_PAYMENT_REWARD).build());
                            } else {
                                tronServiceUtil.saveMissedTransaction(MissedTransactionType.HOT_WALLET.getStatus(),
                                        userACustomerTransaction.getTronAccountAddress(),
                                        FIRST_PAYMENT_REWARD, senderFirstTimePayment);
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(
                                        "issued first payment reward : " + FIRST_PAYMENT_REWARD + " missing transaction").build());
                            }
                        }
                    }
                }
            } catch (Exception e) {
                log.error(e.getMessage());
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
            }

            try {
                boolean[] isSend = {false};

                //4-b
                Optional.ofNullable(customerRepository.nativeQuery("select distinct zc.member_id from zc_customers zc\n" +
                        "                                      inner join zc_customers_rewards zcrw on zc.customer_reward_id = zcrw.id\n" +
                        "                                      inner join zc_customers_conf zccf on zc.customer_conf_id = zccf.id\n" +
                        "where\n" +
                        "    zc.admin is false and zc.manager is false and zc.enabled is true\n" +
                        "  and zc.member_id != '" + customer.getMemberID() + "' and zc.phone_verified is true\n" +
                        "  and zccf.confirmed is true and zcrw.first_payment_reward_issued = 0 and zcrw.sender_first_time_payment = '" + customer.getMemberID() + "';"))
                        .ifPresent(objectList -> objectList.forEach(o -> {
                            String userBMemberID = o.toString();
                            Customer userBCustomer = customerRepository.findCustomerByMemberID(userBMemberID).orElse(null);
                            if (userBCustomer != null) {
                                CustomerReward userBCustomerReward = userBCustomer.getCustomerReward();
                                if (userBCustomerReward != null) {
                                    String txID = tronServiceUtil
                                            .issueOrTransfer(null, customerTransaction.getTronAccountAddress(), FIRST_PAYMENT_REWARD, assetName);
                                    if (txID != null) {
                                        //add FIRST_PAYMENT_REWARD zash to this customer balance, it's already in blockchain address, just update the db and
                                        // cache
                                        customer.setBalance(customer.getBalance() + FIRST_PAYMENT_REWARD);

                                        userBCustomerReward.setFirstPaymentRewardIssued(FIRST_PAYMENT_REWARD);
                                        customerRepository.save(userBCustomer);
                                        isSend[0] = true;
                                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID)
                                                .message("issued first payment reward : " + FIRST_PAYMENT_REWARD).build());
                                    } else {
                                        tronServiceUtil.saveMissedTransaction(MissedTransactionType.HOT_WALLET.getStatus(),
                                                customerTransaction.getTronAccountAddress(),
                                                FIRST_PAYMENT_REWARD, memberID);
                                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID)
                                                .message("issued first payment reward : " + FIRST_PAYMENT_REWARD + " missing transaction").build());
                                    }
                                }
                            }
                        }));
                if (isSend[0]) {
                    amazonSES.sendRewardReferralFromUserB(customer.getEmail(), customer.getMemberID(), customer.getFirstName());
                }
            } catch (Exception e) {
                log.error(e.getMessage());
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID)
                        .message(e.getMessage()).build());
            }

            customer.setCustomerReward(customerReward);
            customerRepository.save(customer);
        }
    }

    //return id to FE
    public Object getNextKYCDuplicated(String mangerMemberID) {
        ZCResponse response = ZCResponse.builder().build();
        String partLog = "POST /kyc/duplicated method";
        List<Map<String, Object>> results = new ArrayList<>();
        final String[] memberID = {""};
        Optional.ofNullable(customerRepository.nativeQuery("select result.id, result.original_id_number, result.id_number, result.type, result.member_id, " +
                "result.zcid\n" +
                "from (\n" +
                "select zcdi.id, zcdi.original_id_number, zc.id_number, zcdi.type, zc.member_id, zc.id as zcid, ROW_NUMBER() OVER(PARTITION BY zcdi.type) rn " +
                "from zc_duplicated_image zcdi\n" +
                "                left join zc_customers zc on zc.id = zcdi.customer_duplicate_id\n" +
                "                  left join zc_customers_conf zcf on zcf.id = zc.customer_conf_id\n" +
                "    where (zcdi.by_who != '"
                + mangerMemberID + "' or zcdi.by_who is null) and (zcdi.action is null or zcdi.action = 'FORKED')\n" +
                "and zcdi.customer_duplicate_id not in\n" +
                "                               (select zcdii.customer_duplicate_id from zc_duplicated_image zcdii where zcdi.type = zcdii.type and zcdii" +
                ".by_who = '"
                + mangerMemberID + "' and zcdii.action is not null)\n" +
                "and zc.admin is false and zc.manager is false and zcf.confirmed is false and zc.enabled is true and zc.id_failed is false\n" +
                "and zcf.eligible_duplication is true\n" +
                "order by zcdi.id, zcdi.original_id_number, zc.id_number, zcdi.type, zc.member_id, zc.id) result\n" +
                "where rn = 1;\n")).ifPresent(objectList -> {
            objectList.forEach(o -> {
                try {
                    Object[] objects = (Object[]) o;
                    //the same or first time
                    if (objects[4].toString().equals(memberID[0]) || memberID[0].isEmpty()) {
                        memberID[0] = objects[4].toString();
                        long zcdiID = Long.parseLong(objects[0].toString()); //will be used to update bywho later
                        //to retrieve from aws
                        //originalIdNumber = idnumber of non duplicate user with trimmed
                        String originalIdNumber = objects[1].toString();
                        String duplicateIdNUmber = objects[2].toString();
                        String type = objects[3].toString();
                        Map<String, Object> objectMapResponse = new HashMap<>();
                        String imageOriginalURL = awsHttp.generateDownloadPreSignedUrl(originalIdNumber
                                .replaceAll("(CIT|REG|ALIEN)*", "").replaceAll("\\s*", "") + type + ".jpeg");
                        String imageDuplicatedURL = awsHttp.generateDownloadPreSignedUrl(duplicateIdNUmber
                                .replaceAll("(CIT|REG|ALIEN)*", "").replaceAll("\\s*", "") + type + ".jpeg");
                        objectMapResponse.put("zcdiID", zcdiID);
                        objectMapResponse.put("type", type);
                        objectMapResponse.put("imageOriginalURL", imageOriginalURL);
                        objectMapResponse.put("imageDuplicatedURL", imageDuplicatedURL);
                        //the member id found should be for same user
                        objectMapResponse.put("memberID", memberID[0]);
                        objectMapResponse.put("id", Long.parseLong(objects[5].toString()));
                        results.add(objectMapResponse);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(mangerMemberID).message(SUCCESS).build());
                    }
                    response.setStatusCode(200);
                    response.setStatus(SUCCESS);
                    response.setSuccess(true);
                } catch (Exception e) {
                    log.error(e.getMessage());
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(mangerMemberID).message(e.getMessage()).build());
                    response.setStatusCode(400);
                    response.setStatus(FAIL);
                    response.setSuccess(false);
                }
            });
            response.setObject(results);
        });
        return response;
    }

    public Object updateDuplications(String memberID, long zcdiID, TestAction action, String mangerMemberID) {
        Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
        String partLog = "POST /kyc/update method";
        if (customer != null) {
            CustomerConfirmation customerConfirmation = customer.getCustomerConfirmation();
            CustomerTransaction customerTransaction = customer.getCustomerTransaction();
            if (customerTransaction != null && customerConfirmation != null && customerConfirmation.isEligibleForManageDuplication()
                    && customer.isEnabled() && !customerConfirmation.isConfirmed() && !action.equals(TestAction.FORKED)) {
                //front or livenesss or ""
                List<DuplicatedImage> duplicatedImages = customer.getDuplicatedImages();
                String typeSupposed = duplicatedImages.stream().filter(duplicatedImage -> duplicatedImage.getId() == zcdiID
                        && duplicatedImage.getAction() == null || Optional.ofNullable(duplicatedImage.getAction()).orElse(TestAction.APPROVE).equals(TestAction.FORKED))
                        .limit(1)
                        .map(DuplicatedImage::getType).findFirst().orElse("");
                if (!typeSupposed.isEmpty()) {
                    //now parse the list, check bywho = mangerMemberID and type = front or liveness and zcdiID is not the same and action is not null,
                    //means already make an action for same user for same type, then no access
                    boolean alreadyVotedForSameUserForSameType = duplicatedImages.stream()
                            .anyMatch(duplicatedImage -> (Optional.ofNullable(duplicatedImage.getBy_who()).orElse("").equals(mangerMemberID)
                                    && duplicatedImage.getType().equals(typeSupposed) && zcdiID != duplicatedImage.getId()
                                    && duplicatedImage.getAction() != null));
                    if (!alreadyVotedForSameUserForSameType) {
                        DuplicatedImage duplicatedImage = duplicatedImages.stream()
                                .filter(duplicatedImage1 -> duplicatedImage1.getId() == zcdiID).findFirst()
                                .orElse(null);
                        if (duplicatedImage != null) {
                            duplicatedImage.setAction(action);
                            duplicatedImage.setBy_who(mangerMemberID);
                            duplicatedImageRepository.save(duplicatedImage);

                            final long[] numberOfIDApproved = {0};
                            final long[] numberOfIDDeclined = {0};
                            final long[] numberOfIDNotSure = {0};
                            final long[] numberOfSelfieApproved = {0};
                            final long[] numberOfSelfieDeclined = {0};
                            final long[] numberOfSelfieNotSure = {0};
                            customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);

                            if (customer != null) {
                                final int[] duplicationSizeID = {0};
                                final int[] numberNullFront = {0}; //i.e still operation waiting for another manager
                                final int[] numberNullSelfie = {0}; //i.e still operation waiting for another manager
                                final int[] duplicationSizeSELFIE = {0};
                                customer.getDuplicatedImages()
                                        .forEach(duplicatedImage1 -> {
                                            String type = duplicatedImage1.getType();
                                            TestAction testAction = duplicatedImage1.getAction();
                                            if (type.equals("_front")) {
                                                if (testAction != null && !testAction.equals(TestAction.FORKED)) {
                                                    ++duplicationSizeID[0];
                                                    if (testAction.equals(TestAction.APPROVE)) {
                                                        ++numberOfIDApproved[0];
                                                    } else if (testAction.equals(TestAction.DECLINE)) {
                                                        ++numberOfIDDeclined[0];
                                                    } else if (testAction.equals(TestAction.NOT_SURE)) {
                                                        ++numberOfIDNotSure[0];
                                                    }
                                                } else {
                                                    ++numberNullFront[0];
                                                }
                                            } else if (type.equals("_liveness")) {
                                                if (testAction != null && !testAction.equals(TestAction.FORKED)) {
                                                    ++duplicationSizeSELFIE[0];
                                                    if (testAction.equals(TestAction.APPROVE)) {
                                                        ++numberOfSelfieApproved[0];
                                                    } else if (testAction.equals(TestAction.DECLINE)) {
                                                        ++numberOfSelfieDeclined[0];
                                                    } else if (testAction.equals(TestAction.NOT_SURE)) {
                                                        ++numberOfSelfieNotSure[0];
                                                    }
                                                } else {
                                                    ++numberNullSelfie[0];
                                                }
                                            }
                                        });


                                //means only two manager approved the testmachines ,so we need a anothers random managers to finish in case of notsure_decline
                                //in case id|selfie duplication table size for front  is less than 3 and the approved is less than 2
                                numberOfIDDeclined[0] += numberOfIDNotSure[0] / 3;
                                numberOfSelfieDeclined[0] += numberOfSelfieNotSure[0] / 3;
                                double realBalance = tronServiceUtil.getBalance(customerTransaction.getTronAccountAddress(), assetName);
                                if (numberOfIDDeclined[0] > 1 || numberOfSelfieDeclined[0] > 1) {
                                    customer.setIdFailed(true);
                                    if (realBalance <= 0) {
                                        amazonSES.sendAccountBlocked(customer.getEmail());

                                        //block account
                                        customer.setEnabled(false);
                                    } else {
                                        if (customer.isNewUser()) {
                                            //start scheduler and fees 2.5%
                                            schedulers.takeFeesAndEmail(memberID, realBalance, customer.getEmail());
                                        } else {
                                            customer.setEnabled(false);
                                            //return all zash in his account to hotwallet
                                            String txID = tronServiceUtil.issueOrTransfer(customerTransaction.getTronPublicKey(),
                                                    hotWalletAddress, realBalance, assetName);
                                            if (txID != null) {
                                                customer.setBalance(0);
                                                amazonSES.sendAccountBlocked(customer.getEmail());
                                            } else {
                                                tronServiceUtil.saveMissedTransaction(customerTransaction.getTronPublicKey(),
                                                        hotWalletAddress, realBalance / Constants.DECIMAL_RATIO, null);
                                            }
                                        }
                                    }
                                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("ID FAILED").build());
                                    customer = customerRepository.save(customer);
                                } else if (numberOfIDApproved[0] > 1 && numberOfSelfieApproved[0] > 1 ||
                                        ((numberNullFront[0] == 0 && numberOfSelfieApproved[0] > 1) || (numberNullSelfie[0] == 0 && numberOfIDApproved[0] > 1))) {
                                    amazonSES.sendAccountConfirmed(customer.getEmail());
                                    customerConfirmation.setConfirmed(true);
                                    customerConfirmation.setEligibleForManageDuplication(false);

                                    customer.getDuplicatedImages().clear();
                                    customer.setCustomerConfirmation(customerConfirmation);
                                    customerRepository.save(customer);
                                    issueRewardAndEmails(customer);
                                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("CONFIRMED ID").build());
                                }

                                if (customer.isEnabled() && !customerConfirmation.isConfirmed()) {
                                    boolean needsManager = (((numberOfIDDeclined[0] + numberNullFront[0]) < duplicationSizeID[0]
                                            && (numberOfIDApproved[0] + numberNullFront[0]) < duplicationSizeID[0])
                                            && (numberOfIDApproved[0] < 2 || numberOfIDDeclined[0] < 2))
                                            || (((numberOfSelfieDeclined[0] + numberNullSelfie[0]) < duplicationSizeSELFIE[0]
                                            && (numberOfSelfieApproved[0] + numberNullSelfie[0]) < duplicationSizeSELFIE[0])
                                            && (numberOfSelfieApproved[0] < 2 || numberOfSelfieDeclined[0] < 2));
                                    if (needsManager) {
                                        duplicatedImages.add(DuplicatedImage
                                                .builder()
                                                .originalIDNumber(duplicatedImage.getOriginalIDNumber())
                                                .type(duplicatedImage.getType())
                                                .action(TestAction.FORKED)
                                                .by_who(mangerMemberID)
                                                .build());
                                        //basically it should update without setters, but we keep it in case
                                        customer.setDuplicatedImages(duplicatedImages);
                                        customerRepository.save(customer);
                                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("NEEDS MANAGER ON DUPLICATION").build());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return CustomResponseDto.ok();
    }

    public Object createOrUpdateManagers(ManagerOrAdminDto managerOrAdminDto, String memberIDCreator) {
        String partLog = "POST /admin/new method";
        try {
            boolean isAdmin = managerOrAdminDto.getAdmin();
            Customer customer = customerRepository.getCustomerByEmail(managerOrAdminDto.getEmail());
            if (customer != null) {
                customer.setAdmin(isAdmin);
                customer.setManager(!isAdmin);
                customer.setIdNumber(memberIDCreator);
                customer.setEnabled(true);
                customerRepository.save(customer);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(managerOrAdminDto.getEmail()).message("user updated").build());
            } else {
                String memberID = PasswordUtil.getMemberID();
                customerRepository.save(Customer
                        .builder()
                        .status(UserStatus.VERIFIED)
                        .admin(isAdmin)
                        .manager(!isAdmin)
                        .idNumber(memberIDCreator)
                        .email(managerOrAdminDto.getEmail())
                        .phone(managerOrAdminDto.getPhone())
                        .phoneVerified(true)
                        .balance(managerOrAdminDto.getBalance())
                        .lastName(managerOrAdminDto.getLastName())
                        .firstName(managerOrAdminDto.getFirstName())
                        .dateRecorded(SIMPLE_DATE_FORMAT.parse(SIMPLE_DATE_FORMAT.format(new Date())))
                        .memberID(memberID)
                        .build());
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(managerOrAdminDto.getEmail()).message("user created").build());
            }
        } catch (ParseException e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(managerOrAdminDto.getEmail()).message(e.getMessage()).build());
        }
        return CustomResponseDto.ok();
    }

    public Object disableManager(String email) {
        String partLog = "POST /deletemanager/{email} method";
        Customer customer = customerRepository.getCustomerByEmail(email);
        if (customer != null && !customer.isAdmin() && customer.isManager() && customer.isEnabled()) {
            customer.setEnabled(false);
            customerRepository.save(customer);
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(email).message(SUCCESS).build());
        }
        return CustomResponseDto.ok();
    }

    public Object getAllManagers(int page, int pageSize) {
        String partLog = "GET /list managers method";
        CustomerListRS response = CustomerListRS.builder().build();
        try {
            response.setCustomers(customerRepository.getCustomersByManagerIsTrue(PageRequest.of(page, pageSize)));
            response.setTotal(customerRepository.countCustomerByManagerIsTrue());
            response.setSuccess(true);
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail("list").message(SUCCESS).build());
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail("list").message(e.getMessage()).build());
            response.setSuccess(false);
        }
        return response;
    }

    public CustomerListRS getBlockedUsers(CustomerPaginationDto query) {
        CustomerListRS response = CustomerListRS.builder().build();
        String partLog = "POST /getBlockedUsers method";
        try {
            List<Customer> customers;
            if (query.getSearchTerm() != null && !query.getSearchTerm().isEmpty()) {
                customers = customerRepository.searchBlockedUsers(query.getSearchTerm(), PageRequest.of(query.getPage(), query.getPageSize()));
                response.setTotal(customerRepository.countBlockedUsers(query.getSearchTerm()));
            } else {
                customers = customerRepository.listAllBlockedUsers(PageRequest.of(query.getPage(), query.getPageSize()));
                response.setTotal(customerRepository.countAllBlockedUsers());
            }
            customers = customers.stream()
                    .filter(customer -> !customer.getAdminApprovals().contains(AuthenticationInterceptor.getLoggedInUserMemberId()))
                    .collect(Collectors.toList());
            response.setCustomers(customers);
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(query.toString()).message(SUCCESS).build());
            response.setSuccess(true);
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(query.toString()).message(e.getMessage()).build());
            response.setSuccess(false);
        }
        log.info("Response success: " + response.isSuccess());
        return response;
    }

    public ZCResponse getIdAndSelfieByUserMemberId(String idNumber) {
        String partLog = "GET /idAndSelfie/{idNumber} method";
        Customer user = customerRepository.getCustomerByIdNumber(idNumber);
        if (user == null) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(idNumber).message(USER_NOT_FOUND).build());
            return ZCResponse.builder().success(false).statusCode(StatusCode.USER_NOT_FOUND).status(USER_NOT_FOUND).build();
        }
        String documentType = user.getDocumentType().getType();
        String idLink = awsHttp.generateDownloadPreSignedUrl(idNumber.replaceAll("(CIT|REG|ALIEN)*", "")
                .replaceAll("\\s*", "") + (documentType.equalsIgnoreCase(DocumentType.ID_CARD.getType()) ? "" : documentType) + "_front.jpeg");
        String livenessLink = awsHttp.generateDownloadPreSignedUrl(idNumber.replaceAll("(CIT|REG|ALIEN)*", "")
                .replaceAll("\\s*", "") + "_liveness.jpeg");
        Map<String, String> imageLinks = new HashMap<>();
        imageLinks.put("idLink", idLink);
        imageLinks.put("livenessLink", livenessLink);
        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(idNumber).message(SUCCESS).build());
        return ZCResponse.builder().success(true).statusCode(HttpStatus.SC_OK).object(imageLinks).build();
    }

    public ZCResponse approveToUnblock(ApproveOrUnblockDto approveOrUnblockDto, String managerMemberID) {
        String partLog = "POST /approveOrUnblock method";
        Customer toBeApprovedUser = customerRepository.getCustomerByMemberID(approveOrUnblockDto.getMemberId());
        if (toBeApprovedUser == null) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(approveOrUnblockDto.getMemberId()).message(NOT_FOUND).build());
            return ZCResponse.builder().success(false).statusCode(StatusCode.USER_NOT_FOUND).status(USER_NOT_FOUND).build();
        }
        if (toBeApprovedUser.isEnabled()) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(toBeApprovedUser.getMemberID()).message("User is not blocked").build());
            return getInvalidResponse("User is not blocked");
        }
        Set<String> adminApprovals = toBeApprovedUser.getAdminApprovals();
        if (adminApprovals == null) {
            adminApprovals = new HashSet<>();
        }
        if (approveOrUnblockDto.getAction() == AdminAction.UNBLOCK) {
            unblockCustomer(toBeApprovedUser, approveOrUnblockDto.getAction());
            adminApprovals.clear();
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(toBeApprovedUser.getMemberID()).message(AdminAction.UNBLOCK.name()).build());
        } else if (approveOrUnblockDto.getAction() == AdminAction.APPROVE) {
            if (adminApprovals.contains(managerMemberID)) {
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(toBeApprovedUser.getMemberID()).message("Manager already approved for this user").build());
                return getInvalidResponse("Manager already approved for this user");
            }
            adminApprovals.add(managerMemberID);
            if (adminApprovals.size() > 1) {
                unblockCustomer(toBeApprovedUser, approveOrUnblockDto.getAction());
                adminApprovals.clear();
            } else {
                customerRepository.save(toBeApprovedUser);
                return ZCResponse.builder().success(true).statusCode(HttpStatus.SC_NO_CONTENT).build();
            }
        }
        return ZCResponse.builder().success(true).statusCode(HttpStatus.SC_OK).build();
    }

    private ZCResponse getInvalidResponse(String status) {
        return ZCResponse.builder().success(false).statusCode(HttpStatus.SC_BAD_REQUEST).status(status).build();
    }

    public Object getBannedDomains() {
        List<DomainBanned> bannedDomains = customerDomainBannedRepository.findAll();
        socketFacade.send(SocketBodyDto.builder().api("GET /banned-domains method").memberIDOrEmail("LIST").message(SUCCESS).build());
        return ZCResponse.builder().statusCode(HttpStatus.SC_OK).success(true).object(bannedDomains).build();
    }

    public Object reset(String memberID) {
        ZCResponse response = ZCResponse.builder().build();
        String partLog = "GET /reset() method -> memberID: " + memberID;
        try {
            Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
            if (customer != null) {
                CustomerConfirmation customerConfirmation = customer.getCustomerConfirmation();
                if (customerConfirmation != null) {
                    customerConfirmation.getTestFailsMachine().remove(TestType.TEST5);
                    customerConfirmation.getTestFailsMachine().remove(TestType.TEST1);
                    customer.setEnabled(true);
                    customer.setIdFailed(false);
                    customerConfirmation.setConfirmed(false);
                    customerConfirmation.getApproved().clear();
                    customerConfirmation.getDecline().clear();
                    customerConfirmation.getNotSure().clear();
                    customerConfirmation.setRequiredToUploadID(true);
                    customerConfirmation.setRequiredToUploadLiveness(true);
                    customerConfirmation.setAttempts(0);
                    customerConfirmation.setEligibleForManageDuplication(false);
                    customerRepository.save(customer);
                    amazonSES.sendResetConfirmation(customer.getEmail(), customer.getFirstName());
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(SUCCESS).build());
                    response.setStatusCode(200);
                    response.setSuccess(true);
                }
            }
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
            response.setObject(e.getMessage());
        }
        return response;
    }

    private void unblockCustomer(Customer customer, AdminAction adminAction) {
        try {
            customer.setEnabled(true);
            customer.setIdFailed(false);
            if (adminAction == AdminAction.UNBLOCK) {
                customer.setCustomerConfirmation(null);
            } else if (adminAction == AdminAction.APPROVE) {
                customer.getCustomerConfirmation().getTestFailsMachine().add(TestType.TEST5);
                customer.getCustomerConfirmation().setConfirmedByAdmin(true);
                customer.getCustomerConfirmation().setConfirmed(true);
                issueRewardAndEmails(customer);
            }
            customer.getDuplicatedImages().clear();
            kycService.deleteAttemptsAfterUnblock(customer.getMemberID());
            customerRepository.save(customer);
            JobKey emailJobKey = new JobKey("job_" + customer.getMemberID(), "email_" + customer.getMemberID());
            scheduler.deleteJob(emailJobKey);
            JobKey balanceJobKey = new JobKey("job_" + customer.getMemberID(), "reduce_" + customer.getMemberID());
            scheduler.deleteJob(balanceJobKey);
        } catch (SchedulerException e) {
            socketFacade.send(SocketBodyDto.builder().api("unblockCustomer method").memberIDOrEmail(customer.getMemberID()).message(e.getMessage()).build());
        }

    }
}
