package com.zimbocash.service;

import com.opencsv.CSVWriter;
import com.zimbocash.dao.CustomerConfirmationRepository;
import com.zimbocash.dao.CustomerOtpRepository;
import com.zimbocash.dao.CustomerRepository;
import com.zimbocash.dao.LoginAuditRepository;
import com.zimbocash.dao.TransactionTrackRepository;
import com.zimbocash.enums.TargetType;
import com.zimbocash.enums.TestType;
import com.zimbocash.enums.UserStatus;
import com.zimbocash.facade.ISocketFacade;
import com.zimbocash.model.DateRange;
import com.zimbocash.model.ManualReviewerDecision;
import com.zimbocash.model.dtos.EmailStatisticsDto;
import com.zimbocash.model.dtos.SocketBodyDto;
import com.zimbocash.model.entity.Customer;
import com.zimbocash.model.entity.CustomerConfirmation;
import com.zimbocash.model.entity.LoginAudit;
import com.zimbocash.model.entity.OTP;
import com.zimbocash.model.entity.TransactionTrack;
import com.zimbocash.model.entity.TypesList;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.zimbocash.util.Constants.SIMPLE_DATE_FORMAT;
import static com.zimbocash.util.Constants.SUCCESS;

@Slf4j
@AllArgsConstructor
@Service
public class AnalyticDashboardService {
    private final ISocketFacade socketFacade;
    private final CustomerRepository customerRepository;
    private final CustomerConfirmationRepository customerConfirmationRepository;
    private final LoginAuditRepository loginAuditRepository;
    private final CustomerOtpRepository customerOtpRepository;
    private final TransactionTrackRepository transactionTrackRepository;

    public Map<String, Object> getAccountTypeMap() {
        String partLog = "GET /api/dashboard/accountType method";
        List<CustomerConfirmation> all = customerConfirmationRepository.findAll();
        Integer oneAttempt = 0;
        Integer twoAttempt = 0;
        Integer threeAttempts = 0;
        Long verified = all.stream().filter(CustomerConfirmation::isConfirmed).count();
        Long failedId = all.stream()
                .filter(customerConfirmation -> customerConfirmation.getAttempts() > 0)
                .filter(customerConfirmation -> !customerConfirmation.isConfirmed())
                .count();
        for (CustomerConfirmation customerConfirmation : all) {
            switch (customerConfirmation.getAttempts()) {
                case 1:
                    oneAttempt++;
                    break;
                case 2:
                    twoAttempt++;
                    break;
                case 3:
                    threeAttempts++;
                    break;
            }
        }
        Map<String, Object> resultList = new LinkedHashMap<>();
        resultList.put("confirmed", verified);
        resultList.put("failedId", failedId);
        resultList.put("oneAttempt", oneAttempt);
        resultList.put("twoAttempt", twoAttempt);
        resultList.put("threeAttempts", threeAttempts);
        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail("LIST").message(SUCCESS).build());
        return resultList;
    }

    public Map<String, Object> getOtpTypeMap() {
        String partLog = "GET /api/dashboard/otp method";
        List<OTP> allOtps = customerOtpRepository.findAll();
        long verifiedWithPhone;
        int all = allOtps.size();
        verifiedWithPhone = allOtps.stream().filter(otp -> otp.getTarget() == TargetType.PHONE).count();
        Map<String, Object> response = new HashMap<>();
        response.put("all", all);
        response.put("phone", verifiedWithPhone);
        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail("LIST").message(SUCCESS).build());
        return response;
    }

    public Map<Object, Long> getUserVerifiedTypeMap(DateRange dateRange) {
        String partLog = "POST /api/dashboard/userVerification method";
        List<Customer> result = customerRepository.findCustomersInDateRange(dateRange);
        Map<Object, Long> resultList = new HashMap<>();
        long verifiedCount = result.stream().filter(customer -> customer.getStatus() == UserStatus.VERIFIED).count();
        resultList.put(UserStatus.VERIFIED, verifiedCount);
        resultList.put(UserStatus.PENDING, result.size() - verifiedCount);
        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(dateRange.toString()).message(String.valueOf(verifiedCount)).build());
        return resultList;
    }

    public Map<String, Object> getTransactionCountMap() {
        String partLog = "GET /api/dashboard/transaction method";
        List<TransactionTrack> transactions;
        transactions = transactionTrackRepository.findAll();
        Integer size = transactions.size();
        Double sum = transactions.stream().map(TransactionTrack::getAmount).reduce(0., Double::sum);
        Map<String, Object> response = new HashMap<>();
        response.put("count", size);
        response.put("sum", sum);
        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(String.valueOf(size)).message(SUCCESS).build());
        return response;
    }

    public Map<String, Long> getLoginAuditMap(DateRange dateRange) {
        String partLog = "POST /api/dashboard/login method";
        long successfulLogins;
        List<LoginAudit> all = loginAuditRepository.findLoginAuditsByDateRecordedBetween(dateRange.getStartDate(), dateRange.getEndDate());
        successfulLogins = all.stream().filter(loginAudit -> loginAudit.getLoginStatus() == HttpStatus.SC_OK).count();
        Map<String, Long> response = new HashMap<>();
        response.put("successful", successfulLogins);
        response.put("failed", all.size() - successfulLogins);
        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail("getLoginAuditMap").message(dateRange.toString()).build());
        return response;
    }

    public Map<String, Integer> getEmailWithCountMap(EmailStatisticsDto emailStatistics) {
        String partLog = "POST /api/dashboard/topEmails method";
        DateRange dateRange = emailStatistics.getDate();
        Integer limit = emailStatistics.getCount();
        List<String> popularEmails = Arrays.asList("@gmail.com", "@outlook.com", "@yahoo.com", "@hotmail.com", "@icloud.com", "@protonmail.com");
        List<String> resultList = customerRepository.getEmailsInDateRange(dateRange);
        List<String> collect = resultList.stream()
                .map(email -> email.substring(email.indexOf("@")))
                .filter(email -> !popularEmails.contains(email))
                .limit(limit)
                .collect(Collectors.toList());
        Map<String, Integer> emailAndCount = new HashMap<>();
        for (String s : collect) {
            if (emailAndCount.containsKey(s)) {
                Integer count = emailAndCount.get(s);
                emailAndCount.put(s, count + 1);
            } else {
                emailAndCount.put(s, 1);
            }
        }
        List<Map.Entry<String, Integer>> arrayAndCount = new ArrayList<>(emailAndCount.entrySet());
        arrayAndCount.sort((entry1, entry2) -> entry2.getValue() - entry1.getValue());
        Map<String, Integer> resultMap = new LinkedHashMap<>();
        arrayAndCount.forEach(entry -> resultMap.put(entry.getKey(), entry.getValue()));
        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail("getEmailWithCountMap").message(SUCCESS).build());
        return resultMap;
    }

    public ResponseEntity getCsv() {
        String partLog = "GET /api/dashboard/csv method";
        List<Customer> result = StreamSupport
                .stream(customerRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
        List<String[]> collect = result.stream().
                filter(customer -> customer.getStatus().equals(UserStatus.VERIFIED))
                .filter(Customer::isEnabled)
                .map(cus -> new String[]{cus.getEmail(), SIMPLE_DATE_FORMAT.format(cus.getDateRecorded())})
                .collect(Collectors.toList());

        File file = new File("emails.csv");
        try {
            file.createNewFile();
            writeToCsv(collect, file.toPath());
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail("getCsv").message(SUCCESS).build());
        } catch (Exception e) {
            log.error("Error while creating csv file, ", e);
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail("getCsv").message(e.getMessage()).build());
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().header("Content-Disposition", "attachment;filename=" + file.getName()).build();
    }

    public Map<String, Map<String, ManualReviewerDecision>> getManualReviewerStatisticMap() {
        List<Object[]> idAndEmails = customerRepository.getManagerIdAndEmails();
        Map<String, String> managerIdAndEmail = new HashMap<>();
        for (Object[] idAndEmail : idAndEmails) {
            managerIdAndEmail.put(String.valueOf(idAndEmail[0]), String.valueOf(idAndEmail[1]));
        }
        List<CustomerConfirmation> customerConfirmations = customerConfirmationRepository.findAll();
        Map<String, Map<String, ManualReviewerDecision>> managerAndDecisions = new HashMap<>();
        for (CustomerConfirmation customerConfirmation : customerConfirmations) {
            if (customerConfirmation.getApproved() != null && !customerConfirmation.getApproved().isEmpty()) {
                for (Map.Entry<String, TypesList> managerAndTypeList : customerConfirmation.getApproved().entrySet()) {
                    TypesList testTypes = managerAndTypeList.getValue();
                    String managerId = managerAndTypeList.getKey();
                    String managerEmail = managerIdAndEmail.get(managerId);
                    Map<String, ManualReviewerDecision> testAndDecision = managerAndDecisions.computeIfAbsent(managerEmail, k -> new HashMap<>());

                    for (TestType type : testTypes.getTestTypes()) {
                        if (!testAndDecision.containsKey(type.getType())) {
                            testAndDecision.put(type.getType(), new ManualReviewerDecision(0, 0, 1));
                            managerAndDecisions.put(managerEmail, testAndDecision);
                        } else {
                            testAndDecision.get(type.getType()).addToApproved();
                        }
                    }

                }
            }
            if (customerConfirmation.getDecline() != null && !customerConfirmation.getDecline().isEmpty()) {
                for (Map.Entry<String, TypesList> managerAndTypeList : customerConfirmation.getDecline().entrySet()) {
                    TypesList testType = managerAndTypeList.getValue();
                    String managerId = managerAndTypeList.getKey();
                    String managerEmail = managerIdAndEmail.get(managerId);
                    Map<String, ManualReviewerDecision> testAndDecision = managerAndDecisions.computeIfAbsent(managerEmail, k -> new HashMap<>());

                    for (TestType type : testType.getTestTypes()) {
                        if (!testAndDecision.containsKey(type.getType())) {
                            testAndDecision.put(type.getType(), new ManualReviewerDecision(1, 0, 0));
                            managerAndDecisions.put(managerEmail, testAndDecision);
                        } else {
                            testAndDecision.get(type.getType()).addToFailed();
                        }
                    }

                }
            }
            if (customerConfirmation.getNotSure() != null && !customerConfirmation.getNotSure().isEmpty()) {
                for (Map.Entry<String, TypesList> managerAndTypeList : customerConfirmation.getNotSure().entrySet()) {
                    TypesList testType = managerAndTypeList.getValue();
                    String managerId = managerAndTypeList.getKey();
                    String managerEmail = managerIdAndEmail.get(managerId);
                    Map<String, ManualReviewerDecision> testAndDecision = managerAndDecisions.computeIfAbsent(managerEmail, k -> new HashMap<>());
                    for (TestType type : testType.getTestTypes()) {
                        if (!testAndDecision.containsKey(type.getType())) {
                            testAndDecision.put(type.getType(), new ManualReviewerDecision(0, 1, 0));
                            managerAndDecisions.put(managerEmail, testAndDecision);
                        } else {
                            testAndDecision.get(type.getType()).addToNotSure();
                        }
                    }

                }

            }
        }
        socketFacade.send(SocketBodyDto.builder().api("GET /api/dashboard/manualReview method").memberIDOrEmail("getManualReviewerStatisticMap").message(String.valueOf(managerAndDecisions.size())).build());
        return managerAndDecisions;
    }

    private void writeToCsv(List<String[]> stringArray, Path path) throws Exception {
        CSVWriter writer = new CSVWriter(new FileWriter(path.toString()));
        for (String[] strings : stringArray) {
            writer.writeNext(strings);
        }
        writer.close();
    }
}
