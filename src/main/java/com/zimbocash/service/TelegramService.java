package com.zimbocash.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zimbocash.commons.CustomResponseDto;
import com.zimbocash.dao.CustomerRepository;
import com.zimbocash.dao.TelegramRepository;
import com.zimbocash.enums.MissedTransactionType;
import com.zimbocash.external.TelegramClient;
import com.zimbocash.model.ZCResponse;
import com.zimbocash.model.entity.Customer;
import com.zimbocash.model.entity.CustomerConfirmation;
import com.zimbocash.model.entity.CustomerReward;
import com.zimbocash.model.entity.CustomerTransaction;
import com.zimbocash.model.entity.Telegram;
import com.zimbocash.util.StatusCode;
import com.zimbocash.util.TronServiceUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Random;

import static com.zimbocash.util.Constants.DB_ERROR_OCCURED;
import static com.zimbocash.util.Constants.MSG_INVALID_MEMBER_ID;
import static com.zimbocash.util.Constants.SUCCESS;
import static com.zimbocash.util.Constants.TELEGRAM_REWARD;
import static com.zimbocash.util.Constants.USER_NOT_FOUND;

@Component
@RequiredArgsConstructor
@Slf4j
//todo add socket info if we need telegram later
public class TelegramService {
    public static String botToken;
    private final CustomerRepository customerRepository;
    private final TelegramRepository telegramRepository;
    private final TronServiceUtil tronServiceUtil;
    private final TelegramClient telegramClient;
    @Value("${tron.tokenID}")
    String assetName;

    public CustomResponseDto requestTelegram(JsonNode jsonNode) {
        CustomResponseDto customResponseDto = CustomResponseDto.builder().build();
        try {
            if (jsonNode.has("message")) {
                JsonNode telegramMessage = jsonNode.get("message");
                if (telegramMessage.has("text") && telegramMessage.has("chat")) {
                    String userData = telegramMessage.get("text").asText().toLowerCase();
                    if (telegramMessage.get("chat").has("id")) {
                        String message;
                        long chatId = telegramMessage.get("chat").get("id").longValue();
                        switch (userData) {
                            case "/start":
                                message = "Hi, I’m the ZASH Admin Bot. I’m here to make your account at ZIMBOCASH more secure. \n" +
                                        "\n" +
                                        "a.\tWhenever you make a transaction, I’ll send you a one time pin. \n" +
                                        "b.\tI’ll keep you posted whenever someone logs into your account. \n" +
                                        "c.\tTo change your password, you’ll need a one time pin.\n" +
                                        "\n" +
                                        "If you need help at any stage, enter /Help\n" +
                                        "\n" +
                                        "I’ll need to quickly confirm your account details. Please enter your ZIMBOCASH account number below.\n";
                                break;
                            case "/help":
                                message = "Send /reset to change account id";
                                break;
                            case "/reset":
                                message = processReset(chatId);
                                break;
                            default:
                                message = processAccountID(chatId, userData);
                                break;
                        }

                        if (message != null) {
                            telegramClient.sendMessage(botToken, String.valueOf(chatId), message);
                            customResponseDto.setSuccess(true);
                        }
                    }
                }
            }
            if (!customResponseDto.isSuccess()) {
                customResponseDto.setMessage("invalid json body");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            customResponseDto.setMessage(e.getMessage());
        }
        return customResponseDto;
    }

    private String processReset(long chatId) {
        String message = "Your telegram account was reset. Please send your account ID";
        try {
            Telegram telegram = telegramRepository.findTelegramByChatID(chatId).orElse(null);
            if (telegram != null) {
                telegramRepository.deleteByChatID(chatId);
            } else {
                message = MSG_INVALID_MEMBER_ID;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            message = "Internal error occurred! please try later";
        }
        return message;
    }

    private String processAccountID(long chatId, String userData) {
        String message = MSG_INVALID_MEMBER_ID;
        try {
            if (validateInput(userData)) {
                Customer customer = customerRepository.findCustomerByMemberID(userData).orElse(null);
                if (customer != null) {
                    Telegram telegram = customer.getTelegram();
                    if (telegram == null) {
                        message = getFirstOTP(chatId, userData);
                    } else {
                        if (telegram.getChatID() == chatId) {
                            message = "Your OTP is: " + telegram.getOtp();
                        } else {
                            if (telegram.isVerified()) {
                                return "Your telegram account is already verified!";
                            }
                            customer.setTelegram(null);
                            customerRepository.save(customer);
                            telegramRepository.deleteByChatID(chatId);
                            message = getFirstOTP(chatId, userData);
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            message = "Internal error occurred! Please try later";
        }
        return message;
    }

    private String getFirstOTP(long chatId, String memberID) {
        try {
            Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
            if (customer != null) {
                int otp = new Random().nextInt(900000) + 100000;
                Telegram telegram = Telegram.builder().chatID(chatId).issueDate(new Date()).otp(otp).build();
                customer.setTelegram(telegram);
                customerRepository.save(customer);
                return "Your OTP is: " + otp;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            return "Internal error occurred! Please try later";
        }
        return MSG_INVALID_MEMBER_ID;
    }

    //@CacheResult(cacheName = "validInput")
    public boolean validateInput(String input) {
        return input.length() > 3 && input.length() < 17 && input.matches("[a-z0-9]*");
    }

    public ZCResponse sendTelegramOTP(String memberID) {
        ZCResponse response = ZCResponse.builder().build();
        if (memberID != null) {
            try {
                Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
                if (customer != null) {
                    Telegram telegram = customer.getTelegram();
                    if (telegram != null && !telegram.isVerified()) {
                        if (telegram.getIssueDate() != null && new Date().getTime() - telegram.getIssueDate().getTime() < 60000) {
                            response.setStatusCode(101);
                        } else {
                            int otp = new Random().nextInt(900000) + 100000;
                            telegram.setOtp(otp);
                            telegram.setIssueDate(new Date());
                            customer.setTelegram(telegram);
                            customerRepository.save(customer);
                            telegramClient.sendMessage(botToken, String.valueOf(telegram.getChatID()), "Your OTP is: " + otp);
                            response.setStatus(SUCCESS);
                            response.setStatusCode(StatusCode.SUCCESS);
                            response.setSuccess(true);
                        }
                    } else {
                        response.setStatusCode(121);
                    }
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                response.setStatus(DB_ERROR_OCCURED);
                response.setStatusCode(105);
            }
        } else {
            response.setStatus(USER_NOT_FOUND);
            response.setStatusCode(StatusCode.USER_NOT_FOUND);
        }
        log.info("Response status code: " + response.getStatusCode());
        return response;
    }

    public ZCResponse disableTelegram(String memberID) {
        ZCResponse response = ZCResponse.builder().build();
        if (memberID != null) {
            try {
                Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
                if (customer != null) {
                    Telegram telegram = customer.getTelegram();
                    if (telegram != null) {
                        customer.setTelegram(null);
                        customerRepository.save(customer);
                        telegramRepository.deleteByChatID(telegram.getChatID());
                        response.setStatus(SUCCESS);
                        response.setStatusCode(StatusCode.SUCCESS);
                        response.setSuccess(true);
                    } else {
                        response.setStatusCode(121);
                    }
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                response.setStatus(DB_ERROR_OCCURED);
                response.setStatusCode(105);
            }
        } else {
            response.setStatus(USER_NOT_FOUND);
            response.setStatusCode(StatusCode.USER_NOT_FOUND);
        }
        log.info("Response status code: " + response.getStatusCode());
        return response;
    }

    public ZCResponse verifyTelegramOTP(String otp, String memberID) {
        ZCResponse response = ZCResponse.builder().build();
        if (memberID != null) {
            try {
                Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
                if (customer != null) {
                    Telegram telegram = customer.getTelegram();
                    CustomerConfirmation customerConfirmation = customer.getCustomerConfirmation();
                    CustomerReward customerReward = customer.getCustomerReward();
                    if (telegram != null && !telegram.isVerified() && customerReward != null) {
                        if (telegram.getIssueDate() == null) {
                            response.setStatus("NO_OTP");
                            response.setStatusCode(104);
                        } else if (new Date().getTime() - telegram.getIssueDate().getTime() > 300000) {
                            response.setStatus("EXPIRED_OTP");
                            response.setStatusCode(103);
                        } else if (Long.parseLong(otp) != telegram.getOtp()) {
                            response.setStatus("WRONG_OTP");
                            response.setStatusCode(102);
                        } else {
                            CustomerTransaction customerTransaction = customer.getCustomerTransaction();
                            if (!telegram.isVerified()) {
                                telegram.setVerified(true);
                                customer.setTelegram(telegram);
                                if (customerConfirmation != null && customerTransaction != null && customerConfirmation.isConfirmed() && customerReward.getTelegramReward() == 0) {
                                    //add 100 zash to user who verify their telegram
                                    String txID = tronServiceUtil
                                            .issueOrTransfer(null, customerTransaction.getTronAccountAddress(), TELEGRAM_REWARD, assetName);
                                    if (txID != null) {
                                        //add 100 zash to their balance, it's already in blockchain address, just update the db and cache
                                        customer.setBalance(customer.getBalance() + TELEGRAM_REWARD);
                                        customerReward.setTelegramReward(TELEGRAM_REWARD);
                                        customer.setCustomerReward(customerReward);
                                        telegramClient.sendMessage(botToken,
                                                String.valueOf(telegram.getChatID()),
                                                "Great, you just linked your telegram account with your ZIMBOCASH account.");
                                        response.setStatus(SUCCESS);
                                        response.setStatusCode(200);
                                        response.setSuccess(true);
                                    } else {
                                        response.setStatus(DB_ERROR_OCCURED);
                                        response.setStatusCode(121);
                                        tronServiceUtil.saveMissedTransaction(MissedTransactionType.HOT_WALLET.getStatus(),
                                                customerTransaction.getTronAccountAddress(), TELEGRAM_REWARD, memberID);
                                    }
                                } else {
                                    response.setStatus(SUCCESS);
                                    response.setStatusCode(200);
                                    response.setSuccess(true);
                                }
                                customerRepository.save(customer);
                            } else {
                                response.setStatus("TELEGRAM_ALREADY_VERIFIED");
                                response.setStatusCode(106);
                            }
                        }
                    } else {
                        response.setStatus("NOT_ALLOWED_TO_VERIFY_TELEGRAM");
                        response.setStatusCode(105);
                    }
                } else {
                    response.setStatus(USER_NOT_FOUND);
                    response.setStatusCode(119);
                }

            } catch (Exception e) {
                log.error(e.getMessage(), e);
                response.setStatus(DB_ERROR_OCCURED);
                response.setStatusCode(105);
            }
        } else {
            response.setStatus(USER_NOT_FOUND);
            response.setStatusCode(119);
        }
        log.info("Response status code: " + response.getStatusCode());
        return response;
    }
}
