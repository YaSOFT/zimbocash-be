package com.zimbocash.service;

import com.zimbocash.commons.CustomResponseDto;
import com.zimbocash.commons.InterceptorAuthService;
import com.zimbocash.dao.CustomerOtpRepository;
import com.zimbocash.dao.CustomerRepository;
import com.zimbocash.dao.CustomerTransactionRepository;
import com.zimbocash.enums.MissedTransactionType;
import com.zimbocash.enums.TargetType;
import com.zimbocash.external.AmazonSES;
import com.zimbocash.external.impl.InternalRestClient;
import com.zimbocash.facade.ICustomerFacade;
import com.zimbocash.facade.ISocketFacade;
import com.zimbocash.model.ZCResponse;
import com.zimbocash.model.dtos.CryptoDto;
import com.zimbocash.model.dtos.SocketBodyDto;
import com.zimbocash.model.dtos.TransferDto;
import com.zimbocash.model.entity.Customer;
import com.zimbocash.model.entity.CustomerConfirmation;
import com.zimbocash.model.entity.CustomerReward;
import com.zimbocash.model.entity.CustomerTransaction;
import com.zimbocash.model.entity.OTP;
import com.zimbocash.model.entity.Telegram;
import com.zimbocash.scheduler.InitiatorScheduler;
import com.zimbocash.util.TronServiceUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import static com.zimbocash.util.Constants.DECIMAL_RATIO;
import static com.zimbocash.util.Constants.FAIL;
import static com.zimbocash.util.Constants.FIRST_PAYMENT_REWARD;
import static com.zimbocash.util.Constants.INTERNAL_ERROR;
import static com.zimbocash.util.Constants.LIMITS;
import static com.zimbocash.util.Constants.PHONE_NUMBER_VERIFIED_ALREADY;
import static com.zimbocash.util.Constants.RECEIVER_BLOCKED;
import static com.zimbocash.util.Constants.RECEIVER_LIMIT_REACHED;
import static com.zimbocash.util.Constants.SENDER_LIMIT_REACHED;
import static com.zimbocash.util.Constants.SUCCESS;
import static com.zimbocash.util.Constants.TELEGRAM_REWARD;
import static com.zimbocash.util.Constants.TRANSACTION_FAIL;
import static com.zimbocash.util.Constants.TRON_NOT_FOUND;
import static com.zimbocash.util.Constants.USER_NOT_FOUND;
import static com.zimbocash.util.Constants.USER_RECEIVER_NOT_FOUND;

@Component
@RequiredArgsConstructor
@Slf4j
public class TransactionService {
    public static volatile String tronWalletAddress;
    private final AmazonSES amazonSES;
    private final InternalRestClient internalRestClient;
    private final CustomerRepository customerRepository;
    private final CustomerOtpRepository customerOtpRepository;
    private final CustomerTransactionRepository customerTransactionRepository;
    private final TronServiceUtil tronServiceUtil;
    private final ISocketFacade socketFacade;
    @Value("${tron.tokenID}")
    String assetName;
    @Value("${email.port}")
    String emailPort;
    @Value("${email.host}")
    String emailHost;

    public ZCResponse sendOTP(String memberID, String phone) {
        log.info("sendSMS -> {} method", phone);
        ZCResponse response = ZCResponse.builder().build();
        String partLog = "POST /api/validation/sendOTP method";
        try {
            Customer customer = customerRepository.findCustomerByMemberIDAndPhoneAndPhoneVerifiedIsFalse(memberID, phone).orElse(null);
            final int[] otpLessThanDay = {0};
            if (customer != null) {
                Date currentDate = new Date();
                final int[] countOTPSPerDay = {0};
                boolean foundLessMinuteOrLimitReached = customer.getOtps().stream().filter(otp -> otp.getTarget().equals(TargetType.PHONE)).anyMatch(otp -> {
                    Date otpIssueDate = otp.getIssueDate();
                    //4 sms per day
                    //similarity in otp, same day same sms will be sent, i.e 123456 , 123456 , 123456, 123456
                    // Tue 123456 != WED 000000 , 000000, 000000, 000000
                    //user in WED try to verify otp with 123456 = error, if he try with 000000 then ok

                    if ((currentDate.getTime() - otpIssueDate.getTime()) < (60 * 1000)) {
                        response.setStatus("LESS_THAN_MINUTE");
                        response.setStatusCode(101);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("LESS_THAN_MINUTE").build());
                        return true;
                    }
                    if ((currentDate.getTime() - otpIssueDate.getTime()) < (24 * 60 * 60 * 1000)) {
                        otpLessThanDay[0] = otp.getOtp();
                        ++countOTPSPerDay[0];
                    } else {
                        customerOtpRepository.deleteById(otp.getId());
                    }
                    if (countOTPSPerDay[0] > 3) {
                        response.setStatus("LIMIT_REACHED_4_SMS_PER_DAY");
                        response.setStatusCode(102);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("LIMIT_REACHED_4_SMS_PER_DAY").build());
                        return true;
                    }
                    return false;
                });

                if (foundLessMinuteOrLimitReached) {
                    return response;
                }

                int otp = (otpLessThanDay[0] == 0) ? (new Random().nextInt(900000) + 100000) : otpLessThanDay[0];
                String message = "Your ZIMBOCASH one time pin is " + otp;
                //uncomment for test
//                CustomResponseDto customResponseDto = CustomResponseDto.ok();
//                comment for test
                CustomResponseDto customResponseDto = internalRestClient.publishSms(message, phone);
                if (!customResponseDto.isSuccess()) {
                    response.setStatusCode(customResponseDto.getStatusCode());
                    response.setStatus(customResponseDto.getMessage());
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(customResponseDto.getMessage()).build());
                } else {
                    customerOtpRepository.save(OTP.builder().issueDate(currentDate).target(TargetType.PHONE).otp(otp).customer(customer).build());
                    response.setStatus(SUCCESS);
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(SUCCESS).build());
                    response.setStatusCode(200);
                    response.setSuccess(true);
                }
                log.info("Response status code: " + response.getStatusCode());
            } else {
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(customer != null ? "phone not same" : USER_NOT_FOUND).build());
            }
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
            log.error(e.getMessage());
        }
        return response;
    }

    public ZCResponse verifyOTP(String phone, int otp) {
        log.info("verifyOTP() method");
        String partLog = "POST /api/validation/verifyotp method";
        ZCResponse response = ZCResponse.builder().build();
        try {
            List<OTP> otpObjectList = customerOtpRepository.findOTPSByOtpAndTarget(otp, TargetType.PHONE);
            if (otpObjectList.isEmpty()) {
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(phone).message("NO_RECORDS_FOUND").build());
                response.setStatus("NO_RECORDS_FOUND");
                response.setStatusCode(101);
            } else {
                OTP otpObject = otpObjectList.stream().max(Comparator.comparing(OTP::getIssueDate)).get();
                Customer customer = otpObject.getCustomer();
                if (customer != null) {
                    InterceptorAuthService.evaluate(customer.getMemberID());
                    //max 3 days for otp, and use the last otp sent (handled from sendotp already)
                    if ((new Date().getTime() - otpObject.getIssueDate().getTime()) > 3 * 24 * 60 * 60 * 1000) {
                        response.setStatus("EXPIRED_OTP");
                        response.setStatusCode(103);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(phone).message("EXPIRED_OTP").build());
                        customerOtpRepository.deleteById(otpObject.getId());
                    } else {
                        if (!Optional.ofNullable(customer.getPhone()).orElse("").equalsIgnoreCase(phone)) {
                            response.setStatus(FAIL);
                            response.setStatusCode(202);
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(phone).message("PHONE NOT SAME AS USER REGISTERED PHONE").build());
                        } else if (!customer.isPhoneVerified()) {
                            CustomerTransaction customerTransaction = customer.getCustomerTransaction();
                            if (customerTransaction == null) {
                                final CryptoDto[] cryptoDto = new CryptoDto[1];
                                if (InitiatorScheduler.counterRemainingReserved > 0) {
                                    //if the reserved exist then we use it, otherwise we create new account
                                    customerTransaction = customerTransactionRepository.findFirst1ByReservedFalse().orElse(null);
                                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(phone)
                                            .message("using reserved tron accounts ").build());
                                } else {
                                    cryptoDto[0] = tronServiceUtil.createAccount();
                                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(phone)
                                            .message("creating new tron account i.e reserved consumed").build());
                                }

                                if ((cryptoDto[0] != null && cryptoDto[0].getTronAddress() != null) || customerTransaction != null) {
                                    if (customerTransaction != null) {
                                        customerTransaction.setReserved(true);
                                        --InitiatorScheduler.counterRemainingReserved;
                                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(phone)
                                                .message("remaining tron accounts reserved " + InitiatorScheduler.counterRemainingReserved).build());
                                    } else {
                                        customerTransaction = CustomerTransaction
                                                .builder()
                                                .tronAccountAddress(cryptoDto[0].getTronAddress())
                                                .tronPublicKey(cryptoDto[0].getPublicKey())
                                                .build();
                                    }
                                    customer.setCustomerTransaction(customerTransaction);
                                    customer.setPhoneVerified(true);
                                    customer = customerRepository.save(customer);
                                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(phone)
                                            .message(SUCCESS).build());
                                    //remove all other otps objects and keep this one in db
                                    customerOtpRepository.deleteMany(otp, customer.getId(), otpObject.getId(), TargetType.PHONE);
                                    response.setObject(customerTransaction.getTronAccountAddress());
                                    response.setStatus(SUCCESS);
                                    response.setStatusCode(200);
                                    response.setSuccess(true);
                                } else {
                                    response.setStatus(INTERNAL_ERROR);
                                    response.setStatusCode(500);
                                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(phone)
                                            .message("cannot create account or use reserved").build());
                                }
                            } else {
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(phone)
                                        .message("customer already have tron account").build());
                                customer.setPhoneVerified(true);
                                response.setObject(customerTransaction.getTronAccountAddress());
                                customer = customerRepository.save(customer);
                                //remove all other otps objects and keep this one in db
                                customerOtpRepository.deleteMany(otp, customer.getId(), otpObject.getId(), TargetType.PHONE);
                                response.setStatus(SUCCESS);
                                response.setStatusCode(200);
                                response.setSuccess(true);
                            }
                        } else {
                            response.setStatus(PHONE_NUMBER_VERIFIED_ALREADY);
                            response.setStatusCode(202);
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(phone)
                                    .message(PHONE_NUMBER_VERIFIED_ALREADY).build());
                            //remove all other otps objects and keep this one in db
                            customerOtpRepository.deleteMany(otp, customer.getId(), otpObject.getId(), TargetType.PHONE);
                        }
                    }
                } else {
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(phone).message(USER_NOT_FOUND).build());
                }
            }

            log.info("Response status code: " + response.getStatusCode());

        } catch (Exception throwables) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(phone).message(throwables.getMessage()).build());
            if (throwables instanceof ResponseStatusException) {
                throw throwables;
            }
            log.error(throwables.getMessage());
        }
        return response;
    }

    public ZCResponse transfer(TransferDto transferDto, String memberID) {
        log.info("transfer() method");
        String partLog = "POST /api/validation/progress method";
        ZCResponse response = ZCResponse.builder().build();
        if (transferDto.getMemberId().equalsIgnoreCase("memberID")) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("you can't send to your self!").build());
            response.setStatus(USER_RECEIVER_NOT_FOUND);
            response.setStatusCode(112);
            return response;
        }
        try {
            Double amount = transferDto.getAmount();
            Customer sender = customerRepository.findCustomerByMemberID(memberID).orElse(null);
            if (sender != null) {
                double TRANSFER_FEES = 0.0d;
                //0.3% from amount
                if (amount >= 0.000334) {
                    TRANSFER_FEES = (amount * 0.3) / 100;
                }
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("TRANSFER_FEES\t" + TRANSFER_FEES).build());

                CustomerTransaction customerTransactionSender = sender.getCustomerTransaction();
                CustomerConfirmation customerConfirmationSender = sender.getCustomerConfirmation();
                if (customerTransactionSender != null && sender.isPhoneVerified()) {
                    if ((sender.isNewUser()
                            && ((sender.getLimitSending() + amount) <= LIMITS || (customerConfirmationSender != null && customerConfirmationSender.isConfirmed())))
                            ||
                            (!sender.isNewUser() && (customerConfirmationSender != null && customerConfirmationSender.isConfirmed()))) {
                        double senderBalance = tronServiceUtil.getBalance(customerTransactionSender.getTronAccountAddress(), assetName);
                        if (senderBalance >= (amount + TRANSFER_FEES)) {
                            Customer receiver = customerRepository.findCustomerByMemberID(transferDto.getMemberId()).orElse(null);
                            if (receiver != null) {
                                CustomerReward receiverCustomerReward = receiver.getCustomerReward();
                                //old users (sender and receiver) should not access here till they verified their ID,
                                //new users may do it till the limit reached
                                CustomerTransaction customerTransactionReceiver = receiver.getCustomerTransaction();
                                CustomerConfirmation customerConfirmationReceiver = receiver.getCustomerConfirmation();
                                if (receiverCustomerReward != null && customerTransactionReceiver != null && receiver.isPhoneVerified()) {
                                    if (receiver.isEnabled() && !receiver.isIdFailed()) {
                                        if (!receiver.isManager() && !receiver.isAdmin() && ((receiver.isNewUser()
                                                && ((receiver.getLimitReceive() + amount) <= LIMITS
                                                || (customerConfirmationReceiver != null && customerConfirmationReceiver.isConfirmed())))
                                                ||
                                                (!receiver.isNewUser() && customerConfirmationReceiver != null && customerConfirmationReceiver.isConfirmed()))) {

                                            String transactionID = tronServiceUtil.issueOrTransfer(customerTransactionSender.getTronPublicKey(),
                                                    customerTransactionReceiver.getTronAccountAddress(), amount, assetName);

                                            if (transactionID != null) {
                                                if (TRANSFER_FEES > 0) {
                                                    String transactionIDFees = tronServiceUtil
                                                            .issueOrTransfer
                                                                    (customerTransactionSender.getTronPublicKey(), tronWalletAddress, TRANSFER_FEES, assetName);
                                                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID)
                                                            .message("transactionIDFees\t" + transactionIDFees + " , fees took from user wallet").build());
                                                    if (transactionIDFees == null) {
                                                        tronServiceUtil.saveMissedTransaction(customerTransactionSender.getTronPublicKey(),
                                                                tronWalletAddress, TRANSFER_FEES, null);
                                                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID)
                                                                .message("cannot take fees from user wallet").build());
                                                    }
                                                }
                                                amazonSES.sendSenderEmail((customerConfirmationSender != null && customerConfirmationSender.isConfirmed() ?
                                                        sender.getFirstName() + " " + sender.getLastName() : ""), sender.getEmail(), amount, transactionID, transferDto.getPaymentMessage(), receiver.getMemberID());

                                                sender.setBalance((senderBalance - ((amount + TRANSFER_FEES) * DECIMAL_RATIO)) / DECIMAL_RATIO);
                                                sender.setLimitSending(sender.getLimitSending() + amount);
                                                receiver.setLimitReceive(receiver.getLimitReceive() + amount);
                                                customerRepository.save(sender);

                                                amazonSES.sendReceiverEmail((customerConfirmationReceiver != null && customerConfirmationReceiver.isConfirmed() ?
                                                        receiver.getFirstName() : ""), receiver.getEmail(), amount, transactionID, transferDto.getPaymentMessage(), (sender.getFirstName() != null ?
                                                        sender.getFirstName() : sender.getPhone()));

                                                receiver.setBalance((tronServiceUtil
                                                        .getBalance(customerTransactionReceiver.getTronAccountAddress(), assetName)) / DECIMAL_RATIO);
                                                customerRepository.save(receiver);
                                                processFirstReceive(sender, receiver);
                                                response.setObject(transactionID);
                                                response.setSuccess(true);
                                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID)
                                                        .message(SUCCESS).build());
                                                response.setStatusCode(200);
                                            } else {
                                                response.setStatus(TRANSACTION_FAIL);
                                                response.setStatusCode(105);
                                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID)
                                                        .message("CANNOT TRANSFER FROM " + customerTransactionSender.getTronAccountAddress() + " to " + customerTransactionReceiver.getTronAccountAddress()).build());
                                            }
                                        } else {
                                            response.setStatus(RECEIVER_LIMIT_REACHED);
                                            response.setStatusCode(123);
                                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(RECEIVER_LIMIT_REACHED).build());
                                        }
                                    } else {
                                        response.setStatus(RECEIVER_BLOCKED);
                                        response.setStatusCode(124);
                                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(RECEIVER_BLOCKED).build());
                                    }
                                } else {
                                    response.setStatus(TRON_NOT_FOUND);
                                    response.setStatusCode(110);
                                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("receiver cant receive phone : " + receiver.isPhoneVerified() + " or reward is null or doesn't have tron account").build());
                                }
                            } else {
                                response.setStatus(USER_RECEIVER_NOT_FOUND);
                                response.setStatusCode(112);
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(USER_RECEIVER_NOT_FOUND).build());
                            }

                        } else {
                            response.setStatus("NOT_ENOUGH_FUNDS");
                            response.setStatusCode(111);
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("NOT_ENOUGH_FUNDS").build());
                        }
                    } else {
                        response.setStatus(SENDER_LIMIT_REACHED);
                        response.setStatusCode(125);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(SENDER_LIMIT_REACHED).build());
                    }
                } else {
                    response.setStatus(TRON_NOT_FOUND);
                    response.setStatusCode(110);
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("sender cant receive phone : " + sender.isPhoneVerified() + " or reward is null or doesn't have tron account").build());
                }
            } else {
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(USER_NOT_FOUND).build());
                response.setStatus(USER_NOT_FOUND);
                response.setStatusCode(119);
            }
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
            log.error(e.getMessage(), e);
            response.setStatus(INTERNAL_ERROR);
            response.setStatusCode(500);
        }

        log.info("Response status code: " + response.getStatusCode());
        return response;
    }


    private void processFirstReceive(Customer sender, Customer receiver) {
        String partLog = "internal /api/validation/processFirstReceive method";
        if (sender != null && receiver != null) {
            CustomerConfirmation customerConfirmationReceiver = receiver.getCustomerConfirmation();
            CustomerTransaction customerTransactionReceiver = receiver.getCustomerTransaction();
            CustomerReward receiverCustomerReward = receiver.getCustomerReward();
            CustomerTransaction customerTransaction = sender.getCustomerTransaction();
            CustomerConfirmation customerConfirmation = sender.getCustomerConfirmation();
            //receiverCustomerReward is not null, as it's checked from transfer
            if (customerTransaction != null && customerTransactionReceiver != null) {
                //if user a is verified
                if (customerConfirmation != null && customerConfirmation.isConfirmed()) {
                    //if user b is verified
                    if (customerConfirmationReceiver != null && customerConfirmationReceiver.isConfirmed()) {
                        //if the receiver reward table not yet filled by the sender first time payment, i.e user a not yet received first time reward payment
                        if (receiverCustomerReward.getFirstPaymentRewardIssued() == 0 && receiverCustomerReward.getSenderFirstTimePayment() == null) {
                            //in this case make the transfer
                            String txID =
                                    tronServiceUtil.issueOrTransfer(null, customerTransaction.getTronAccountAddress(), FIRST_PAYMENT_REWARD, assetName);
                            receiverCustomerReward.setFirstPaymentRewardIssued(FIRST_PAYMENT_REWARD);
                            receiverCustomerReward.setSenderFirstTimePayment(sender.getMemberID());
                            receiver.setCustomerReward(receiverCustomerReward);
                            customerRepository.save(receiver);
                            if (txID != null) {
                                sender.setBalance(sender.getBalance() + FIRST_PAYMENT_REWARD);
                                customerRepository.save(sender);
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(sender.getMemberID()).message("FIRST_PAYMENT_REWARD " + FIRST_PAYMENT_REWARD).build());
                                amazonSES.sendFirstPaymentRewardToSender(sender.getEmail(), sender.getFirstName(), sender.getMemberID());
                            } else {
                                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(sender.getMemberID()).message("FIRST_PAYMENT_REWARD " + FIRST_PAYMENT_REWARD + " missing transaction").build());
                                tronServiceUtil.saveMissedTransaction(MissedTransactionType.HOT_WALLET.getStatus(),
                                        customerTransaction.getTronAccountAddress(),
                                        FIRST_PAYMENT_REWARD, sender.getMemberID());
                            }
                        } else {
                            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(sender.getMemberID()).message("receiver already got FIRST_PAYMENT_REWARD").build());
                        }
                        //else don't do anything as already received or is not the first one that he make payment to the receiver
                    } else {
                        receiverCustomerReward.setSenderFirstTimePayment(sender.getMemberID());
                        receiver.setCustomerReward(receiverCustomerReward);
                        customerRepository.save(receiver);
                        socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(sender.getMemberID()).message("receiver is not confirmed").build());
                    }
                } else {
                    receiverCustomerReward.setSenderFirstTimePayment(sender.getMemberID());
                    receiver.setCustomerReward(receiverCustomerReward);
                    customerRepository.save(receiver);
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(sender.getMemberID()).message("sender is not confirmed").build());
                }
            } else {
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(sender.getMemberID()).message("sender and/or receiver tron accounts are null").build());
            }
        } else {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail("FIRST RECEIVE").message("sender and/or receiver are null").build());
        }
    }

    public ZCResponse getBalance(String memberID) {
        String partLog = "GET /api/validation/get2 method";
        log.info("getBalance() method");
        ZCResponse response = ZCResponse.builder().build();
        Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
        if (customer != null) {
            CustomerTransaction customerTransaction = customer.getCustomerTransaction();
            CustomerReward customerReward = customer.getCustomerReward();
            if (customerTransaction != null && customerReward != null) {
                Map<String, Object> futureReward = new HashMap<>();
                double signUpReward = customerReward.getSignUpReward();
                double telegramReward = customerReward.getTelegramReward();
                double clickReward = customerReward.getClickRewardPending();
                futureReward.put("signup", signUpReward > 0 ? 0 : ICustomerFacade.getSignUpReward(customer.getDateRecorded()));
                Telegram telegram = customer.getTelegram();
                if (telegram != null && telegram.isVerified()) {
                    futureReward.put("telegram", telegramReward > 0 ? 0 : TELEGRAM_REWARD);
                }
                futureReward.put("click", clickReward);
                final long[] totalFirstPaymentRewardPending = {0L};
                Optional.ofNullable(customerRepository.nativeQuery("select distinct zc.member_id from zc_customers zc\n" +
                        "                                      inner join zc_customers_rewards zcrw on zc.customer_reward_id = zcrw.id\n" +
                        "where\n" +
                        "    zc.admin is false and zc.manager is false and zc.enabled is true\n" +
                        "  and zc.member_id != '" + customer.getMemberID() + "' and zc.phone_verified is true\n" +
                        "  and zcrw.first_payment_reward_issued = 0 and zcrw.sender_first_time_payment = '" + customer.getMemberID() + "';"))
                        .ifPresent(objectList -> objectList.forEach(o -> {
                            String userBMemberID = o.toString();
                            customerRepository
                                    .findCustomerByMemberID(userBMemberID).ifPresent(userBCustomer -> totalFirstPaymentRewardPending[0] += FIRST_PAYMENT_REWARD);
                        }));
                //pending first payment reward, either this user or the receivers users doesn't yet verified their id
                futureReward.put("first_payment", totalFirstPaymentRewardPending[0]);
                Map<String, Double> referrals = new HashMap<>();
                Optional.ofNullable(customerRepository.nativeQuery("select distinct zc.member_id from zc_customers zc\n" +
                        "                                      inner join zc_customers_rewards zcrw on zc.customer_reward_id = zcrw.id\n" +
                        "where\n" +
                        "    zc.admin is false and zc.manager is false and zc.enabled is true\n" +
                        "  and zc.member_id != '" + memberID + "' and zcrw.who_referral_you = '" + memberID + "' and zcrw.referral_reward_amount = 0;"))
                        .ifPresent(objectList -> objectList.forEach(o -> {
                            String referralMemberID = o.toString();
                            Customer userBCustomer = customerRepository.findCustomerByMemberID(referralMemberID).orElse(null);
                            if (userBCustomer != null) {
                                CustomerReward userBCustomerReward = userBCustomer.getCustomerReward();
                                if (userBCustomerReward != null) {
                                    referrals.put(referralMemberID, ICustomerFacade.getSignUpReward(userBCustomer.getDateRecorded()) / 10);
                                }
                            }
                        }));
                //pending referrals, either this user or referred users doesn't yet verified their id
                futureReward.put("referrals", referrals);
                response.setSuccess(true);
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(SUCCESS).build());
                response.setStatusCode(200);
                response.setObject(futureReward);
            } else {
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("tron is null or reward is null").build());
                response.setStatus(USER_NOT_FOUND);
                response.setStatusCode(119);
            }
        } else {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(USER_NOT_FOUND).build());
            response.setStatus(USER_NOT_FOUND);
            response.setStatusCode(119);
        }
        log.info("Response status code: " + response.getStatusCode());
        return response;
    }
}
