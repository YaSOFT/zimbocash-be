package com.zimbocash.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DocumentType {
    ID_CARD("ID_CARD"), PASSPORT("PASSPORT"), DRIVER_LICENCE("DRIVER_LICENCE"), METAL_ID("METAL_ID");
    String type;
}
