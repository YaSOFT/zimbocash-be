package com.zimbocash.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TokenType {
    REFRESH("refresh"), TOKEN("token");
    private String name;
}
