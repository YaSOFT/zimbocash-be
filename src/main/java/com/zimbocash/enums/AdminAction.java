package com.zimbocash.enums;

public enum AdminAction {
    UNBLOCK,
    APPROVE,
    ENABLED,
    DISABLED
}
