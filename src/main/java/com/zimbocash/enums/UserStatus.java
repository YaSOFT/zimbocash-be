package com.zimbocash.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UserStatus {
    PENDING("P"), VERIFIED("Y");
    String status;
}
