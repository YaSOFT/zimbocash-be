package com.zimbocash.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MissedTransactionStatus {
    PAID("PAID"), UNPAID("UNPAID");
    String status;
}
