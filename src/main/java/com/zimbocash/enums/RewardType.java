package com.zimbocash.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum RewardType {
    SIGNUP, TELEGRAM, FIRST_PAYMENT, REFERRAL
}

