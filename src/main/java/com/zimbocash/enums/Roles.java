package com.zimbocash.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Roles {
    ADMIN("ADMIN"), USER("USER"), MANAGER("MANAGER"), ANONYMOUS("ANONYMOUS");
    String level;
}
