package com.zimbocash.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TargetType {
    PHONE("PHONE"), EMAIL("EMAIL"), TELEGRAM("TELEGRAM");
    String name;
}
