package com.zimbocash.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum WalletType {
    WALLET_HOT("wallethot"), PVT("pvt");
    private String type;
}
