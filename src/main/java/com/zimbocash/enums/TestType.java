package com.zimbocash.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TestType {
    TEST1("perfectMatch"), TEST2("idDataToCustomerInfo"), TEST3("facialMatch"), TEST4("livenessMatch"), TEST5("photoshop");
    String type;
}
