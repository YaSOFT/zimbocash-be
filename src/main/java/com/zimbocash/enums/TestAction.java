package com.zimbocash.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TestAction {
    APPROVE("APPROVE"), DECLINE("DECLINE"), NOT_SURE("NOT_SURE"), FORKED("FORKED");
    String action;
}
