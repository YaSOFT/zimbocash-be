package com.zimbocash.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MissedTransactionType {

    HOT_WALLET("HOT_WALLET");

    String status;
}

