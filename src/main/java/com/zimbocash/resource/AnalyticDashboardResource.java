package com.zimbocash.resource;

import com.zimbocash.facade.IAdminDashboardFacade;
import com.zimbocash.model.DateRange;
import com.zimbocash.model.dtos.EmailStatisticsDto;
import com.zimbocash.util.StatusCode;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@AllArgsConstructor
@RequestMapping("api/dashboard/")
@PreAuthorize("hasAuthority('ADMIN')")
@RestController
public class AnalyticDashboardResource {
    private final IAdminDashboardFacade adminDashboardFacade;

    @GetMapping("accountType")
    public ResponseEntity<Object> getAccountTypeStatistics() {
        return ResponseEntity.status(StatusCode.SUCCESS).body(adminDashboardFacade.getAccountTypeStatistics());
    }

    @GetMapping("otp")
    public ResponseEntity<Object> getOtpStatistics() {
        return ResponseEntity.status(StatusCode.SUCCESS).body(adminDashboardFacade.getOtpStatistics());
    }

    @PostMapping("userVerification")
    public ResponseEntity<Object> getUserVerificationStatistics(@Valid @RequestBody DateRange dateRange) {
        return ResponseEntity.status(StatusCode.SUCCESS).body(adminDashboardFacade.getUserVerificationStatistics(dateRange));
    }

    @GetMapping("transaction")
    public ResponseEntity<Object> getTransactionStatistics() {
        return ResponseEntity.status(StatusCode.SUCCESS).body(adminDashboardFacade.getTransactionStatistics());
    }

    @PostMapping("login")
    public ResponseEntity<Object> getLoginStatistics(@Valid @RequestBody DateRange dateRange) {
        return ResponseEntity.status(StatusCode.SUCCESS).body(adminDashboardFacade.getLoginStatistics(dateRange));
    }

    @GetMapping("csv")
    public ResponseEntity getCsvForVerifiedEmails() {
        return adminDashboardFacade.getCsvForVerifiedEmails();
    }

    //TODO not working check again
    @GetMapping("manualReview")
    public ResponseEntity<Object> getManualReviewerStatistics() {
        return ResponseEntity.status(StatusCode.SUCCESS).body(adminDashboardFacade.getManualReviewerStatistics());
    }

    @PostMapping("topEmails")
    public ResponseEntity<Object> getEmailStatistics(@Valid @RequestBody EmailStatisticsDto emailStatisticsDto) {
        return ResponseEntity.status(StatusCode.SUCCESS).body(adminDashboardFacade.getEmailStatistics(emailStatisticsDto));
    }
}
