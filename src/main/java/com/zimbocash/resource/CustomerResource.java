package com.zimbocash.resource;

import com.zimbocash.commons.CustomResponseDto;
import com.zimbocash.config.security.AuthenticationInterceptor;
import com.zimbocash.facade.ICustomerFacade;
import com.zimbocash.facade.ITelegramFacade;
import com.zimbocash.facade.ITransactionFacade;
import com.zimbocash.model.ZCResponse;
import com.zimbocash.model.dtos.ClickDto;
import com.zimbocash.model.dtos.CustomerDto;
import com.zimbocash.model.dtos.FeedbackDto;
import com.zimbocash.model.dtos.LoginDto;
import com.zimbocash.model.dtos.SignUpDto;
import com.zimbocash.util.StatusCode;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequestMapping("api/customer/")
@RequiredArgsConstructor
@RestController
public class CustomerResource {
    private final ICustomerFacade customerFacade;
    private final ITransactionFacade transactionFacade;
    private final ITelegramFacade telegramFacade;

    @GetMapping("telegram/send")
    @PreAuthorize("hasAnyAuthority('USER')")
    public ResponseEntity<ZCResponse> sendTelegramOTP() {
        ZCResponse zcResponse = (ZCResponse) telegramFacade.sendTelegramOTP(AuthenticationInterceptor.getLoggedInUserMemberId());
        return ResponseEntity.status(zcResponse.isSuccess() ? HttpStatus.OK : HttpStatus.BAD_REQUEST).body(zcResponse);
    }

    @GetMapping("telegram/disable")
    @PreAuthorize("hasAnyAuthority('USER')")
    public ResponseEntity<ZCResponse> disableTelegram() {
        ZCResponse zcResponse = (ZCResponse) telegramFacade.disableTelegram(AuthenticationInterceptor.getLoggedInUserMemberId());
        return ResponseEntity.status(zcResponse.isSuccess() ? HttpStatus.OK : HttpStatus.BAD_REQUEST).body(zcResponse);
    }

    @GetMapping("telegram/verify/{otp}")
    @PreAuthorize("hasAnyAuthority('USER')")
    public ResponseEntity<ZCResponse> verifyTelegramOTP(@PathVariable("otp") String otp) {
        ZCResponse zcResponse = (ZCResponse) telegramFacade.verifyTelegramOTP(otp, AuthenticationInterceptor.getLoggedInUserMemberId());
        return ResponseEntity.status(zcResponse.isSuccess() ? HttpStatus.OK : HttpStatus.BAD_REQUEST).body(zcResponse);
    }

    @PostMapping("signin")
    public ResponseEntity<ZCResponse> signIn(@RequestBody @Valid LoginDto loginDto) {
        ZCResponse zcResponse = (ZCResponse) customerFacade.signIn(loginDto.getEmail(), loginDto.getPassword());
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }

    @PostMapping("signup")
    public ResponseEntity<ZCResponse> signUp(@RequestBody @Valid SignUpDto signUpDto) {
        ZCResponse zcResponse = (ZCResponse) customerFacade.signUp(signUpDto);
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }

    @PostMapping("forgetpassword")
    public ResponseEntity<ZCResponse> forgetPassword(@RequestBody LoginDto loginDto) {
        ZCResponse zcResponse = (ZCResponse) customerFacade.forgetPassword(loginDto.getEmail());
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }

    @PostMapping("resetpassword")
    public ResponseEntity<ZCResponse> resetPassword(@RequestBody LoginDto loginDto) {
        ZCResponse zcResponse = (ZCResponse) customerFacade.resetPassword(loginDto.getEmail(), loginDto.getPassword(), loginDto.getNewPassword());
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }

    @PostMapping("click")
    public ResponseEntity<ZCResponse> submitClick(@RequestBody @Valid ClickDto clickDto) {
        ZCResponse zcResponse = (ZCResponse) customerFacade.submitClick(clickDto);
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }

    @PostMapping("moveWallet/{receiverMemberID}")
    @PreAuthorize("hasAnyAuthority('USER')")
    public ResponseEntity<ZCResponse> moveWallet(@PathVariable("receiverMemberID") String receiverMemberID) {
        // allow him to move zash to verified account which not received yet zash from non-verified
        //  accounts (receiverMemberID should be verified id and not yet moved zash to him from non-verified accounts )
        ZCResponse zcResponse = (ZCResponse) customerFacade.moveWallet(AuthenticationInterceptor.getLoggedInUserMemberId(), receiverMemberID);
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }

    @PostMapping("update")
    @PreAuthorize("hasAnyAuthority('USER')")
    //used only once, user can update his details and the phone is NOT VERIFIED YET
    public ResponseEntity<ZCResponse> updateCustomer(@RequestBody CustomerDto customerDto) {
        ZCResponse zcResponse = (ZCResponse) customerFacade.updateCustomer(customerDto.getPhone(), AuthenticationInterceptor.getLoggedInUserMemberId());
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }

    @PutMapping("refreshAuth")
    @PreAuthorize("hasAnyAuthority('USER')")
    public ResponseEntity<CustomResponseDto> refreshToken(@RequestParam("refreshToken") String refreshToken) {
        CustomResponseDto customResponseDtoResult = (CustomResponseDto) customerFacade
                .refreshToken(refreshToken, AuthenticationInterceptor.getLoggedInUserMemberId());
        return ResponseEntity.status(customResponseDtoResult.getStatusCode()).body(customResponseDtoResult);
    }

    @GetMapping("receiveclicks")
    @PreAuthorize("hasAnyAuthority('USER')")
    public ResponseEntity<ZCResponse> receiveClickRewards() {
        ZCResponse zcResponse = (ZCResponse) customerFacade.receiveClickRewards(AuthenticationInterceptor.getLoggedInUserMemberId());
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }

    @GetMapping("get/{address}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    public ResponseEntity<ZCResponse> getTronBalance(@PathVariable("address") String address) {
        ZCResponse zcResponse = (ZCResponse) customerFacade.getTronBalance(address);
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }

    @GetMapping("get2/{memberID}")
    @PreAuthorize("hasAnyAuthority('USER','ADMIN')")
    public ResponseEntity<ZCResponse> getDetailedBalance(@PathVariable("memberID") String memberID) {
        ZCResponse zcResponse = (ZCResponse) transactionFacade.getBalance(memberID);
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }

    @GetMapping("checkmemberid/{memberID}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    public ResponseEntity<ZCResponse> isMemberIDExist(@PathVariable("memberID") String memberID) {
        ZCResponse zcResponse = (ZCResponse) customerFacade.checkMemberID(memberID);
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }

    @PostMapping("feedback/{memberID}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER')")
    public ResponseEntity<ZCResponse> sendFeedback(@PathVariable("memberID") String memberID, @RequestBody @Valid FeedbackDto feedbackDto) {
        ZCResponse zcResponse = (ZCResponse) customerFacade.sendFeedback(feedbackDto, memberID);
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }

    @GetMapping("chatbot/{message}")
    @PreAuthorize("permitAll()")
    public ResponseEntity<ZCResponse> chatbotResult(@PathVariable("message") String message) {
        ZCResponse zcResponse = (ZCResponse) customerFacade.chatbotResult(message);
        return ResponseEntity.status(zcResponse.getStatusCode()).body(zcResponse);
    }

    @GetMapping("getCustomer/{memberID}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'USER', 'MANAGER')")
    public ResponseEntity<ZCResponse> getCustomerDetailsByMemberID(@PathVariable("memberID") String memberID) {
        ZCResponse zcResponse = (ZCResponse) customerFacade.getCustomerDetailsByMemberID(memberID);
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }
}
