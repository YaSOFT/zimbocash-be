
package com.zimbocash.resource;

import com.zimbocash.config.security.AuthenticationInterceptor;
import com.zimbocash.enums.DocumentType;
import com.zimbocash.facade.IKYCFacade;
import com.zimbocash.facade.ITransactionFacade;
import com.zimbocash.model.ZCResponse;
import com.zimbocash.model.dtos.CustomerDto;
import com.zimbocash.model.dtos.OTPDto;
import com.zimbocash.model.dtos.TransferDto;
import com.zimbocash.util.StatusCode;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("api/validation/")
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('USER')")
public class TransactionResource {
    private final ITransactionFacade transactionFacade;
    private final IKYCFacade kycFacade;

    @PostMapping("progress")
    public ResponseEntity<ZCResponse> transfer(@RequestBody @Valid TransferDto transferDto) {
        ZCResponse zcResponse = (ZCResponse) transactionFacade.transfer(transferDto, AuthenticationInterceptor.getLoggedInUserMemberId());
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }

    @PostMapping("sendotp")
    public ResponseEntity<ZCResponse> sendOTP(@RequestBody @Valid OTPDto otpDto) {
        ZCResponse zcResponse = (ZCResponse) transactionFacade.sendOTP(AuthenticationInterceptor.getLoggedInUserMemberId(), otpDto.getPhone());
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }

    @PostMapping("verifyotp")
    public ResponseEntity<ZCResponse> verifyOTPAndSetUpPhoneVerified(@RequestBody @Valid OTPDto otpDto) {
        ZCResponse zcResponse = (ZCResponse) transactionFacade.verifyOTP(otpDto.getPhone(), otpDto.getOtp());
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }

    @PostMapping("updatekyc")
    //start process upload user information
    public ResponseEntity<ZCResponse> updateCustomer(@RequestBody @Valid CustomerDto customerDto) {
        ZCResponse zcResponse = (ZCResponse) kycFacade.updateCustomerKYC(customerDto, AuthenticationInterceptor.getLoggedInUserMemberId());
        return ResponseEntity.status(zcResponse.isSuccess() ? HttpStatus.OK : HttpStatus.BAD_REQUEST).body(zcResponse);
    }

    @PostMapping("rekognition/id")
    //start process upload id
    public ResponseEntity<ZCResponse> uploadIdImage(@RequestBody @Valid DocumentType documentType) {
        ZCResponse zcResponse = (ZCResponse) kycFacade.uploadId(AuthenticationInterceptor.getLoggedInUserMemberId(), documentType);
        return ResponseEntity.status(zcResponse.isSuccess() ? HttpStatus.OK : HttpStatus.BAD_REQUEST).body(zcResponse);
    }

    @PostMapping("rekognition/liveness")
    //start process upload liveness
    public ResponseEntity<ZCResponse> uploadLivenessImage(@RequestBody @Valid DocumentType documentType) {
        ZCResponse zcResponse = (ZCResponse) kycFacade.uploadLiveness(AuthenticationInterceptor.getLoggedInUserMemberId(), documentType);
        return ResponseEntity.status(zcResponse.isSuccess() ? HttpStatus.OK : HttpStatus.BAD_REQUEST).body(zcResponse);
    }

    @PostMapping("getimageurl")
    public ResponseEntity<ZCResponse> getImageUrl(@RequestBody @Valid DocumentType documentType) {
        ZCResponse zcResponse = (ZCResponse) kycFacade.getImageUrl(AuthenticationInterceptor.getLoggedInUserMemberId(), documentType);
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }
}
