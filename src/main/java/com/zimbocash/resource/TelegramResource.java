package com.zimbocash.resource;

import com.fasterxml.jackson.databind.JsonNode;
import com.zimbocash.commons.CustomResponseDto;
import com.zimbocash.facade.ITelegramFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

//@RequestMapping("webhook/")
@RequiredArgsConstructor
//@RestController
public class TelegramResource {
    private final ITelegramFacade telegramFacade;

    @PostMapping
    public ResponseEntity<Object> requestTelegram(@RequestBody JsonNode jsonNode) {
        CustomResponseDto customResponseDto = telegramFacade.requestTelegram(jsonNode);
        return ResponseEntity
                .status(HttpStatus.OK).body(customResponseDto.getMessage());
    }
}
