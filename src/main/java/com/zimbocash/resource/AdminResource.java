package com.zimbocash.resource;

import com.zimbocash.commons.CustomResponseDto;
import com.zimbocash.config.security.AuthenticationInterceptor;
import com.zimbocash.enums.TestAction;
import com.zimbocash.facade.IAdminFacade;
import com.zimbocash.model.ZCResponse;
import com.zimbocash.model.dtos.ApproveOrUnblockDto;
import com.zimbocash.model.dtos.CustomerKYCManageDto;
import com.zimbocash.model.dtos.CustomerPaginationDto;
import com.zimbocash.model.dtos.DomainBannedDto;
import com.zimbocash.model.dtos.ManagerOrAdminDto;
import com.zimbocash.model.entity.Customer;
import com.zimbocash.model.entity.CustomerConfirmation;
import com.zimbocash.model.entity.CustomerReward;
import com.zimbocash.util.StatusCode;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import java.text.ParseException;
import java.util.Date;

import static com.zimbocash.util.Constants.SIMPLE_DATE_FORMAT;

@RestController
@RequestMapping("api/customer/admin/")
@RequiredArgsConstructor
public class AdminResource {
    private final IAdminFacade adminFacade;
    private final IAdminFacade.KYCAuthentication kycAuthentication;

    @PostMapping("new")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> createOrUpdateManagers(@RequestBody @Valid ManagerOrAdminDto customerAdminDtos) {
        return ResponseEntity.ok(adminFacade.createOrUpdateManagers(customerAdminDtos, AuthenticationInterceptor.getLoggedInUserMemberId()));
    }

    @DeleteMapping("{email}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> deleteManager(@PathVariable("email") String email) {
        return ResponseEntity.ok(adminFacade.deleteManager(email));
    }

    @GetMapping("list")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> listManagers(@RequestParam("page") int page, @RequestParam("size") int pageSize) {
        return ResponseEntity.ok(adminFacade.listAllManagers(page, pageSize));
    }

    @PostMapping("update")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> adminUpdateCustomer(@RequestBody @Valid ManagerOrAdminDto customerAdminDto) {
        CustomResponseDto customResponseDto = null;
        try {
            customResponseDto = adminFacade.adminUpdateCustomer(Customer.builder()
                    .email(customerAdminDto.getEmail())
                    .firstName(customerAdminDto.getFirstName())
                    .lastName(customerAdminDto.getLastName())
                    .idNumber(customerAdminDto.getIdNumber())
                    .phone(customerAdminDto.getPhone())
                    .customerReward(CustomerReward
                            .builder()
                            .linkClicks(0)
                            .build()
                    )
                    .customerConfirmation(CustomerConfirmation
                            .builder()
                            .confirmed(customerAdminDto.getApproved())
                            .createdDate(SIMPLE_DATE_FORMAT.parse(SIMPLE_DATE_FORMAT.format(new Date())))
                            .build())
                    .build());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ResponseEntity
                .status(customResponseDto.isSuccess() ? HttpStatus.BAD_REQUEST : HttpStatus.OK).body(customResponseDto.getMessage());
    }

    @PostMapping("customers")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> adminGetCustomers(@RequestBody @Valid CustomerPaginationDto query) {
        return ResponseEntity.ok(adminFacade.getCustomerList(query));
    }

    //new api
    @PutMapping("domains")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> adminBanDomain(@RequestBody DomainBannedDto domainBannedDto) {
        return ResponseEntity.ok(adminFacade.banOrUnBanDomain(domainBannedDto));
    }

    @GetMapping("banned-domains")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> getBannedDomains() {
        return ResponseEntity.ok(adminFacade.getBannedDomains());
    }

    @GetMapping("reviews")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> getReviews() {
        return ResponseEntity.ok(adminFacade.getReviews());
    }

    @GetMapping("details/{member_id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> getDetails(@PathVariable("member_id") String memberID) {
        return ResponseEntity.ok(adminFacade.getDetails(memberID));
    }

    @GetMapping("kyc/reset/{member_id}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    public ResponseEntity<Object> reset(@PathVariable("member_id") String memberID) {
        return ResponseEntity.ok(adminFacade.reset(memberID));
    }

    @GetMapping("disable/{email}/{comment}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> disable(@Email @PathVariable("email") String email, @PathVariable("comment") String comment) {
        return ResponseEntity.ok(adminFacade.disable(email, comment));
    }

    @GetMapping("enable/{email}/{comment}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> enable(@Email @PathVariable("email") String email, @PathVariable("comment") String comment) {
        return ResponseEntity.ok(adminFacade.enable(email, comment));
    }

    /*******************************/ //kyc process
    @GetMapping("kyc/next")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    public ResponseEntity<ZCResponse> getNext() {
        ZCResponse zcResponse = (ZCResponse) kycAuthentication.getNext(AuthenticationInterceptor.getLoggedInUserMemberId());
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }

    @PostMapping("kyc/takeaction")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    public ResponseEntity<Object> takeActionKYC(@Valid @RequestBody CustomerKYCManageDto customerKYCManageDto) {
        return ResponseEntity.ok(kycAuthentication.takeActionKYC(customerKYCManageDto, AuthenticationInterceptor.getLoggedInUserMemberId()));
    }

    //get next duplicated
    @GetMapping("kyc/duplicated")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    public ResponseEntity<Object> getKYCDuplications() {
        return ResponseEntity.ok(kycAuthentication.getKYCDuplicated(AuthenticationInterceptor.getLoggedInUserMemberId()));
    }

    @PutMapping("kyc/duplicate/takeaction/{memberID}/{zcdiID}/{action}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'MANAGER')")
    public ResponseEntity<Object> takeActionDuplicate(@PathVariable("memberID") String memberID, @PathVariable("zcdiID") long zcdiID,
                                                      @PathVariable(
                                                              "action") TestAction action) {
        return ResponseEntity.ok(kycAuthentication.updateDuplications(memberID, zcdiID, action, AuthenticationInterceptor.getLoggedInUserMemberId()));
    }

    @PostMapping("blockedUsers")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> listBlockedUsers(@RequestBody @Valid CustomerPaginationDto query) {
        return ResponseEntity.ok(adminFacade.listBlockedUsers(query));
    }

    @GetMapping("idAndSelfie/{idNumber}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ZCResponse> getSelfieAndIdForUser(@PathVariable("idNumber") String idNumber) {
        ZCResponse zcResponse = (ZCResponse) adminFacade.getIdAndSelfieByUserMemberId(idNumber);
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }


    @PostMapping("approveOrUnblock")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ZCResponse> approveOrUnblock(@RequestBody ApproveOrUnblockDto approveOrUnblockDto) {
        ZCResponse zcResponse = adminFacade.approveToUnblock(approveOrUnblockDto, AuthenticationInterceptor.getLoggedInUserMemberId());
        return ResponseEntity.status(zcResponse.isSuccess() ? StatusCode.SUCCESS : StatusCode.BAD_REQUEST).body(zcResponse);
    }
}
