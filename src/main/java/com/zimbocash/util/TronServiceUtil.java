package com.zimbocash.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.Any;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.zimbocash.crypto.Parameter;
import com.zimbocash.crypto.RSASignature;
import com.zimbocash.crypto.Sha256Sm3Hash;
import com.zimbocash.crypto.protos.Protocol;
import com.zimbocash.crypto.protos.ReturnOuterClass;
import com.zimbocash.crypto.protos.TransactionExtensionOuterClass;
import com.zimbocash.crypto.protos.contract.AssetIssueContractOuterClass;
import com.zimbocash.crypto.protos.contract.BalanceContract;
import com.zimbocash.crypto.utils.Base58;
import com.zimbocash.crypto.utils.ByteArray;
import com.zimbocash.crypto.utils.Hash;
import com.zimbocash.dao.MissedTransactionRepository;
import com.zimbocash.dao.TransactionTrackRepository;
import com.zimbocash.enums.MissedTransactionStatus;
import com.zimbocash.enums.ServerType;
import com.zimbocash.external.Api;
import com.zimbocash.external.PvtClientHttp;
import com.zimbocash.external.WalletGrpc;
import com.zimbocash.external.WalletHttp;
import com.zimbocash.facade.ISocketFacade;
import com.zimbocash.model.dtos.CryptoDto;
import com.zimbocash.model.dtos.HashDto;
import com.zimbocash.model.dtos.SignatureDto;
import com.zimbocash.model.dtos.SocketBodyDto;
import com.zimbocash.model.entity.MissedTransaction;
import com.zimbocash.model.entity.TransactionTrack;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.spongycastle.asn1.x509.SubjectPublicKeyInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.zimbocash.enums.WalletType.PVT;
import static com.zimbocash.enums.WalletType.WALLET_HOT;
import static com.zimbocash.util.Constants.DECIMAL_RATIO;
import static com.zimbocash.util.Constants.SIMPLE_DATE_FORMAT;

@Component
@Slf4j
@RequiredArgsConstructor
public class TronServiceUtil {
    private final static byte addressPreFixByte = Parameter.CommonConstant.ADD_PRE_FIX_BYTE_MAINNET;
    public static volatile String hotWalletAddress;
    public static volatile String tronWalletAddress;
    private final TransactionTrackRepository transactionTrackRepository;
    private final MissedTransactionRepository missedTransactionRepository;
    private final PvtClientHttp pvtClientHttp;
    private final WalletHttp walletHttp;
    private final ObjectMapper objectMapper;
    private final Hash hash;
    private final ISocketFacade socketFacade;
    @Value("${tron.fullNodeAddress}")
    String fullNode;
    //this is init in trx not zash
    @Value("${tron.initAssetAmountZASH}")
    long initAmount;
    @Value("${tron.tokenID}")
    String assetName;
    private WalletGrpc.WalletBlockingStub blockingStubFull;

    public static boolean addressValid(byte[] address) {
        if (ArrayUtils.isEmpty(address) || (address.length != Parameter.CommonConstant.ADDRESS_SIZE)) {
            return false;
        }
        byte preFixByte = address[0];
        return preFixByte == addressPreFixByte;
    }

    public static Sha256Sm3Hash getBlockHash(TransactionExtensionOuterClass.BlockExtension block) {
        return Sha256Sm3Hash.of(block.getBlockHeader().getRawData().toByteArray());
    }

    public static com.zimbocash.crypto.protos.Protocol.Transaction setReference(com.zimbocash.crypto.protos.Protocol.Transaction transaction,
                                                                                TransactionExtensionOuterClass.BlockExtension newestBlock) {
        long blockHeight = newestBlock.getBlockHeader().getRawData().getNumber();
        byte[] blockHash = getBlockHash(newestBlock).getBytes();
        byte[] refBlockNum = ByteArray.fromLong(blockHeight);
        com.zimbocash.crypto.protos.Protocol.Transaction.raw rawData = transaction.getRawData().toBuilder()
                .setRefBlockHash(ByteString.copyFrom(ByteArray.subArray(blockHash, 8, 16)))
                .setRefBlockBytes(ByteString.copyFrom(ByteArray.subArray(refBlockNum, 6, 8)))
                .build();
        return transaction.toBuilder().setRawData(rawData).build();
    }

    private static byte[] decode58(String input) {
        byte[] decodeCheck = Base58.decode(input);
        if (decodeCheck.length <= 4) {
            return null;
        }
        byte[] decodeData = new byte[decodeCheck.length - 4];
        System.arraycopy(decodeCheck, 0, decodeData, 0, decodeData.length);
        byte[] hash0 = Sha256Sm3Hash.hash(decodeData);
        byte[] hash1 = Sha256Sm3Hash.hash(hash0);
        if (hash1[0] == decodeCheck[decodeData.length]
                && hash1[1] == decodeCheck[decodeData.length + 1]
                && hash1[2] == decodeCheck[decodeData.length + 2]
                && hash1[3] == decodeCheck[decodeData.length + 3]) {
            return decodeData;
        }
        return null;
    }

    public static AssetIssueContractOuterClass.TransferAssetContract createTransferAssetContract(
            byte[] to, byte[] assertName, byte[] owner, long amount) {
        AssetIssueContractOuterClass.TransferAssetContract.Builder builder = AssetIssueContractOuterClass.TransferAssetContract.newBuilder();
        ByteString bsTo = ByteString.copyFrom(to);
        ByteString bsName = ByteString.copyFrom(assertName);
        ByteString bsOwner = ByteString.copyFrom(owner);
        builder.setToAddress(bsTo);
        builder.setAssetName(bsName);
        builder.setOwnerAddress(bsOwner);
        builder.setAmount(amount);
        return builder.build();
    }

    @PostConstruct
    public void init() {
        ManagedChannel channelFull = ManagedChannelBuilder.forTarget(fullNode)
                .usePlaintext()
                .keepAliveTimeout(3, TimeUnit.SECONDS)
                .build();
        blockingStubFull = WalletGrpc.newBlockingStub(channelFull);
    }

    public CryptoDto createAccount() {
        String partLog = "internal createAccount method";
        byte[] newPublicKey = pvtClientHttp.createAccount();
        String toAddress = getAddressFromPublicKeyHex(new String(newPublicKey));
        //create asset, 0.1 trx as fees, and 0.000001 trx send to the account receiver
        boolean isCreated = createAsset(tronWalletAddress, toAddress, initAmount);
        if (toAddress != null && isCreated) {
            return CryptoDto.builder().publicKey(new String(newPublicKey)).tronAddress(toAddress).build();
        } else {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail("createAccount").message("cannot create account").build());
            return null;
        }
    }

    public String getAddressFromPublicKeyHex(String pubKeyInHex) {
        try {
            SubjectPublicKeyInfo publicKeyInfo = SubjectPublicKeyInfo.getInstance(ByteArray.fromHexString(pubKeyInHex));
            byte[] ecPointBytes = publicKeyInfo.getPublicKeyData().getBytes();
            return hash.encodeTo58(hash.computeAddress(ecPointBytes));
        } catch (NoSuchAlgorithmException e) {
            socketFacade.send(SocketBodyDto.builder().api("internal getAddressFromPublicKeyHex method ").memberIDOrEmail("getAddressFromPublicKeyHex").message(e.getMessage()).build());
            log.error(e.getMessage());
        }
        return null;
    }

    private com.zimbocash.crypto.protos.Protocol.Transaction createTransaction(byte[] from, byte[] to, long amount) {
        com.zimbocash.crypto.protos.Protocol.Transaction.Builder transactionBuilder = com.zimbocash.crypto.protos.Protocol.Transaction.newBuilder();
        TransactionExtensionOuterClass.BlockExtension newestBlock = getBlock(-1);
        com.zimbocash.crypto.protos.Protocol.Transaction.Contract.Builder contractBuilder = com.zimbocash.crypto.protos.Protocol.Transaction.Contract.newBuilder();
        BalanceContract.TransferContract.Builder transferContractBuilder = BalanceContract.TransferContract.newBuilder();
        ByteString bsTo = ByteString.copyFrom(to);
        ByteString bsOwner = ByteString.copyFrom(from);
        transferContractBuilder.setToAddress(bsTo);
        transferContractBuilder.setAmount(amount);
        transferContractBuilder.setOwnerAddress(bsOwner);
        try {
            Any any = Any.pack(transferContractBuilder.build());
            contractBuilder.setParameter(any);
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api("internal createTransaction method").memberIDOrEmail("createTransaction").message(e.getMessage()).build());
            log.error(e.getMessage());
            return null;
        }
        com.zimbocash.crypto.protos.Protocol.Transaction.Contract.ContractType transferContract =
                com.zimbocash.crypto.protos.Protocol.Transaction.Contract.ContractType.TransferContract;
        contractBuilder.setType(transferContract);
        transactionBuilder.getRawDataBuilder().addContract(contractBuilder)
                .setTimestamp(System.currentTimeMillis())
                .setExpiration(newestBlock.getBlockHeader().getRawData().getTimestamp() + 10 * 60 * 60 * 1000);
        com.zimbocash.crypto.protos.Protocol.Transaction transaction = transactionBuilder.build();
        return setReference(transaction, newestBlock);
    }

    private TransactionExtensionOuterClass.BlockExtension getBlock(int blockNumber) {
        if (blockNumber < 0) {
            return blockingStubFull.getNowBlock2(Api.EmptyMessage.newBuilder().build());
        }
        Api.NumberMessage.Builder builder = Api.NumberMessage.newBuilder();
        builder.setNum(blockNumber);
        return blockingStubFull.getBlockByNum2(builder.build());
    }

    private byte[] decodeFromBase58Check(String addressBase58) {
        if (StringUtils.isEmpty(addressBase58)) {
            return null;
        }
        byte[] address = decode58(addressBase58);
        if (!addressValid(address)) {
            socketFacade.send(SocketBodyDto.builder().api("internal decodeFromBase58Check method").memberIDOrEmail("decodeFromBase58Check")
                    .message("address not valid " + addressBase58).build());
            return null;
        }
        return address;
    }


    private byte[] signTransaction2Byte(Object publicKeyInHex, Protocol.Transaction transaction, byte[] transactionRawBytes, String walletType) {
        byte[] hash = Sha256Sm3Hash.hash(transactionRawBytes);
        SignatureDto signatureDto;
        try {
            if (publicKeyInHex == null) {
                byte[] bytes = objectMapper.writeValueAsBytes(HashDto.builder().bytes(hash)
                        .key(walletType.equalsIgnoreCase("wallettrx") ? tronWalletAddress : hotWalletAddress).build());
                byte[] signature = RSASignature.signRSA(bytes);
                signatureDto = objectMapper.readValue(walletHttp.sign(objectMapper.writeValueAsBytes(
                        SignatureDto.builder().signedTransaction(signature).message(bytes).build())), SignatureDto.class);
                if (!RSASignature.verifySignatureFromServers(signatureDto.getMessage(), signatureDto.getSignedTransaction(), ServerType.GREEN)) {
                    socketFacade.send(SocketBodyDto.builder().api("internal signTransaction2Byte method").memberIDOrEmail("signTransaction2Byte")
                            .message("GREEN signature not valid").build());
                    return null;
                }
            } else {
                CryptoDto cryptoDto = CryptoDto.builder().transactionRawAsJsonHash(hash).publicKey(publicKeyInHex.toString()).build();
                byte[] bytes = objectMapper.writeValueAsBytes(cryptoDto);
                byte[] signature = RSASignature.signRSA(bytes);
                signatureDto = objectMapper.readValue(pvtClientHttp.sign(objectMapper.writeValueAsString(
                        SignatureDto.builder().signedTransaction(signature).message(bytes).build())), SignatureDto.class);
                if (!RSASignature.verifySignatureFromServers(signatureDto.getMessage(), signatureDto.getSignedTransaction(), ServerType.RED)) {
                    socketFacade.send(SocketBodyDto.builder().api("internal signTransaction2Byte method").memberIDOrEmail("signTransaction2Byte")
                            .message("RED signature not valid").build());
                    return null;
                }
            }
            return transaction.toBuilder().addSignature(ByteString.copyFrom(signatureDto.getMessage())).build().toByteArray();
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api("internal signTransaction2Byte method").memberIDOrEmail("signTransaction2Byte").message(e.getMessage()).build());
            log.error(e.getMessage());
            return null;
        }
    }

    public boolean createAsset(String fromBase, String toBase, long amount) {
        com.zimbocash.crypto.protos.Protocol.Transaction transaction = createTransaction(decodeFromBase58Check(fromBase), decodeFromBase58Check(toBase), amount);
        try {
            if (transaction != null) {
                byte[] transactionBytes = transaction.getRawData().toByteArray();
                //signTransaction2Byte to hotwalletserver
                byte[] signedTransaction = signTransaction2Byte(null, transaction, transactionBytes, "wallettrx");
                return broadcastTransaction(Protocol.Transaction.parseFrom(signedTransaction));
            }
            socketFacade.send(SocketBodyDto.builder().api("internal createAsset method").memberIDOrEmail("createAsset").message("transaction object null").build());
            return false;
        } catch (InvalidProtocolBufferException e) {
            log.error(e.getMessage());
            socketFacade.send(SocketBodyDto.builder().api("internal createAsset method").memberIDOrEmail("createAsset").message(e.getMessage()).build());
            return false;
        }
    }

    private boolean broadcastTransaction(Protocol.Transaction signedTransaction) {
        int i = 10;
        ReturnOuterClass.Return response;
        do {
            response = blockingStubFull.broadcastTransaction(signedTransaction);
            i--;
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                socketFacade.send(SocketBodyDto.builder().api("internal broadcastTransaction method").memberIDOrEmail("broadcastTransaction").message(e.getMessage()).build());
                log.error(e.getMessage());
            }
        } while (!response.getResult() && response.getCode() == ReturnOuterClass.Return.response_code.SERVER_BUSY && i > 0);
        return response.getResult();
    }

    private TransactionExtensionOuterClass.TransactionExtension prepareTransaction(AssetIssueContractOuterClass.TransferAssetContract contract) {
        return blockingStubFull.transferAsset2(contract);
    }

    //    @CacheResult(cacheName = "convertAmountToTronAmount")
    public long convertAmountToTronAmount(double amount) {
        return (long) (amount * DECIMAL_RATIO);
    }

    public String issueOrTransfer(String publicKeyInHex, String to, double amount, String assetName) {
        String fromAddress;
        if (publicKeyInHex == null) {
            fromAddress = hotWalletAddress;
        } else {
            fromAddress = getAddressFromPublicKeyHex(publicKeyInHex);
        }
        AssetIssueContractOuterClass.TransferAssetContract contract =
                createTransferAssetContract(decodeFromBase58Check(to), assetName.getBytes(), decodeFromBase58Check(fromAddress), convertAmountToTronAmount(amount));

        TransactionExtensionOuterClass.TransactionExtension transactionExtension = prepareTransaction(contract);
        try {
            String txID = processTransactionExtension(publicKeyInHex, transactionExtension);
            if (txID != null && !txID.isEmpty()) {
                TransactionTrack transactionTrack = TransactionTrack.builder()
                        .txId(txID)
                        .senderId(fromAddress)
                        .receiverId(to)
                        .amount(amount)
                        .dateRecorded(SIMPLE_DATE_FORMAT.parse(SIMPLE_DATE_FORMAT.format(new Date())))
                        .build();
                transactionTrackRepository.save(transactionTrack);
                return txID;
            }
            socketFacade.send(SocketBodyDto.builder().api("internal issueOrTransfer method").memberIDOrEmail("issueOrTransfer").message("txID is null").build());
            return null;
        } catch (IOException | ParseException e) {
            log.error(e.getMessage());
            socketFacade.send(SocketBodyDto.builder().api("internal issueOrTransfer method").memberIDOrEmail("issueOrTransfer").message(e.getMessage()).build());
            return null;
        }
    }

    private String processTransactionExtension(String publicKeyInHex, TransactionExtensionOuterClass.TransactionExtension transactionExtension)
            throws IOException {
        if (transactionExtension == null) {
            return null;
        }
        ReturnOuterClass.Return ret = transactionExtension.getResult();
        if (!ret.getResult()) {
            return null;
        }
        com.zimbocash.crypto.protos.Protocol.Transaction transaction = transactionExtension.getTransaction();
        if (transaction == null || transaction.getRawData().getContractCount() == 0) {
            socketFacade.send(SocketBodyDto.builder().api("internal processTransactionExtension method")
                    .memberIDOrEmail("processTransactionExtension").message("transaction object is null or getContractCount is 0").build());
            return null;
        }

        byte[] transactionBytes = transaction.getRawData().toByteArray();
        byte[] signedTransaction;
        if (publicKeyInHex == null) {
            socketFacade.send(SocketBodyDto.builder().api("internal processTransactionExtension method")
                    .memberIDOrEmail("processTransactionExtension").message("hot wallet sending").build());
            signedTransaction = signTransaction2Byte(null, transaction, transactionBytes, WALLET_HOT.getType());
        } else {
            socketFacade.send(SocketBodyDto.builder().api("internal processTransactionExtension method")
                    .memberIDOrEmail("processTransactionExtension").message("user wallet sending").build());
            signedTransaction = signTransaction2Byte(publicKeyInHex, transaction, transactionBytes, PVT.getType());
        }

        String txID = ByteArray.toHexString(Sha256Sm3Hash.hash(transaction.getRawData().toByteArray()));
        boolean isSuccess = broadcastTransaction(Protocol.Transaction.parseFrom(signedTransaction));
        if (isSuccess) {
            return txID;
        }
        socketFacade.send(SocketBodyDto.builder().api("internal processTransactionExtension method")
                .memberIDOrEmail("processTransactionExtension").message("isSuccess " + isSuccess).build());
        return null;
    }

    //0.004 zash, 0.004 * 1_000_000  = 40_000
    public double getBalance(String address, String assetName) {
        ByteString addressBS = ByteString.copyFrom(decodeFromBase58Check(address));
        Protocol.Account account = Protocol.Account.newBuilder().setAddress(addressBS).build();
        account = blockingStubFull.getAccount(account);
//        return new BigDecimal(account.getBalance()).divide(new BigDecimal(DECIMAL_RATIO), RoundingMode.HALF_UP).doubleValue();
        //for other asset balances return the balance with 6 omit i.e / 1000000
        return account.getAssetV2Map().entrySet().stream().filter(stringLongEntry -> stringLongEntry.getKey().equals(assetName))
                .mapToLong(Map.Entry::getValue).sum();
    }

    public void saveMissedTransaction(String tronPublicKey, String toAddress, double amount, String memberIDReceiver) {
        try {
            MissedTransaction missedTransaction = MissedTransaction.builder()
                    .senderId(tronPublicKey)
                    .receiverId(toAddress)
                    .amount(amount)
                    .status(MissedTransactionStatus.UNPAID)
                    .dateRecorded(SIMPLE_DATE_FORMAT.parse(SIMPLE_DATE_FORMAT.format(new Date())))
                    .memberIDReceiver(memberIDReceiver)
                    .build();
            missedTransactionRepository.save(missedTransaction);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            socketFacade.send(SocketBodyDto.builder().api("internal saveMissedTransaction method")
                    .memberIDOrEmail("saveMissedTransaction").message(ex.getMessage()).build());
        }
    }
}
