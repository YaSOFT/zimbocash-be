package com.zimbocash.util;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lexruntime.AmazonLexRuntime;
import com.amazonaws.services.lexruntime.AmazonLexRuntimeClientBuilder;
import com.amazonaws.services.lexruntime.model.PostTextRequest;
import com.amazonaws.services.lexruntime.model.PostTextResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ChatBot {

    @Value("${chatbot.name}")
    String chatBotName;
    @Value("${chatbot.alias}")
    String chatBotAlias;
    @Value("${chatbot.userid}")
    String chatBotUserid;

    public String getChatbotResponse(String message) {
        log.info("Chatbot message: " + message);
        PostTextRequest request = getPostTextRequest().withInputText(message);
        AmazonLexRuntime client = getAmazonLexRuntime();
        PostTextResult result = client.postText(request);
        if (result == null) {
            return null;
        }
        log.info("Chatbot response: " + result.getMessage());

        return result.getMessage();
    }

    public AmazonLexRuntime getAmazonLexRuntime() {
        return AmazonLexRuntimeClientBuilder.standard().withRegion(Regions.EU_WEST_1).build();
    }

    public PostTextRequest getPostTextRequest() {
        return new PostTextRequest().withBotName(chatBotName).withBotAlias(chatBotAlias).withUserId(chatBotUserid);
    }
}
