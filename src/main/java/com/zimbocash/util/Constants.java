package com.zimbocash.util;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
@ConfigurationProperties(prefix = "constants")
public class Constants {
    public static final String SUCCESS = "SUCCESS";
    public static final String FAIL = "FAIL";
    public static final String NOT_ALLOWED_TO_PERFORM_THIS_ACTION = "NOT_ALLOWED_TO_PERFORM_THIS_ACTION";
    public static final String NOT_FOUND = "NOT_FOUND";
    public static final String TRON_NOT_FOUND = "TRON_ACCOUNT_NOT_FOUND";
    public static final String DB_ERROR_OCCURED = "DB_ERROR_OCCURED";
    public static final String USER_NOT_FOUND = "USER_NOT_FOUND";
    public static final String ALREADY_CONFIRMED = "USER_ALREADY_CONFIRMED_ID_VERIFICATION";
    public static final String NEEDS_LIVENESS = "UPLOAD_LIVENESS_WAITING";
    public static final String NEEDS_ID_UPLOAD = "UPLOAD_ID_WAITING";
    public static final String ID_UPLOAD_SUCCESS = "ID_UPLOAD_SUCCESS";
    public static final String CONTACT_US = "CONTACT_US";
    public static final String NEEDS_MANUAL_REVIEW = "NEEDS_MANUAL_REVIEW";
    public static final String ATTEMPTS_EXCEED = "ATTEMPTS_EXCEED";
    public static final String ACCOUNT_SUSPENDED_PERMANENTLY = "ACCOUNT_SUSPENDED_PERMANENTLY";
    public static final String ACCOUNT_SUSPENDED_PERMANENTLY_WITH_MOVE_ZASH_ALLOWED = "ACCOUNT_SUSPENDED_PERMANENTLY_WITH_MOVE_ZASH_ALLOWED";
    public static final String USER_RECEIVER_NOT_FOUND = "USER_RECEIVER_NOT_FOUND";
    public static final String WRONG_PASSWORD = "WRONG_PASSWORD";
    public static final String WRONG_PASSWORD_PATTERN = "WRONG_PASSWORD_PATTERN";
    public static final String PHONE_NUMBER_EXIST_ALREADY = "PHONE_NUMBER_EXIST_ALREADY";
    public static final String PHONE_NUMBER_VERIFIED_ALREADY = "PHONE_NUMBER_VERIFIED_ALREADY";
    public static final double DECIMAL_RATIO = 1000000;
    public static final String LIMIT_REACHED = "LIMIT_REACHED";
    public static final String RECEIVER_LIMIT_REACHED = "RECEIVER_LIMIT_REACHED";
    public static final String SENDER_LIMIT_REACHED = "SENDER_LIMIT_REACHED";
    public static final String RECEIVER_BLOCKED = "RECEIVER_BLOCKED";
    public static final String NOILO_ALPHABET = "23456789ABCDEFGHJKMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz";
    public static final String NOILO_LOWERCASE_ALPHABET = "23456789abcdefghjkmnpqrstuvwxyz";
    public static final String[] LIVENESS_WORDS = {"AWAY", "ABOVE", "AGAPE", "AHEAD", "AMAZE", "AMONG", "AWAKE", "AWASH", "BABY", "BACK", "BAKE",
            "BAND", "BANK", "BANK", "BASE", "BEANS", "BEEN", "BODY", "BONE", "BOOK", "BOSS", "BUSES", "BUSH",
            "BUSHY", "CAKE", "CAME", "CAMP", "CASH", "CHAMP", "CHEEK", "CHEW", "COPE", "DAWN", "DEEP",
            "DESK", "DONE", "DOWN", "DUCK", "DUSK", "EACH", "EDGE", "EVEN", "GAME", "GAVE", "GAZE", "GONE", "GOOD", "HAND",
            "HAPPY", "HAVE", "HEAD", "HOME", "HONEY", "HOPE", "HOUSE", "HUMAN", "KEEP", "LOVE", "MADE", "MAKE",
            "MANY", "MASK", "MOOD", "MOON", "MOUSE", "MOVE", "MUCH", "NAME", "NECK", "NEED", "NEWS",
            "NOSE", "OKAY", "OPEN", "OZONE", "PAGE", "PHASE", "PHONE", "SAME", "SAND", "SAUCE", "SAVE", "SEEK", "SEEM",
            "SEND", "SHOE", "SHOP", "SHOW", "SNOW", "SOME", "SONG", "SPADE", "SUCH", "SUNNY", "SWAMP", "UPON", "WAGE",
            "WAKE", "WASH", "WHEN", "WOOD", "ZONE"};
    public static final String FROM = "community@zimbo.cash";
    public static final String FROMNAME = "The ZIMBOCASH Team";
    public static final String WELCOME_SUBJECT = "Welcome to ZIMBOCASH!";
    public static final String ACCOUNT_CONFIRM_SUBJECT = "ZIMBOCASH – Confirm your account ";
    public static final String PASSWORD_RESET_SUBJECT = "ZIMBOCASH – Password Reset ";
    public static final String TRANSACTION_SENT_SUBJECT = "ZIMBOCASH – Payment Sent";
    public static final String TRANSACTION_FAIL = "TRANSACTION_FAIL!";
    public static final String TRANSACTION_RECIEVED_SUBJECT = "ZIMBOCASH – Payment Received";
    public static final String REDUCING_SUBJECT = "ZASH account blocked – READ TO AVOID LOSING YOUR ZASH";
    public static final String REMINDER_SUBJECT = "You are almost there WITH YOUR ZIMBOCASH ID verification";
    public static final String BLOCKED_ACCOUNT_SUBJECT = "Your ID failed our test – account blocked";
    public static final String CONFIRMED_ACCOUNT_SUBJECT = "Success! We verified your ID";
    public static final String REWARD_RECEIVED_SUBJECT = "You just received your ZASH signup reward";
    public static final String REWARD_REFERRAL_SUBJECT = "ZIMBOCASH - Referral Reward";
    public static final String FIRST_PAYMENT_REWARD_SUBJECT = "ZIMBOCASH – First Payment Reward";
    public static final String ZASH_MOVED_RECEIVER_SUBJECT = "ZIMBOCASH – Transfer Received";
    public static final String MISSING_ALLOCATION_SUCCESS = "You just received your ZASH rewards";
    public static final String ZASH_MOVED_SENDER_SUBJECT = "Your ZASH has been transferred from your blocked account";
    public static final String PASSWORD_CHANGED = "ZIMBOCASH Password Changed";
    public static final String MEMBER_ID = "member_id";
    public static final String MSG_INVALID_MEMBER_ID = "That doesn’t look like a valid account number. Please check your account number again and re-enter it below.";
    public static final SimpleDateFormat ESOLUTION_SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat SIMPLE_LESS_DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
    public static final SimpleDateFormat SIMPLE_PAYMENT_DATE_FORMAT = new SimpleDateFormat("HH:mm");
    public static final String INTERNAL_ERROR = "Internal error occurred! please try later, if this happen again please contact customer support. ";
    public static final String RESET_CONFIRMATION = "ZIMBOCASH – Reset Confirmation";
    public static final String ENABLE_USER = "ZIMBOCASH – Enable account";
    public static final String DISABLE_USER = "ZIMBOCASH – Disable account";
    public static double TELEGRAM_REWARD;
    public static double FIRST_PAYMENT_REWARD;
    public static double LIMITS;

    public void setTelegramReward(double telegramReward) {
        TELEGRAM_REWARD = telegramReward;
    }

    public void setFirstPaymentReward(double firstPaymentReward) {
        FIRST_PAYMENT_REWARD = firstPaymentReward;
    }

    public void setLIMITS(double limits) {
        Constants.LIMITS = limits;
    }

}
