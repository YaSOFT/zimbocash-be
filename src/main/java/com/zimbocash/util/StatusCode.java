package com.zimbocash.util;

public interface StatusCode {
    int WRONG_FILE_TYPE = 109;
    int BAD_REQUEST = 400;
    int EMPTY_ID_DATA = 108;
    int WRONG_CREDENTIALS = 101;
    int WRONG_PATTERN = 107;
    int USER_NOT_FOUND = 119;
    int USER_NOT_ALLOWED = 120;
    int SUCCESS = 200;
    int INTERNAL_ERROR = 500;
    int UNABLE_TO_SEND_SMS = 123;
    int LEAKED_PASSWORD = 106;
    int LOCKED_ACCOUNT = 110;
    int EXPIRED_PASSWORD = 321;
}
