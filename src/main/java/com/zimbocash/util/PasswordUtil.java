package com.zimbocash.util;

import org.apache.commons.lang3.RandomStringUtils;

import static com.zimbocash.util.Constants.LIVENESS_WORDS;
import static com.zimbocash.util.Constants.NOILO_ALPHABET;
import static com.zimbocash.util.Constants.NOILO_LOWERCASE_ALPHABET;

public class PasswordUtil {
    public static String getRandomString(String charSet, int length) {
        return RandomStringUtils.random(length, charSet != null ? charSet : NOILO_LOWERCASE_ALPHABET);
    }

    public static String getMemberID() {
        return getRandomString(NOILO_LOWERCASE_ALPHABET, 8);
    }

    public static String getRandomPassword(int length) {
        String str = null;
        boolean flag = true;
        while (flag) {
            str = getRandomString(NOILO_ALPHABET, length);
            flag = !checkString(str);
        }

        return str;
    }

    private static boolean checkString(String str) {
        char ch;
        boolean capitalFlag = false;
        boolean lowerCaseFlag = false;
        boolean numberFlag = false;
        for (int i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isDigit(ch)) {
                numberFlag = true;
            } else if (Character.isUpperCase(ch)) {
                capitalFlag = true;
            } else if (Character.isLowerCase(ch)) {
                lowerCaseFlag = true;
            }
            if (numberFlag && capitalFlag && lowerCaseFlag) {
                return true;
            }
        }
        return false;
    }

    public static String getLivenessWord() {
        return LIVENESS_WORDS[(int) (Math.random() * LIVENESS_WORDS.length)];
    }

    public static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (byte b : hash) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
