package com.zimbocash.config.security;

import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zimbocash.external.AmazonSES;
import com.zimbocash.external.impl.InternalRestClient;
import com.zimbocash.facade.ISocketFacade;
import com.zimbocash.model.dtos.SocketBodyDto;
import com.zimbocash.service.AdminService;
import com.zimbocash.service.CustomerService;
import com.zimbocash.service.TelegramService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

@Component
@Slf4j
@RequiredArgsConstructor
@DependsOn(value = {"socketFacade"})
public class AES256Cipher implements PasswordEncoder {
    private static String SECRET_KEY;
    private static String salt;
    private final ObjectMapper objectMapper;
    private final ISocketFacade socketFacade;
    @Value("${aws.secret-name}")
    String secretName;
    @Value("${aws.region}")
    String region;
    private volatile MessageDigest digestSHA256;
    private volatile Cipher aesCipher;
    @Value("${jsonNode.aes256.secretKey}")
    private String aes256SecretKey;
    @Value("${jsonNode.aes256.salt}")
    private String aes256SaltKey;
    @Value("${jsonNode.telegram.token}")
    private String telegramTokenKey;
    @Value("${jsonNode.email.username}")
    private String emailUsernameKey;
    @Value("${jsonNode.email.password}")
    private String emailPasswordKey;
    @Value("${jsonNode.esolution.username}")
    private String esolutionUsernameKey;
    @Value("${jsonNode.esolution.password}")
    private String esolutionPasswordKey;
    @Value("${jsonNode.google.secret}")
    private String googleSecretKey;

    public String getSecret() {
        AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard().withRegion(region).build();
        String secret;
        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest().withSecretId(secretName);
        GetSecretValueResult getSecretValueResult;
        try {
            getSecretValueResult = client.getSecretValue(getSecretValueRequest);
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
        secret = getSecretValueResult.getSecretString();
        if (secret == null) {
            secret = new String(java.util.Base64.getDecoder().decode(getSecretValueResult.getSecretBinary()).array());
        }

        return secret;
    }

    @PostConstruct
    public void init() throws IOException {
        readSecretManagerAndFill();
    }

    private void readSecretManagerAndFill() throws IOException {
        String secret = getSecret();
        if (secret != null && !secret.isEmpty()) {
            objectMapper.configure(JsonReadFeature.ALLOW_UNESCAPED_CONTROL_CHARS.mappedFeature(), true);
            JsonNode jsonNode = objectMapper.readTree(secret);
            if (jsonNode.has(aes256SecretKey)) {
                SECRET_KEY = jsonNode.get(aes256SecretKey).asText();//32bit
            }
            if (jsonNode.has(aes256SaltKey)) {
                salt = jsonNode.get(aes256SaltKey).asText();
            }
            if (jsonNode.has(telegramTokenKey)) {
                AdminService.botToken = TelegramService.botToken = CustomerService.botToken = jsonNode.get(telegramTokenKey).asText();
            }
            if (jsonNode.has(emailUsernameKey)) {
                AmazonSES.emailUsername = jsonNode.get(emailUsernameKey).asText();
            }
            if (jsonNode.has(emailPasswordKey)) {
                AmazonSES.emailPassword = jsonNode.get(emailPasswordKey).asText();
            }
            if (jsonNode.has(esolutionUsernameKey)) {
                InternalRestClient.username = jsonNode.get(esolutionUsernameKey).asText();
            }
            if (jsonNode.has(esolutionPasswordKey)) {
                InternalRestClient.password = jsonNode.get(esolutionPasswordKey).asText();
            }
            if (jsonNode.has(googleSecretKey)) {
                InternalRestClient.googleSecret = jsonNode.get(googleSecretKey).asText();
            }
            initCipher();
        } else {
            socketFacade.send(SocketBodyDto.builder().api("internal readSecretManagerAndFill method").memberIDOrEmail("readSecretManagerAndFill")
                    .message("secret is null or empty").build());
        }
    }

    @SneakyThrows
    public void initCipher() {
        digestSHA256 = MessageDigest.getInstance("SHA-256");
        byte[] keyData = SECRET_KEY.substring(0, 16).getBytes(StandardCharsets.UTF_8);
        aesCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        aesCipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(keyData, "AES"));
    }

    private byte[] getAuthHash(CharSequence charSequence) {
        byte[] hashedPassword = digestSHA256.digest(charSequence.toString().getBytes(StandardCharsets.UTF_8));
        return (salt.concat(Base64.encodeBase64String(hashedPassword))).getBytes(StandardCharsets.UTF_8);
    }

    @Override
    public String encode(CharSequence charSequence) {
        try {
            return new String(java.util.Base64.getEncoder().encode(aesCipher.doFinal(getAuthHash(charSequence))));
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            log.error("error occurred while hashing {}", e.getMessage());
            return null;
        }
    }

    @Override
    public boolean matches(CharSequence charSequence, String encryptedHash256) {
        try {
            return new String(java.util.Base64.getEncoder().encode(aesCipher.doFinal(getAuthHash(charSequence)))).equals(encryptedHash256);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            log.error("error occurred while hashing {}", e.getMessage());
            socketFacade.send(SocketBodyDto.builder().api("internal matches method").memberIDOrEmail("matches")
                    .message(e.getMessage()).build());
            return false;
        }
    }
}
