package com.zimbocash.config.security.jwt;

import com.zimbocash.crypto.RSASignature;
import com.zimbocash.enums.Claims;
import com.zimbocash.enums.Roles;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static com.zimbocash.enums.TokenType.REFRESH;
import static com.zimbocash.enums.TokenType.TOKEN;

@Component
@RequiredArgsConstructor
public class JwtProvider {
    @Value("${jwt.expireTime}")
    long expireTime;
    @Value("${jwt.issuer}")
    private String ISSUER;
    @Value("${zimbo.extension}")
    private String zimboExtension;

    public String generateToken(String memberID, boolean isAdmin, boolean isManager, boolean isRefresh) {
        Set<String> roles = new HashSet<>();
        if (isAdmin) {
            roles.add(Roles.ADMIN.getLevel());
        } else {
            if (isManager) {
                roles.add(Roles.MANAGER.getLevel());
            } else {
                roles.add(Roles.USER.getLevel());
            }
        }
        long expireDate = new Date().getTime() + (1000 * (isAdmin || isManager ? 60 * 60 * 24 * 365 : expireTime));
        return Jwts.builder()
                .setSubject(isRefresh ? REFRESH.getName() : TOKEN.getName())
                .setId(UUID.randomUUID().toString())
                .setIssuer(ISSUER)
                .signWith(RSASignature.jwtPrivateKey)
                .claim("issuerID", memberID.concat(zimboExtension))
                .claim(Claims.upn.name(), memberID)
                .setExpiration(new Date(expireDate))
                .claim(Claims.groups.name(), new ArrayList<>(roles))
                .compact();
    }
}
