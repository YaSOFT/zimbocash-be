package com.zimbocash.config.security.jwt;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "application.jwt")
@Component
@Data
@NoArgsConstructor
public class JwtConfig {
    private String tokenPrefix;
    private Integer tokenExpirationAfterS;

}
