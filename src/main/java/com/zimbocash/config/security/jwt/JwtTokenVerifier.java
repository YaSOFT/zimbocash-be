package com.zimbocash.config.security.jwt;

import com.google.common.base.Strings;
import com.zimbocash.config.security.AuthenticationInterceptor;
import com.zimbocash.crypto.RSASignature;
import com.zimbocash.dao.CustomerRepository;
import com.zimbocash.enums.Roles;
import com.zimbocash.facade.ISocketFacade;
import com.zimbocash.model.dtos.SocketBodyDto;
import com.zimbocash.model.entity.CustomerTransaction;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.zimbocash.enums.TokenType.REFRESH;

@Component
@RequiredArgsConstructor
@Slf4j
@DependsOn(value = {"socketFacade"})
public class JwtTokenVerifier extends OncePerRequestFilter {
    private static List<String> memberIDsDEVS = Arrays.asList("qrj9nzbe", "cpbk3uyt", "925f5tas");
    private final JwtConfig jwtConfig;
    private final CustomerRepository customerRepository;
    private final HandlerExceptionResolver handlerExceptionResolver;
    //if the phone is not verified, then allow access only to those apis
    private final ISocketFacade socketFacade;
    @Value("${api.url.updateUrl}")
    private String updateUrl;
    @Value("${api.url.sendOtpUrl}")
    private String sendOtpUrl;
    @Value("${api.url.verifyOtpUrl}")
    private String verifyOtpUrl;
    @Value("${api.url.refreshAuthUrl}")
    private String refreshAuthUrl;
    @Value("${api.url.moveWalletUrl}")
    private String moveWalletUrl;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authorizationHeader = request.getRequestURI().startsWith("/zash/socket") && request.getParameter("authToken") != null
                ? request.getParameter("authToken") : request.getHeader(HttpHeaders.AUTHORIZATION);
        if (Strings.isNullOrEmpty(authorizationHeader) || !authorizationHeader.startsWith(jwtConfig.getTokenPrefix())) {
            filterChain.doFilter(request, response);
            return;
        }

        String token = authorizationHeader.replace(jwtConfig.getTokenPrefix(), "");

        try {
            Jws<Claims> claimsJws = Jwts.parser()
                    .setSigningKey(RSASignature.jwtPublicKey)
                    .parseClaimsJws(token);

            Claims body = claimsJws.getBody();
            String subject = body.getSubject();

            if (subject.equalsIgnoreCase(REFRESH.getName())) {
                handlerExceptionResolver.resolveException(request, response, null, new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "cannot login with refresh token"));
            }

            List<String> authorities = (List<String>) body.get("groups");
            String memberID = body.get("upn").toString();

            Set<SimpleGrantedAuthority> simpleGrantedAuthorities = authorities.stream()
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toSet());
            Authentication authentication = new UsernamePasswordAuthenticationToken(memberID, null, simpleGrantedAuthorities);
            final boolean[] canAccess = {true};
            customerRepository.findCustomerByMemberID(memberID).ifPresent(customer -> {
                String api = ServletUriComponentsBuilder.fromCurrentRequestUri().build().toUri().getPath();
                if (!customer.isEnabled()) {
                    log.info("customer -> {} disabled, response -> {}", customer.getMemberID(), 406);
                    handlerExceptionResolver.resolveException(request, response, null, new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "customer -> disabled"));
                    canAccess[0] = false;
                }
                CustomerTransaction customerTransaction = customer.getCustomerTransaction();
                if (!customer.isAdmin() && !customer.isManager()) {
                    if (customer.isNewUser()) {
                        if (customerTransaction != null &&
                                customer.isIdFailed() && (!api.startsWith(moveWalletUrl))) {
                            log.info("new customer -> {} move, response -> {}", customer.getMemberID(), 406);
                            handlerExceptionResolver
                                    .resolveException(request, response, null, new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "new customer -> move"));
                            canAccess[0] = false;

                        }
                    }
                    if ((!api.equals(updateUrl) && !api.equals(sendOtpUrl)
                            && !api.equals(verifyOtpUrl)
                            && !api.equals(refreshAuthUrl)) && !customer.isPhoneVerified()) {
                        log.info("customer -> {} verify, response -> {}", customer.getMemberID(), 406);
                        handlerExceptionResolver.resolveException(request, response, null, new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "customer -> verify"));
                        canAccess[0] = false;
                    }
                }
            });
            if (canAccess[0]) {
                SecurityContextHolder.getContext().setAuthentication(authentication);
                if (request.getRequestURI().startsWith("/zash/socket")) {
                    if (!(memberIDsDEVS.contains(AuthenticationInterceptor.getLoggedInUserMemberId())
                            || AuthenticationInterceptor.getRole().equals(Roles.ADMIN.getLevel()))) {
                        SecurityContextHolder.clearContext();
                    }
                }
            } else {
                return;
            }
        } catch (JwtException e) {
            socketFacade.send(SocketBodyDto.builder().api("internal doFilterInternal method").memberIDOrEmail("JwtTokenVerifier")
                    .message(e.getMessage()).build());
            handlerExceptionResolver.resolveException(request, response, null, e);
            return;
        }

        filterChain.doFilter(request, response);
    }
}
