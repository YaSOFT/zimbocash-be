package com.zimbocash.config.security;

import com.zimbocash.enums.Roles;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
//todo remove this and save memberid in memory
public class AuthenticationInterceptor {
    public static String getLoggedInUserMemberId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (authentication == null) ? null : authentication.getName();
    }

    public static String getRole() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (authentication == null) ? Roles.ANONYMOUS.getLevel() : authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority).findFirst().orElse(Roles.ANONYMOUS.getLevel());
    }
}
