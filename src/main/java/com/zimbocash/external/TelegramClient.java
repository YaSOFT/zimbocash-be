package com.zimbocash.external;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@RequiredArgsConstructor
@Component
public class TelegramClient {
    private final RestTemplate restTemplate;
    @Value("${api.TelegramClient.url}")
    private String url;

    public void sendMessage(String token, String chatId, String text) {
        /*
        String requestUrl = url + "bot" + token + "/sendMessage";
        requestUrl = UriComponentsBuilder.fromHttpUrl(requestUrl).queryParam("chat_id", chatId).queryParam("text", text).toUriString();
        return restTemplate.getForEntity(requestUrl, String.class).getBody();
         */
    }
}
