package com.zimbocash.external.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zimbocash.commons.CustomResponseDto;
import com.zimbocash.external.GoogleClientHttp;
import com.zimbocash.external.sms.SmsSender;
import com.zimbocash.model.dtos.ESolutionMessageDto;
import com.zimbocash.util.StatusCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class InternalRestClient {
    public static volatile String username;
    public static volatile String password;
    public static volatile String googleSecret;
    private final ObjectMapper objectMapper;
    private final SmsSender smsSender;
    private final GoogleClientHttp googleClientHttp;
    private final List<String> econetCodes = List.of("77", "78");
    private String failedMessage = "message with id: %s failed to deliver due %s";

    public boolean verifyReCaptcha(String responseCaptcha) {
        try {
            String jsonObject = googleClientHttp.verifyReCaptcha(googleSecret, responseCaptcha);
            JsonNode jsonNode = objectMapper.readTree(jsonObject);
            return jsonNode.has("success") && jsonNode.get("success").isBoolean() && jsonNode.get("success").asBoolean();
        } catch (IOException e) {
            return false;
        }
    }


    public CustomResponseDto publishSms(String message, String phone) {
        CustomResponseDto customResponseDto = CustomResponseDto.ok();
        customResponseDto.setStatusCode(200);
        String code = phone.substring(4, 6);
        try {
            boolean wasMessageSent;
            if (econetCodes.contains(code)) {
                wasMessageSent = smsSender.sendWithEconet(phone, message);
            } else {
                wasMessageSent = smsSender.sendWithNetOne(phone, message);
            }
            if (!wasMessageSent) {
                String jsonObject = smsSender.sendWithEsolutions(phone, message);
                ESolutionMessageDto eSolutionMessageDtoResponse = objectMapper.readValue(jsonObject, ESolutionMessageDto.class);
                if (eSolutionMessageDtoResponse.getStatus().equals(ESolutionMessageDto.Status.FAILED)) {
                    customResponseDto.setMessage(String.format(failedMessage, eSolutionMessageDtoResponse.getMessageId(), eSolutionMessageDtoResponse.getNarrative()));
                    customResponseDto.setStatusCode(StatusCode.UNABLE_TO_SEND_SMS);
                    customResponseDto.setSuccess(false);
                }
            }
        } catch (Exception e) {
            customResponseDto.setMessage(e.getMessage());
            customResponseDto.setStatusCode(StatusCode.UNABLE_TO_SEND_SMS);
            customResponseDto.setSuccess(false);
        }
        return customResponseDto;
    }
}
