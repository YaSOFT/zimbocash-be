package com.zimbocash.external;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@RequiredArgsConstructor
@Component
public class PvtClientHttp {
    private final RestTemplate pvtHotWalletRestTemplate;
    @Value("${api.PvtClientHttp.url}")
    private String url;

    //should return the transaction as jsonobject string with signature field
    public byte[] sign(String entity) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return pvtHotWalletRestTemplate.postForEntity(url + "sign", new HttpEntity<>(entity, httpHeaders), byte[].class).getBody();
    }

    //should return new public key
    public byte[] createAccount() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return pvtHotWalletRestTemplate
                .exchange(UriComponentsBuilder.fromHttpUrl(url + "new").build().toUri(), HttpMethod.GET, new HttpEntity<>(httpHeaders), byte[].class).getBody();
    }

}
