package com.zimbocash.external;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.rekognition.model.CompareFacesMatch;
import com.amazonaws.services.rekognition.model.CompareFacesRequest;
import com.amazonaws.services.rekognition.model.CompareFacesResult;
import com.amazonaws.services.rekognition.model.DetectTextFilters;
import com.amazonaws.services.rekognition.model.DetectTextRequest;
import com.amazonaws.services.rekognition.model.DetectTextResult;
import com.amazonaws.services.rekognition.model.DetectionFilter;
import com.amazonaws.services.rekognition.model.FaceMatch;
import com.amazonaws.services.rekognition.model.Image;
import com.amazonaws.services.rekognition.model.IndexFacesRequest;
import com.amazonaws.services.rekognition.model.QualityFilter;
import com.amazonaws.services.rekognition.model.S3Object;
import com.amazonaws.services.rekognition.model.SearchFacesByImageRequest;
import com.amazonaws.services.rekognition.model.SearchFacesByImageResult;
import com.amazonaws.services.rekognition.model.TextDetection;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.zimbocash.dao.CustomerRepository;
import com.zimbocash.facade.ISocketFacade;
import com.zimbocash.model.dtos.SocketBodyDto;
import com.zimbocash.model.entity.Customer;
import com.zimbocash.model.entity.DuplicatedImage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class AWSHttp {
    private final ISocketFacade socketFacade;
    private final CustomerRepository customerRepository;
    @Value("${aws.region}")
    String region;
    @Value("${aws.bucket-name}")
    String bucketName;
    @Value("${aws.collection-id}")
    String collectionId;

    public Double getSimilarity(String source, String target) {
        try {

            Image sourceImage = new Image().withS3Object(new S3Object().withName(source).withBucket(bucketName));
            Image targetImage = new Image().withS3Object(new S3Object().withName(target).withBucket(bucketName));

            CompareFacesRequest compareRequest = new CompareFacesRequest()
                    .withSourceImage(sourceImage)
                    .withTargetImage(targetImage)
                    .withSimilarityThreshold(80F);

            CompareFacesResult compareFacesResult = AmazonRekognitionClientBuilder.defaultClient()
                    .compareFaces(compareRequest);
            List<CompareFacesMatch> faceMatches = compareFacesResult.getFaceMatches();
            if (faceMatches.size() == 1) {
                CompareFacesMatch compareFacesMatch = faceMatches.get(0);
                Float similarity = compareFacesMatch.getSimilarity();
                return Double.valueOf(similarity);
            }
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api("internal getSimilarity method").memberIDOrEmail("source " + source + " , target " + target)
                    .message(e.getMessage()).build());
        }
        socketFacade.send(SocketBodyDto.builder().api("internal getSimilarity method").memberIDOrEmail("source " + source + " , target " + target)
                .message("similarity 0").build());
        return (double) 0;
    }

    public List<TextDetection> getTextDetections(String imageName, float minimumConfidence) {
        DetectTextRequest detectTextRequest = new DetectTextRequest()
                .withImage(new Image()
                        .withS3Object(new S3Object().withName(imageName).withBucket(bucketName)));

        if (minimumConfidence != 0F) {
            detectTextRequest = detectTextRequest
                    .withFilters(new DetectTextFilters().withWordFilter(new DetectionFilter().withMinConfidence(minimumConfidence)));
        }
        DetectTextResult result = AmazonRekognitionClientBuilder.defaultClient().detectText(detectTextRequest);
        return result.getTextDetections();
    }

    public boolean searchForDuplicateFace(String target, String memberId, String type, String idNumber, boolean isSaveFace) {
        log.info("searchForDuplicateFace() method -> {}", type);
        Customer customer = customerRepository.findCustomerByMemberID(memberId).orElse(null);
        if (customer != null) {
            try {
                AmazonRekognition rekognitionClient = AmazonRekognitionClientBuilder.defaultClient();
                Image image = new Image().withS3Object(new S3Object().withName(target).withBucket(bucketName));
                SearchFacesByImageRequest searchFacesByImageRequest = new SearchFacesByImageRequest()
                        .withCollectionId(collectionId + type)
                        .withImage(image)
                        .withFaceMatchThreshold(99F)
                        .withMaxFaces(1);

                SearchFacesByImageResult searchFacesByImageResult =
                        rekognitionClient.searchFacesByImage(searchFacesByImageRequest);

                List<FaceMatch> faceImageMatches = searchFacesByImageResult.getFaceMatches();
                if (faceImageMatches.size() > 0) {
                    for (FaceMatch face : faceImageMatches) {
                        String idNumberTrimmed = face.getFace().getExternalImageId()
                                .replaceAll("(CIT|REG|ALIEN)*", "")
                                .replaceAll("\\s*", "");

                        if (idNumberTrimmed.equalsIgnoreCase(idNumber)) {
                            socketFacade.send(SocketBodyDto.builder().api("internal searchForDuplicateFace method").memberIDOrEmail(memberId)
                                    .message("idNumberTrimmed " + idNumberTrimmed + " same as " + idNumber).build());
                            return true;
                        }
                        List<DuplicatedImage> duplicatedImages = customer.getDuplicatedImages();
                        duplicatedImages.add(DuplicatedImage
                                .builder()
                                .originalIDNumber(idNumberTrimmed)
                                .type(type)
                                .build());
                        //basically it should update without setters, but we keep it in case
                        customer.setDuplicatedImages(duplicatedImages);
                        customerRepository.save(customer);
                        log.info("searchForDuplicateFace found -> memberId = {} , face duplicate similarity {}", memberId, face.getSimilarity());
                    }
                    return false;
                } else {
                    if (isSaveFace) {
                        socketFacade.send(SocketBodyDto.builder().api("internal searchForDuplicateFace method").memberIDOrEmail(memberId)
                                .message("not found similar , save face").build());
                        saveFaceMetadata(target, idNumber, type);
                        return true;
                    }
                }
            } catch (Exception e) {
                socketFacade.send(SocketBodyDto.builder().api("internal searchForDuplicateFace method").memberIDOrEmail(memberId)
                        .message(e.getMessage()).build());
                log.error("searchForDuplicateFace aws issue\t" + e.getMessage());
                List<DuplicatedImage> duplicatedImages = customer.getDuplicatedImages();
                duplicatedImages.add(DuplicatedImage
                        .builder()
                        .originalIDNumber(customer.getIdNumber().replaceAll("\\s*", ""))
                        .type(type)
                        .build());
                //basically it should update without setters, but we keep it in case
                customer.setDuplicatedImages(duplicatedImages);
                customerRepository.save(customer);
                return false;
            }
        }
        return false;
    }

    public void saveFaceMetadata(String target, String idNumber, String type) {
        log.info("saveFaceMetadata() method");

        AmazonRekognition rekognitionClient = AmazonRekognitionClientBuilder.defaultClient();
        Image image = new Image().withS3Object(new S3Object().withName(target).withBucket(bucketName));

        IndexFacesRequest indexFacesRequest = new IndexFacesRequest()
                .withImage(image)
                .withQualityFilter(QualityFilter.AUTO)
                .withMaxFaces(1)
                .withExternalImageId(idNumber)
                .withCollectionId(collectionId + type)
                .withDetectionAttributes("DEFAULT");
        rekognitionClient.indexFaces(indexFacesRequest);

    }

    public String generateUploadPreSignedUrl(String fileName) {
        log.info("generateUploadPreSignedUrl() method, fileName -> {}", fileName);
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(region).withAccelerateModeEnabled(true).build();
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, fileName);
        java.util.Date expiration = new java.util.Date();
        if (fileName.endsWith("_liveness.jpeg")) {
            expiration.setTime(expiration.getTime() + 1000 * 60 * 30);
        } else {
            expiration.setTime(expiration.getTime() + 1000 * 60 * 20);
        }
        generatePresignedUrlRequest.setMethod(HttpMethod.PUT);
        generatePresignedUrlRequest.setExpiration(expiration);
        generatePresignedUrlRequest.setContentType("image/jpeg");
        generatePresignedUrlRequest.setZeroByteContent(false);
        URL url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
        return String.valueOf(url);
    }

    public String generateDownloadPreSignedUrl(String fileName) {
        log.info("generateDownloadPreSignedUrl() method, fileName -> {}", fileName);
        AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(region).withAccelerateModeEnabled(true).build();
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, fileName);
        java.util.Date expiration = new java.util.Date();
        expiration.setTime(expiration.getTime() + 1000 * 60 * 5);
        generatePresignedUrlRequest.setMethod(HttpMethod.GET);
        generatePresignedUrlRequest.setExpiration(expiration);
        generatePresignedUrlRequest.setZeroByteContent(false);
        URL url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
        return String.valueOf(url);
    }
}
