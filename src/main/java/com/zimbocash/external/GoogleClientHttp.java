package com.zimbocash.external;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;


@RequiredArgsConstructor
@Component
public class GoogleClientHttp {
    private final RestTemplate restTemplate;
    @Value("${api.GoogleClientHttp.url}")
    private String url;

    public String verifyReCaptcha(String secret, String response) {
        UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("secret", secret)
                .queryParam("response", response).build();
        return restTemplate.exchange(uriComponents.toUriString(), HttpMethod.POST, new HttpEntity<>(null), String.class).getBody();
    }
}
