package com.zimbocash.external;

import com.zimbocash.facade.impl.SocketFacade;
import com.zimbocash.model.SMTPCredentials;
import com.zimbocash.model.dtos.SocketBodyDto;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.annotation.PostConstruct;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import static com.zimbocash.util.Constants.ACCOUNT_CONFIRM_SUBJECT;
import static com.zimbocash.util.Constants.BLOCKED_ACCOUNT_SUBJECT;
import static com.zimbocash.util.Constants.CONFIRMED_ACCOUNT_SUBJECT;
import static com.zimbocash.util.Constants.DISABLE_USER;
import static com.zimbocash.util.Constants.ENABLE_USER;
import static com.zimbocash.util.Constants.FIRST_PAYMENT_REWARD_SUBJECT;
import static com.zimbocash.util.Constants.FROM;
import static com.zimbocash.util.Constants.FROMNAME;
import static com.zimbocash.util.Constants.MISSING_ALLOCATION_SUCCESS;
import static com.zimbocash.util.Constants.PASSWORD_CHANGED;
import static com.zimbocash.util.Constants.PASSWORD_RESET_SUBJECT;
import static com.zimbocash.util.Constants.REDUCING_SUBJECT;
import static com.zimbocash.util.Constants.REMINDER_SUBJECT;
import static com.zimbocash.util.Constants.RESET_CONFIRMATION;
import static com.zimbocash.util.Constants.REWARD_RECEIVED_SUBJECT;
import static com.zimbocash.util.Constants.REWARD_REFERRAL_SUBJECT;
import static com.zimbocash.util.Constants.SIMPLE_PAYMENT_DATE_FORMAT;
import static com.zimbocash.util.Constants.TRANSACTION_RECIEVED_SUBJECT;
import static com.zimbocash.util.Constants.TRANSACTION_SENT_SUBJECT;
import static com.zimbocash.util.Constants.WELCOME_SUBJECT;
import static com.zimbocash.util.Constants.ZASH_MOVED_RECEIVER_SUBJECT;
import static com.zimbocash.util.Constants.ZASH_MOVED_SENDER_SUBJECT;

@Slf4j
@Component
public class AmazonSES {
    private static final DecimalFormat decimalFormat = new DecimalFormat("#.#######");
    public static volatile String emailUsername;
    public static volatile String emailPassword;
    private static volatile SMTPCredentials smtpCredentials;
    @Value("${email.port}")
    String emailPort;
    @Value("${email.host}")
    String emailHost;
    @Autowired
    private Configuration freeMarkerConfig;
    private Template passwordChanged;
    private Template passwordConfirmAccount;
    private Template passwordReset;
    private Template senderEmail;
    private Template welcomeEmail;
    private Template receiverEmail;
    private Template accountBlocked;
    private Template accountConfirmed;
    private Template firstPaymentReward;
    private Template firstPaymentRewardSender;
    private Template receivedReward;
    private Template rewardReferral;
    private Template rewardReferralFromUserB;
    private Template zashMovedReceiver;
    private Template zashMovedSender;
    private Template reducingBalance;
    private Template firstReminderEmail;
    private Template secondReminderEmail;
    private Template missedPaymentSuccess;
    private Template resetConfirmation;
    private Template enableUser;
    private Template disableUser;
    @Autowired
    private SocketFacade socketFacade;

    public void initializeTemplates() throws IOException {
        accountBlocked = freeMarkerConfig.getTemplate("accountBlocked.html");
        accountConfirmed = freeMarkerConfig.getTemplate("accountConfirmed.html");
        disableUser = freeMarkerConfig.getTemplate("disableUser.html");
        enableUser = freeMarkerConfig.getTemplate("enableUser.html");
        firstPaymentReward = freeMarkerConfig.getTemplate("firstPaymentReward.html");
        firstPaymentRewardSender = freeMarkerConfig.getTemplate("firstPaymentRewardSender.html");
        missedPaymentSuccess = freeMarkerConfig.getTemplate("missedPaymentSuccess.html");
        passwordChanged = freeMarkerConfig.getTemplate("passwordChanged.html");
        passwordConfirmAccount = freeMarkerConfig.getTemplate("passwordConfirmAccount.html");
        passwordReset = freeMarkerConfig.getTemplate("passwordReset.html");
        receivedReward = freeMarkerConfig.getTemplate("receivedReward.html");
        receiverEmail = freeMarkerConfig.getTemplate("receiverEmail.html");
        reducingBalance = freeMarkerConfig.getTemplate("reducingBalance.html");
        resetConfirmation = freeMarkerConfig.getTemplate("resetConfirmation.html");
        rewardReferral = freeMarkerConfig.getTemplate("rewardReferral.html");
        rewardReferralFromUserB = freeMarkerConfig.getTemplate("rewardReferralFromUserB.html");
        senderEmail = freeMarkerConfig.getTemplate("senderEmail.html");
        welcomeEmail = freeMarkerConfig.getTemplate("welcomeEmail.html");
        zashMovedReceiver = freeMarkerConfig.getTemplate("zashMovedReceiver.html");
        zashMovedSender = freeMarkerConfig.getTemplate("zashMovedSender.html");
        firstReminderEmail = freeMarkerConfig.getTemplate("firstEmailReminder.html");
        secondReminderEmail = freeMarkerConfig.getTemplate("secondEmailReminder.html");
    }

    @PostConstruct
    public void init() throws IOException {
        smtpCredentials = SMTPCredentials.builder().smtpUsername(emailUsername).smtpPassword(emailPassword).host(emailHost).port(emailPort).build();
        initializeTemplates();
    }

    public void sendPasswordChanged(String email) {
        sendEmail(email, PASSWORD_CHANGED, passwordChanged, Collections.emptyMap());
    }

    public void sendZashMovedToSender(String email, String memberID, double amount, String transactionID) {
        sendEmail(email,
                ZASH_MOVED_SENDER_SUBJECT,
                zashMovedSender, Map.of("amount", decimalFormat.format(amount), "transactionID", transactionID, "memberID", memberID, "date", SIMPLE_PAYMENT_DATE_FORMAT.format(new Date())));
    }

    public void sendZashMovedToReceiver(String email, String name, double amount, String transactionID) {
        sendEmail(email,
                ZASH_MOVED_RECEIVER_SUBJECT,
                zashMovedReceiver, Map.of("amount", decimalFormat.format(amount), "transactionID", transactionID, "name", name, "date", SIMPLE_PAYMENT_DATE_FORMAT.format(new Date())));
    }

    public void sendRewardReferralFromUserB(String email, String memberID, String name) {
        sendEmail(email,
                REWARD_REFERRAL_SUBJECT,
                rewardReferralFromUserB, Map.of("memberID", memberID, "name", name));
    }

    public void sendRewardReferral(String email, String howMuch, String memberID, String name) {
        sendEmail(email,
                REWARD_REFERRAL_SUBJECT,
                rewardReferral, Map.of("howMuch", howMuch, "memberID", memberID, "name", name));
    }

    public void sendReceivedReward(String email, String howMuch, String memberID, String name) {
        sendEmail(email,
                REWARD_RECEIVED_SUBJECT,
                receivedReward, Map.of("howMuch", howMuch, "memberID", memberID, "name", name));
    }

    public void sendFirstPaymentRewardToSender(String email, String name, String memberID) {
        sendEmail(email,
                FIRST_PAYMENT_REWARD_SUBJECT,
                firstPaymentRewardSender, Map.of("name", name, "memberID", memberID));
    }

    public void sendFirstPaymentReward(String email, String name, String memberID) {
        sendEmail(email,
                FIRST_PAYMENT_REWARD_SUBJECT,
                firstPaymentReward, Map.of("name", name, "memberID", memberID));
    }

    public void sendAccountBlocked(String email) {
        sendEmail(email,
                BLOCKED_ACCOUNT_SUBJECT,
                accountBlocked, Collections.emptyMap());
    }

    public void sendAccountConfirmed(String email) {
        sendEmail(email,
                CONFIRMED_ACCOUNT_SUBJECT,
                accountConfirmed, Collections.emptyMap());
    }

    public void sendPasswordConfirmAccount(String email, String password) {
        sendEmail(email, ACCOUNT_CONFIRM_SUBJECT + SIMPLE_PAYMENT_DATE_FORMAT.format(new Date()), passwordConfirmAccount, Map.of("code", password));
    }

    public void sendPasswordResetEmail(String email, String password) {
        sendEmail(email, PASSWORD_RESET_SUBJECT + SIMPLE_PAYMENT_DATE_FORMAT.format(new Date()), passwordReset, Map.of("code", password));
    }

    public void sendSenderEmail(String fullName, String email, double amount, String transactionID,
                                String paymentMessage, String receiverMemberID) {
        sendEmail(email,
                TRANSACTION_SENT_SUBJECT,
                senderEmail, Map.of("date", SIMPLE_PAYMENT_DATE_FORMAT.format(new Date()), "name", fullName,
                        "receiverMemberID", receiverMemberID, "transactionID", transactionID,
                        "amount", decimalFormat.format(amount), "paymentMessage", paymentMessage.trim()));
    }

    public void sendReceiverEmail(String fullName, String email, double amount, String transactionID,
                                  String paymentMessage, String senderName) {
        String sender;
        if (senderName.contains("+263")) {
            sender = "+263*****" + senderName.substring(senderName.length() - 4);
        } else {
            sender = senderName;
        }
        sendEmail(email,
                TRANSACTION_RECIEVED_SUBJECT,
                receiverEmail, Map.of("date", SIMPLE_PAYMENT_DATE_FORMAT.format(new Date()), "name", fullName,
                        "transactionID", transactionID, "paymentMessage", Optional.ofNullable(paymentMessage).orElse("").trim(),
                        "amount", decimalFormat.format(amount), "senderName", sender));
    }

    public void sendReducingEmail(String email, String firstName) {
        sendEmail(email, REDUCING_SUBJECT, reducingBalance, Map.of("fullName", firstName));
    }

    public void sendFirstReminderEmail(String email, String firstName) {
        sendEmail(email, REMINDER_SUBJECT, firstReminderEmail, Map.of("fullName", firstName));
    }

    public void sendSecondReminderEmail(String email, String firstName) {
        sendEmail(email, REMINDER_SUBJECT, secondReminderEmail, Map.of("fullName", firstName));
    }

    public void sendWelcomeEmail(String email, String memberID) {
        sendEmail(email, WELCOME_SUBJECT,
                welcomeEmail, Map.of("memberID", memberID));
    }

    public void sendMissedPaymentSuccess(String email, String fullName, double amount) {
        Map<String, Object> model = new HashMap<>();
        model.put("fullName", fullName);
        model.put("amount", decimalFormat.format(amount));
        sendEmail(email, MISSING_ALLOCATION_SUCCESS,
                missedPaymentSuccess, Map.of("amount", decimalFormat.format(amount), "fullName", fullName));
    }

    public void sendResetConfirmation(String email, String firstName) {
        sendEmail(email, RESET_CONFIRMATION, resetConfirmation, Map.of("fullName", firstName));
    }

    public void sendEnableUser(String email) {
        sendEmail(email, ENABLE_USER, enableUser, Collections.emptyMap());
    }

    public void sendDisableUSer(String email, String firstName) {
        sendEmail(email, DISABLE_USER, disableUser, Map.of("fullName", firstName));
    }

    private void sendEmail(String email, String subject, freemarker.template.Template template, Map<String, Object> model) {
        // Create a Properties object to contain connection configuration information.
        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.ssl.trust", smtpCredentials.getHost());
        props.put("mail.smtp.port", Integer.valueOf(smtpCredentials.getPort()));
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        try {
            // Create a Session object to represent a mail session with the specified
            // properties.
            Session session = Session.getDefaultInstance(props);

            // Create a message with the specified information.
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(FROM, FROMNAME));
            String body = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);

            msg.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
            msg.setSubject(subject);
            msg.setContent(body, "text/html");

            // Create a transport.
            Transport transport = session.getTransport();

            // Connect to Amazon SES using the SMTP username and password you specified
            // above.
            transport.connect(smtpCredentials.getHost(), smtpCredentials.getSmtpUsername(), smtpCredentials.getSmtpPassword());

            // Send the email.
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
        } catch (Exception ex) {
            socketFacade.send(SocketBodyDto.builder().api("internal sendEmail method").memberIDOrEmail(email)
                    .message(ex.getMessage()).build());
            log.info("The email was not sent.");
            log.error(ex.getMessage(), ex);
        }
    }
}
