package com.zimbocash.external.sms;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.zimbocash.facade.ISocketFacade;
import com.zimbocash.model.dtos.SmsList;
import com.zimbocash.model.dtos.SocketBodyDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;


@RequiredArgsConstructor
@Component
@Slf4j
public class EconetSmsSender {
    private final RestTemplate restTemplate;
    private final ISocketFacade socketFacade;

    @Value("${api.Econet.url}")
    private String econetUrl;

    @Value("${api.Econet.login}")
    private String econetLogin;

    @Value("${api.Econet.password}")
    private String econetPassword;


    public boolean sendSms(String phoneNumber, String sms) {
        String requestUrl = UriComponentsBuilder.fromHttpUrl(econetUrl)
                .queryParam("user", econetLogin)
                .queryParam("password", econetPassword)
                .queryParam("password", econetPassword)
                .queryParam("mobiles", phoneNumber)
                .queryParam("sms", sms).toUriString();
        String body = restTemplate.getForEntity(requestUrl, String.class).getBody();
        try {
            SmsList smsList = new XmlMapper().readValue(body, SmsList.class);
            return smsList.getError() == null;
        } catch (IOException e) {
            socketFacade.send(SocketBodyDto.builder().api("internal sendSmsToEconet method").memberIDOrEmail(phoneNumber)
                    .message(e.getMessage()).build());
            log.error("Cannot construct smsList from " + body, e);
            return false;
        }
    }
}
