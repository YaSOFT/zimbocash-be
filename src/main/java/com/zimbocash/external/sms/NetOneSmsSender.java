package com.zimbocash.external.sms;

import lombok.extern.slf4j.Slf4j;
import org.jsmpp.InvalidResponseException;
import org.jsmpp.PDUException;
import org.jsmpp.bean.Alphabet;
import org.jsmpp.bean.BindType;
import org.jsmpp.bean.ESMClass;
import org.jsmpp.bean.GeneralDataCoding;
import org.jsmpp.bean.MessageClass;
import org.jsmpp.bean.NumberingPlanIndicator;
import org.jsmpp.bean.RegisteredDelivery;
import org.jsmpp.bean.SMSCDeliveryReceipt;
import org.jsmpp.bean.TypeOfNumber;
import org.jsmpp.extra.NegativeResponseException;
import org.jsmpp.extra.ResponseTimeoutException;
import org.jsmpp.session.BindParameter;
import org.jsmpp.session.SMPPSession;
import org.jsmpp.util.AbsoluteTimeFormatter;
import org.jsmpp.util.TimeFormatter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;

@Service
@Slf4j
public class NetOneSmsSender {

    private static final TimeFormatter TIME_FORMATTER = new AbsoluteTimeFormatter();
    @Value("${api.NetOne.address}")
    private String address;
    @Value("${api.NetOne.port}")
    private int port;
    @Value("${api.NetOne.username}")
    private String username;
    @Value("${api.NetOne.password}")
    private String password;
    @Value("${api.NetOne.originator}")
    private String originator;

    public boolean sendSms(String message, String phoneNumber) {
        return broadcastMessage(message, phoneNumber);
    }

    private boolean broadcastMessage(String message, String number) {
        boolean wasMessageSent = false;
        SMPPSession session = initSession();
        if (session != null) {
            try {
                String messageNumber = session.submitShortMessage("cmt",
                        TypeOfNumber.ALPHANUMERIC, NumberingPlanIndicator.ISDN,
                        originator, TypeOfNumber.INTERNATIONAL, NumberingPlanIndicator.ISDN,
                        number.substring(1),
                        new ESMClass(), (byte) 0, (byte) 1, TIME_FORMATTER.format(new Date()), null,
                        new RegisteredDelivery(SMSCDeliveryReceipt.DEFAULT), (byte) 0, new GeneralDataCoding(Alphabet.ALPHA_DEFAULT, MessageClass.CLASS1, false), (byte) 0,
                        message.getBytes());
                Thread.sleep(1000);
                wasMessageSent = true;
                log.info("Message successfully sent, number: {}, message: {}, message number: {}", number, message, messageNumber);
            } catch (PDUException e) {
                log.error("Invalid PDU parameter", e);
            } catch (ResponseTimeoutException e) {
                log.error("Response timeout", e);
            } catch (InvalidResponseException e) {
                log.error("Receive invalid response", e);
            } catch (NegativeResponseException e) {
                log.error("Receive negative response", e);
            } catch (IOException e) {
                log.error("I/O error occurred", e);
            } catch (Exception e) {
                log.error("Exception occurred submitting SMPP request", e);
            }
        } else {
            log.error("Session creation failed with SMPP broker.");
        }
        if (session != null) {
            session.unbindAndClose();
        }
        return wasMessageSent;
    }

    private SMPPSession initSession() {
        SMPPSession session = new SMPPSession();
        session.setTransactionTimer(3000);
        try {
            String systemId = session.connectAndBind(address, port,
                    new BindParameter(BindType.BIND_TRX, username, password, "cp", TypeOfNumber.UNKNOWN, NumberingPlanIndicator.UNKNOWN, null));
            log.info("Connected with SMPP with system id {}", systemId);
        } catch (IOException e) {
            log.error("I/O error occured", e);
            session = null;
        }
        return session;
    }


}