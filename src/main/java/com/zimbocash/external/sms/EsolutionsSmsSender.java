package com.zimbocash.external.sms;

import com.zimbocash.external.impl.InternalRestClient;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Component
@RequiredArgsConstructor
public class EsolutionsSmsSender {
    private final RestTemplate restTemplate;
    @Value("${esolution.sender}")
    String sender;
    @Value("${api.SnsPublisher.url}")
    private String url;

    public String sendWithEsolutions(String phone, String messageContent) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.AUTHORIZATION, createBasicAuthHeaderValue(InternalRestClient.username, InternalRestClient.password));
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        String requestUrl = url.concat("single/") + sender + "/" + phone + "/" + messageContent;
        return restTemplate.exchange(requestUrl, HttpMethod.GET, new HttpEntity<>(httpHeaders), String.class).getBody();
    }

    public String createBasicAuthHeaderValue(String username, String password) {
        return "Basic ".concat(Base64.getEncoder().encodeToString((username + ":" + password).getBytes(StandardCharsets.UTF_8)));
    }
}
