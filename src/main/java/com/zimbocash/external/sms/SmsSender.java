package com.zimbocash.external.sms;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
@Slf4j
public class SmsSender {
    private final EconetSmsSender econetSmsSender;
    private final EsolutionsSmsSender esolutionsSmsSender;
    private final NetOneSmsSender netOneSmsSender;

    public String sendWithEsolutions(String phone, String messageContent) {
        return esolutionsSmsSender.sendWithEsolutions(phone, messageContent);
    }

    public boolean sendWithEconet(String phone, String message) {
        return econetSmsSender.sendSms(phone, message);
    }

    public boolean sendWithNetOne(String phone, String message) {
        return netOneSmsSender.sendSms(message, phone);
    }
}
