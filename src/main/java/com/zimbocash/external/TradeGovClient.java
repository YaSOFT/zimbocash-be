package com.zimbocash.external;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@RequiredArgsConstructor
@Component
public class TradeGovClient {
    private final RestTemplate restTemplate;
    @Value("${api.TradeGovClient.url}")
    private String url;

    public String getList(String bearerToken) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.AUTHORIZATION, bearerToken);
        return restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(httpHeaders), String.class).getBody();
    }

}
