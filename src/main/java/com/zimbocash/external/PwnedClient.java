package com.zimbocash.external;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.websocket.server.PathParam;


@RequiredArgsConstructor
@Component
public class PwnedClient {
    private final RestTemplate restTemplate;
    @Value("${api.PwnedClient.url}")
    private String url;

    public String isPasswordPwned(@PathParam("password") String password) {
        String requestUrl = url + "range/" + password;
        return restTemplate.getForEntity(requestUrl, String.class).getBody();
    }
}
