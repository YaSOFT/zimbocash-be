package com.zimbocash.dao;

import com.zimbocash.model.entity.CustomerTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional
public interface CustomerTransactionRepository extends JpaRepository<CustomerTransaction, Long> {
    int countByReservedFalse();

    Optional<CustomerTransaction> findFirst1ByReservedFalse();
}
