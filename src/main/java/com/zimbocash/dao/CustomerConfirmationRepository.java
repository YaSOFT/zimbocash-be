package com.zimbocash.dao;

import com.zimbocash.model.entity.CustomerConfirmation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface CustomerConfirmationRepository extends JpaRepository<CustomerConfirmation, Long> {

}
