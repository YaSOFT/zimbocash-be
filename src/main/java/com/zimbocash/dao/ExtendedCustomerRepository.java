package com.zimbocash.dao;

import com.zimbocash.model.DateRange;
import com.zimbocash.model.entity.Customer;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface ExtendedCustomerRepository {

    List nativeQuery(String nativeQuery);

    List<String> getEmailsInDateRange(DateRange dateRange);

    List<Customer> findCustomersInDateRange(DateRange dateRange);

    List<Object[]> getManagerIdAndEmails();

    long countCustomer(String query);
}
