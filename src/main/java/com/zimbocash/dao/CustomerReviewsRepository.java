package com.zimbocash.dao;

import com.zimbocash.model.entity.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface CustomerReviewsRepository extends JpaRepository<Review, Long> {

}
