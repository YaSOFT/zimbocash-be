package com.zimbocash.dao;

import com.zimbocash.model.entity.CustomerReward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface CustomerRewardRepository extends JpaRepository<CustomerReward, Long> {

    long countCustomerRewardByWhoReferralYou(String whoReferralYou);
}
