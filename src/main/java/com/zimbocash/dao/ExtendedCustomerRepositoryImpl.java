package com.zimbocash.dao;

import com.zimbocash.model.DateRange;
import com.zimbocash.model.entity.Customer;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class ExtendedCustomerRepositoryImpl implements ExtendedCustomerRepository {
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List nativeQuery(String nativeQuery) {
        return entityManager.createNativeQuery(nativeQuery).getResultList();
    }


    @Override
    public List<String> getEmailsInDateRange(DateRange dateRange) {
        return entityManager.createQuery("select c.email from Customer c WHERE c.dateRecorded between :start AND :end")
                .setParameter("start", dateRange.getStartDate())
                .setParameter("end", dateRange.getEndDate())
                .getResultList();
    }

    @Override
    public List<Customer> findCustomersInDateRange(DateRange dateRange) {
        return entityManager
                .createQuery("SELECT c FROM Customer c WHERE c.dateRecorded between :start AND :end", Customer.class)
                .setParameter("start", dateRange.getStartDate())
                .setParameter("end", dateRange.getEndDate()).getResultList();
    }

    @Override
    public List<Object[]> getManagerIdAndEmails() {
        return entityManager.createQuery("select memberID,email from Customer where manager=true").getResultList();
    }

    @Override
    public long countCustomer(String query) {
        return entityManager.createQuery("select count(c) from Customer c where " + query, Long.class).getSingleResult();
    }
}
