package com.zimbocash.dao;

import com.zimbocash.model.entity.Telegram;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional
public interface TelegramRepository extends JpaRepository<Telegram, Long> {

    void deleteByChatID(Long chatId);

    Optional<Telegram> findTelegramByChatID(Long chatId);

}
