package com.zimbocash.dao;

import com.zimbocash.enums.UserStatus;
import com.zimbocash.model.entity.Customer;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long>, ExtendedCustomerRepository {

    Customer getCustomerByEmail(String email);

    @Deprecated
    List<Customer> getCustomersByNewUserFalse();

    Customer getCustomerByIdNumber(String idNumber);

    Customer getCustomerByMemberID(String memberId);

    Optional<Customer> findCustomerByMemberID(String memberId);

    Optional<Customer> findCustomerByMemberIDAndPhoneAndPhoneVerifiedIsFalse(String memberId, String phone);

    List<Customer> getCustomersByManagerIsTrue(Pageable pageable);

    @Query("FROM Customer where customerConfirmation is not null and enabled = false")
    List<Customer> listAllBlockedUsers(Pageable pageable);

    @Query("select count(c.id) from Customer c where c.enabled = false and c.customerConfirmation is not null")
    long countAllBlockedUsers();

    @Query("FROM Customer where email LIKE CONCAT('%',:searchTerm,'%') and customerConfirmation is not null and enabled = false and idFailed = true")
    List<Customer> searchBlockedUsers(@Param("searchTerm") String searchTerm, Pageable pageable);

    @Query(value = "SELECT count(c.id) FROM Customer c WHERE c.email LIKE CONCAT('%',:searchTerm,'%') and c.customerConfirmation is not null and c.idFailed = true")
    long countBlockedUsers(@Param("searchTerm") String searchTerm);

    long countCustomerByManagerIsTrue();

    @Query("update Customer set status=?1, password=?2 where email=?3 and password=?4")
    @Modifying
    int updateStatusAndPasswordByEmailAndPassword(UserStatus userStatus, String newPassword, String email, String password);

    @Query("update Customer set status=?1, password=?2 where email=?3")
    @Modifying
    int updateStatusAndPasswordByEmail(UserStatus userStatus, String newPassword, String email);

    @Query("update Customer set refreshToken=?1 where refreshToken=?2 and memberID=?3")
    @Modifying
    int updateRefreshTokenByRefreshTokenAndMemberId(String newRefreshToken, String refreshToken, String memberId);

    @Query("update Customer set refreshToken=?1 where memberID=?2")
    @Modifying
    int updateRefreshTokenByMemberId(String refreshToken, String memberId);

    @Query("update Customer set enabled=?1 where email=?2")
    @Modifying
    int updateEnabledByEmail(boolean enable, String email);

    @Query(value = "FROM Customer WHERE email LIKE CONCAT('%',:searchTerm,'%') OR memberID LIKE CONCAT('%',:searchTerm,'%') OR phone LIKE CONCAT('%',:searchTerm,'%') OR idNumber LIKE CONCAT('%',:searchTerm,'%') OR firstName LIKE CONCAT('%',:searchTerm,'%') OR lastName LIKE CONCAT('%',:searchTerm,'%')")
    List<Customer> listAllCustomers(@Param("searchTerm") String searchTerm, Pageable pageable);

    @Query(value = "SELECT count(c.id) FROM Customer c WHERE c.email LIKE CONCAT('%',:searchTerm,'%') OR c.memberID LIKE CONCAT('%',:searchTerm,'%') OR c.phone LIKE CONCAT('%',:searchTerm,'%') OR c.idNumber LIKE CONCAT('%',:searchTerm,'%') OR c.firstName LIKE CONCAT('%',:searchTerm,'%') OR c.lastName LIKE CONCAT('%',:searchTerm,'%')")
    long countAllCustomers(@Param("searchTerm") String searchTerm);
}

