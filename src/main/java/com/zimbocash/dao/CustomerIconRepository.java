package com.zimbocash.dao;

import com.zimbocash.model.entity.CustomerIcon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Deprecated
public interface CustomerIconRepository extends JpaRepository<CustomerIcon, String> {
}
