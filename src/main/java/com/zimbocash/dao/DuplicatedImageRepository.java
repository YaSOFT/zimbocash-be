package com.zimbocash.dao;

import com.zimbocash.model.entity.DuplicatedImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface DuplicatedImageRepository extends JpaRepository<DuplicatedImage, Long> {
}
