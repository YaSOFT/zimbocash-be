package com.zimbocash.dao;

import com.zimbocash.model.entity.CustomerIP;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface CustomerIPRepository extends JpaRepository<CustomerIP, Long> {
    long countByIp(String ip);

}
