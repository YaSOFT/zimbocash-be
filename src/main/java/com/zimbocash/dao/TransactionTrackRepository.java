package com.zimbocash.dao;

import com.zimbocash.model.entity.TransactionTrack;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface TransactionTrackRepository extends JpaRepository<TransactionTrack, Long> {
}
