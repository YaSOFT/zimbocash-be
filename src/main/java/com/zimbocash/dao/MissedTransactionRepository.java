package com.zimbocash.dao;

import com.zimbocash.model.entity.MissedTransaction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface MissedTransactionRepository extends JpaRepository<MissedTransaction, Long> {

    @Query("from MissedTransaction where status = 'UNPAID' and senderId is not null and receiverId is not null and amount > 0 and attempts < 2")
    List<MissedTransaction> list(Pageable pageable);
}
