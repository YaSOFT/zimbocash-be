package com.zimbocash.dao;

import com.zimbocash.enums.TargetType;
import com.zimbocash.model.entity.OTP;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface CustomerOtpRepository extends JpaRepository<OTP, Long> {
    @Modifying
    @Query("delete from OTP c where c.otp=?1 and c.customer.id=?2 and c.id<>?3 and c.target=?4")
    void deleteMany(int otp, long id, long id1, TargetType phone);

    List<OTP> findOTPSByOtpAndTarget(int otp, TargetType targetType);
}
