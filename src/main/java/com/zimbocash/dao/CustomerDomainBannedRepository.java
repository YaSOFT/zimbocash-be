package com.zimbocash.dao;

import com.zimbocash.model.entity.DomainBanned;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional
public interface CustomerDomainBannedRepository extends JpaRepository<DomainBanned, Long> {

    Optional<DomainBanned> getByPattern(String domain);
}
