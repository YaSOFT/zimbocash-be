package com.zimbocash.dao;

import com.zimbocash.model.dtos.TempObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;


@Deprecated
@Repository
public class JdbcRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<TempObject> findTempObjectByEmail(String emails) {
        return jdbcTemplate.query("select zc.email email, zc.tron_public_key tpk, zc.tron_account_address traa, zc.member_id_old mio from zc_maindb.zc_customers as zc where zc.email in (" + emails + ")",
                (resultSet, i) -> TempObject.builder().email(resultSet.getString("email"))
                        .privateKey(resultSet.getString("mio")).oldAddress(resultSet.getString("traa"))
                        .oldPublicKey(resultSet.getString("tpk")).build());
    }
}
