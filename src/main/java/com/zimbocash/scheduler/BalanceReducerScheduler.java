package com.zimbocash.scheduler;

import com.zimbocash.dao.CustomerRepository;
import com.zimbocash.facade.impl.SocketFacade;
import com.zimbocash.model.dtos.SocketBodyDto;
import com.zimbocash.model.entity.Customer;
import com.zimbocash.model.entity.CustomerTransaction;
import com.zimbocash.util.Constants;
import com.zimbocash.util.TronServiceUtil;
import lombok.SneakyThrows;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BalanceReducerScheduler implements Job {
    public static volatile String hotWalletAddress;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private TronServiceUtil tronServiceUtil;
    @Autowired
    private Scheduler scheduler;
    @Value("${tron.tokenID}")
    private String assetName;
    @Autowired
    private SocketFacade socketFacade;

    @SneakyThrows
    @Override
    public void execute(JobExecutionContext context) {
        JobKey jobKey = context.getJobDetail().getKey();
        boolean successReduce = false;
        JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        if (dataMap.containsKey("memberID") && dataMap.containsKey("amount")) {
            String memberID = dataMap.getString("memberID");
            double amount = Double.parseDouble(dataMap.getString("amount")); // the amount to be cut
            Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
            if (customer != null && customer.isIdFailed() && customer.isEnabled()) {
                CustomerTransaction customerTransaction = customer.getCustomerTransaction();
                if (customerTransaction != null) {
                    double realBalance = tronServiceUtil.getBalance(customerTransaction.getTronAccountAddress(), assetName);
                    if (realBalance > 0) {
                        successReduce = true;
                        double amountToBeReduced = Math.min(amount, realBalance) / Constants.DECIMAL_RATIO;
                        String txID = tronServiceUtil.issueOrTransfer(customerTransaction.getTronPublicKey(), hotWalletAddress, amountToBeReduced, assetName);
                        if (txID != null) {
                            double newBalance = (realBalance / Constants.DECIMAL_RATIO) - amountToBeReduced;
                            customer.setBalance(Math.max(newBalance, 0));
                            customerRepository.save(customer);
                            socketFacade.send(SocketBodyDto.builder().api("BalanceReducerScheduler").memberIDOrEmail(memberID)
                                    .message("amount Reduced " + amountToBeReduced).build());
                        } else {
                            socketFacade.send(SocketBodyDto.builder().api("BalanceReducerScheduler").memberIDOrEmail(memberID)
                                    .message("amount Reduced " + amountToBeReduced + " missing transaction").build());
                            tronServiceUtil.saveMissedTransaction(customerTransaction.getTronPublicKey(),
                                    hotWalletAddress, amountToBeReduced, null);
                        }
                    }
                }
            }
        } else {
            socketFacade.send(SocketBodyDto.builder().api("BalanceReducerScheduler").memberIDOrEmail("BalanceReducerScheduler")
                    .message("dataMap not valid" + dataMap.toString()).build());
        }

        //stop the scheduler and remove jobs if doesn't match cases, i.e balance reach 0
        if (!successReduce) {
            scheduler.deleteJob(jobKey);
        }
    }
}

