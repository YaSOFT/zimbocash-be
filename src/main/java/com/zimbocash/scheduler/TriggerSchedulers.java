package com.zimbocash.scheduler;

import com.zimbocash.facade.ISocketFacade;
import com.zimbocash.model.dtos.SocketBodyDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.JobKey.jobKey;
import static org.quartz.TriggerBuilder.newTrigger;

@Component
@RequiredArgsConstructor
@Slf4j
public class TriggerSchedulers {
    private final Scheduler scheduler;
    private final ISocketFacade socketFacade;

    public void reminder(String memberID, String email, String firstName, String lastName, String phone) throws Exception {
        if (scheduler != null) {
            scheduler.start();
            if (scheduler.isStarted()) {
                if (!scheduler.checkExists(jobKey("reminder_job_" + memberID, "reminder_group_" + memberID))) {
                    LocalDateTime date = LocalDateTime.now();
                    JobDetail job = newJob(EmailSMSReminderScheduler.class)
                            .usingJobData("memberID", memberID)
                            .usingJobData("name", firstName + " " + lastName)
                            .usingJobData("email", email)
                            .usingJobData("phone", Optional.ofNullable(phone).orElse("none"))
                            .withIdentity("reminder_job_" + memberID, "reminder_group_" + memberID)
                            .build();
                    int dayOfWeek = date.getDayOfWeek().getValue() + 1;
                    dayOfWeek = dayOfWeek > 7 ? dayOfWeek - 7 : dayOfWeek;
                    Trigger trigger = newTrigger()
                            .withIdentity("reminder_trigger_" + memberID, "reminder_group_" + memberID)
                            //start scheduler after two hours
                            .startAt(Date.from(date.plusHours(2).atZone(ZoneId.systemDefault()).toInstant()))
                            //ends after one week
                            .endAt(Date.from(date.plusWeeks(1).plusMinutes(10).atZone(ZoneId.systemDefault()).toInstant()))
                            .withSchedule(CronScheduleBuilder.atHourAndMinuteOnGivenDaysOfWeek(date.getHour(), date.getMinute(),
                                    dayOfWeek /*one week*/, (dayOfWeek + 1 > 7 ? dayOfWeek - 6 : dayOfWeek + 1),
                                    (dayOfWeek + 2 > 7 ? dayOfWeek - 5 : dayOfWeek + 2))
                                    .withMisfireHandlingInstructionDoNothing())
                            .forJob(job.getKey())
                            .build();
                    scheduler.scheduleJob(job, trigger);
                }
            }
        }
    }

    public void takeFeesAndEmail(String memberID, double realBalance, String email) {
        String partLog = "internal takeFeesAndEmail method";
        try {
            if (scheduler != null) {
                scheduler.start();
                if (scheduler.isStarted()) {
                    LocalDateTime date = LocalDateTime.now();
                    if (!scheduler.checkExists(jobKey("job_" + memberID, "email_" + memberID))) {
                        JobDetail job = newJob(EmailReducerScheduler.class)
                                .usingJobData("email", email)
                                .usingJobData("memberID", memberID)
                                .usingJobData("amount", String.valueOf(realBalance > 0 ? (realBalance / 40) : 0))
                                .withIdentity("job_" + memberID, "email_" + memberID)
                                .build();
                        Trigger trigger = newTrigger()
                                .withIdentity("trigger_" + memberID, "email_" + memberID)
                                .startNow()
                                //ends after five months + firing now
                                .endAt(Date.from(date.plusMonths(5).plusMinutes(10).atZone(ZoneId.systemDefault()).toInstant()))
                                .withSchedule(CronScheduleBuilder.monthlyOnDayAndHourAndMinute(Math.min(date.getDayOfMonth(), 28),
                                        date.getHour(), date.getMinute() > 58 ? date.getMinute() : (date.getMinute() + 1))
                                        .withMisfireHandlingInstructionDoNothing())
                                .forJob(job.getKey())
                                .build();
                        scheduler.scheduleJob(job, trigger);
                    }

                    if (!scheduler.checkExists(jobKey("job_" + memberID, "reduce_" + memberID))) {
                        JobDetail job = newJob(BalanceReducerScheduler.class)
                                .usingJobData("memberID", memberID)
                                .usingJobData("amount", String.valueOf(realBalance > 0 ? (realBalance / 40) : 0))
                                .withIdentity("job_" + memberID, "reduce_" + memberID)
                                .build();
                        Trigger trigger = newTrigger()
                                .withIdentity("trigger_" + memberID, "reduce_" + memberID)
                                .startNow()
                                .withSchedule(CronScheduleBuilder.monthlyOnDayAndHourAndMinute(Math.min(date.getDayOfMonth(), 28),
                                        date.getHour(), date.getMinute() > 58 ? date.getMinute() : (date.getMinute() + 1))
                                        .withMisfireHandlingInstructionDoNothing())
                                .forJob(job.getKey())
                                .build();
                        scheduler.scheduleJob(job, trigger);
                    }
                } else {
                    socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("scheduler cannot be started").build());
                }
            } else {
                socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message("scheduler is null").build());
            }
        } catch (SchedulerException e) {
            socketFacade.send(SocketBodyDto.builder().api(partLog).memberIDOrEmail(memberID).message(e.getMessage()).build());
            log.error(e.getMessage(), e);
        }
    }


}
