package com.zimbocash.scheduler;

import com.zimbocash.dao.CustomerRepository;
import com.zimbocash.dao.MissedTransactionRepository;
import com.zimbocash.enums.MissedTransactionStatus;
import com.zimbocash.enums.MissedTransactionType;
import com.zimbocash.external.AmazonSES;
import com.zimbocash.facade.ISocketFacade;
import com.zimbocash.model.dtos.SocketBodyDto;
import com.zimbocash.model.entity.Customer;
import com.zimbocash.model.entity.MissedTransaction;
import com.zimbocash.util.TronServiceUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Component
@RequiredArgsConstructor
@Slf4j
@DependsOn(value = {"constants", "socketFacade"})
public class MissedTransactionScheduler {
    private final MissedTransactionRepository missedTransactionRepository;
    private final TronServiceUtil tronServiceUtil;
    private final AmazonSES amazonSES;
    private final CustomerRepository customerRepository;
    private final ISocketFacade socketFacade;

    @Value("${tron.tokenID}")
    String assetName;

    @PostConstruct
    public void init() {
        cronJob();
    }

    //would run at 12:00 PM, every 3 days starting on the 1st, every month
    @Scheduled(cron = "0 0 12 */3 * ?")
    void cronJob() {
        log.info("cronJob() method");
        checkTransaction();
    }

    private void checkTransaction() {
        CompletableFuture.runAsync(() -> {
            List<MissedTransaction> missedTransactions = missedTransactionRepository.list(PageRequest.of(0, 20));
            while (missedTransactions.size() == 20) {
                makeMissedTransaction(missedTransactions);
                missedTransactions = missedTransactionRepository.list(PageRequest.of(0, 20));
            }
            if (missedTransactions.size() < 20 && missedTransactions.size() > 0) {
                makeMissedTransaction(missedTransactions);
            }
        }).join();
    }

    //@CacheResult(cacheName = "getSender")
    public String getSender(String senderId) {
        if (senderId.equals(MissedTransactionType.HOT_WALLET.getStatus())) {
            return null;
        } else {
            return senderId;
        }
    }

    private void makeMissedTransaction(List<MissedTransaction> missedTransactions) {
        try {
            missedTransactions.forEach(missedTransaction -> {
                String senderPublicKey = getSender(missedTransaction.getSenderId());
                String memberIDReceiver = missedTransaction.getMemberIDReceiver();
                String receiverAddress = missedTransaction.getReceiverId();
                //user will receive something
                if (memberIDReceiver != null && !memberIDReceiver.isEmpty()) {
                    Customer customer = customerRepository.findCustomerByMemberID(memberIDReceiver).orElse(null);
                    if (customer != null && customer.isEnabled() && !customer.isManager() && !customer.isAdmin()) {
                        String txID = tronServiceUtil.issueOrTransfer(senderPublicKey, receiverAddress, missedTransaction.getAmount(), assetName);
                        if (txID != null) {
                            missedTransaction.setStatus(MissedTransactionStatus.PAID);
                            missedTransactionRepository.save(missedTransaction);
                            amazonSES.sendMissedPaymentSuccess(customer.getEmail(), customer.getFirstName() + " " + customer.getLastName(), missedTransaction.getAmount());
                        } else {
                            missedTransaction.setAttempts(missedTransaction.getAttempts() + 1);
                            missedTransactionRepository.save(missedTransaction);
                            socketFacade.send(SocketBodyDto.builder().api("internal makeMissedTransaction method").memberIDOrEmail("makeMissedTransaction")
                                    .message("txID is null receiverAddress " + receiverAddress).build());
                        }
                    }
                    //hotwallet will receive something
                } else {
                    String txID = tronServiceUtil.issueOrTransfer(senderPublicKey, receiverAddress, missedTransaction.getAmount(), assetName);
                    if (txID != null) {
                        missedTransaction.setStatus(MissedTransactionStatus.PAID);
                        missedTransactionRepository.save(missedTransaction);
                    } else {
                        missedTransaction.setAttempts(missedTransaction.getAttempts() + 1);
                        missedTransactionRepository.save(missedTransaction);
                        socketFacade.send(SocketBodyDto.builder().api("internal makeMissedTransaction method").memberIDOrEmail("makeMissedTransaction")
                                .message("txID is null hot wallet ").build());
                    }
                }
            });
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api("internal makeMissedTransaction method").memberIDOrEmail("makeMissedTransaction")
                    .message(e.getMessage()).build());
            log.error(e.getMessage());
        }
    }

}
