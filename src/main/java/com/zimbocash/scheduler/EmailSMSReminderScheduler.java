package com.zimbocash.scheduler;

import com.zimbocash.dao.CustomerRepository;
import com.zimbocash.enums.TestType;
import com.zimbocash.external.AmazonSES;
import com.zimbocash.external.impl.InternalRestClient;
import com.zimbocash.facade.impl.SocketFacade;
import com.zimbocash.model.dtos.SocketBodyDto;
import com.zimbocash.model.entity.CustomerConfirmation;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Set;

@Component
@Slf4j
public class EmailSMSReminderScheduler implements Job {
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private AmazonSES amazonSES;
    @Autowired
    private SocketFacade socketFacade;
    @Autowired
    private Scheduler scheduler;
    @Autowired
    private InternalRestClient internalRestClient;

    @SneakyThrows
    @Override
    public void execute(JobExecutionContext context) {
        JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        if (dataMap.containsKey("memberID") && dataMap.containsKey("name") && dataMap.containsKey("email") && dataMap.containsKey("phone")) {
            String email = dataMap.getString("email");
            String memberID = dataMap.getString("memberID");
            String name = dataMap.getString("name");
            String phone = dataMap.getString("phone");
            final boolean[] stopScheduler = {false};
            customerRepository.findCustomerByMemberID(memberID).ifPresent(customer -> {
                if (customer.isIdFailed() || !customer.isEnabled()) {
                    stopScheduler[0] = true;
                } else {
                    CustomerConfirmation customerConfirmation = customer.getCustomerConfirmation();
                    if (customerConfirmation != null) {
                        Set<TestType> testFailsMachine = customerConfirmation.getTestFailsMachine();
                        if (testFailsMachine != null && testFailsMachine.contains(TestType.TEST5)) {
                            stopScheduler[0] = true;
                        }
                    }
                }
            });
            if (stopScheduler[0]) {
                JobKey jobKey = context.getJobDetail().getKey();
                //stop scheduler for this user
                scheduler.deleteJob(jobKey);
                socketFacade.send(SocketBodyDto.builder().api("EmailReminderScheduler").memberIDOrEmail(memberID)
                        .message("scheduler stopped for user " + memberID).build());
            } else {
                //email|sms him
                boolean mayFireAgain = context.getTrigger().mayFireAgain();
                Date nextFireTime = context.getTrigger().getNextFireTime();
                if (mayFireAgain) { //i.e we are in 1 or second step
                    Date actualDate = new Date();
                    if ((actualDate.getDay() + 1) == nextFireTime.getDay()) {
                        //i.e we are in first
                        amazonSES.sendFirstReminderEmail(email, name);
                        socketFacade.send(SocketBodyDto.builder().api("EmailReminderScheduler").memberIDOrEmail(memberID)
                                .message("first reminder sent").build());
                    } else if (!phone.equals("none")) {
                        //i.e we are in second, send sms
                        String message = "Hi " + name + ". You started the ZIMBOCASH ID verification process but didn’t finish. \n Log on to www.zimbo.cash/login to upload your ID and Selfie You’re almost there. \nWarm regards. \nThe ZIMBOCASH Team.";
                        internalRestClient.publishSms(message, phone);
                        socketFacade.send(SocketBodyDto.builder().api("EmailReminderScheduler").memberIDOrEmail(memberID)
                                .message("second reminder sent").build());
                    } else {
                        amazonSES.sendSecondReminderEmail(email, name);
                        socketFacade.send(SocketBodyDto.builder().api("EmailReminderScheduler").memberIDOrEmail(memberID)
                                .message("second reminder cannot be sent as user doesn't have phone, as solution sending second reminder").build());
                    }
                } else {
                    amazonSES.sendSecondReminderEmail(email, name);
                    //send the 3 one
                    socketFacade.send(SocketBodyDto.builder().api("EmailReminderScheduler").memberIDOrEmail(memberID)
                            .message("third and last reminder sent").build());
                }
            }
        } else {
            socketFacade.send(SocketBodyDto.builder().api("EmailReminderScheduler").memberIDOrEmail("EmailReminderScheduler")
                    .message("dataMap not valid" + dataMap.toString()).build());
        }
    }
}

