package com.zimbocash.scheduler;

import com.zimbocash.dao.CustomerRepository;
import com.zimbocash.external.AmazonSES;
import com.zimbocash.facade.impl.SocketFacade;
import com.zimbocash.model.dtos.SocketBodyDto;
import com.zimbocash.model.entity.Customer;
import com.zimbocash.model.entity.CustomerTransaction;
import com.zimbocash.util.Constants;
import com.zimbocash.util.TronServiceUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EmailReducerScheduler implements Job {
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private TronServiceUtil tronServiceUtil;
    @Value("${tron.tokenID}")
    private String assetName;
    @Autowired
    private AmazonSES amazonSES;
    @Autowired
    private SocketFacade socketFacade;

    @SneakyThrows
    @Override
    public void execute(JobExecutionContext context) {
        JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        if (dataMap.containsKey("memberID") && dataMap.containsKey("amount") && dataMap.containsKey("email")) {
            String email = dataMap.getString("email");
            String memberID = dataMap.getString("memberID");
            double amount = Double.parseDouble(dataMap.getString("amount"));
            Customer customer = customerRepository.findCustomerByMemberID(memberID).orElse(null);
            if (customer != null && customer.isIdFailed() && customer.isEnabled()) {
                CustomerTransaction customerTransaction = customer.getCustomerTransaction();
                if (customerTransaction != null) {
                    double realBalance = tronServiceUtil.getBalance(customerTransaction.getTronAccountAddress(), assetName);
                    if (realBalance > 0) {
                        log.info("sending email to {}, we are reducing this amount {}", email, amount / Constants.DECIMAL_RATIO);
                        amazonSES.sendReducingEmail(customer.getEmail(), customer.getFirstName());
                        socketFacade.send(SocketBodyDto.builder().api("EmailReducerScheduler").memberIDOrEmail(memberID)
                                .message("sending email reducing").build());
                    }
                }
            }
        } else {
            socketFacade.send(SocketBodyDto.builder().api("EmailReducerScheduler").memberIDOrEmail("EmailReducerScheduler")
                    .message("dataMap not valid" + dataMap.toString()).build());
        }
    }
}

