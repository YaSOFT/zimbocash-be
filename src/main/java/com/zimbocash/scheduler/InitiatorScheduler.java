package com.zimbocash.scheduler;

import com.zimbocash.dao.CustomerTransactionRepository;
import com.zimbocash.external.TradeGovClient;
import com.zimbocash.external.WalletHttp;
import com.zimbocash.facade.ISocketFacade;
import com.zimbocash.model.dtos.SocketBodyDto;
import com.zimbocash.service.AdminService;
import com.zimbocash.service.KYCService;
import com.zimbocash.service.TransactionService;
import com.zimbocash.util.TronServiceUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@Slf4j
@Component
@RequiredArgsConstructor
public class InitiatorScheduler {
    //todo remove at later stage
    public static volatile int counterRemainingReserved = 0;
    private final TradeGovClient tradeGovClient;
    private final WalletHttp walletHttp;
    private final CustomerTransactionRepository customerTransactionRepository;
    private final ISocketFacade socketFacade;
    @Value("${trade-gov.token}")
    String token;
    @Value("${trade-gov.file-name}")
    String fileName;

    @PostConstruct
    public void init() {
        readAndCreateNamesFile();
        initWallets();
        //todo remove at later stage
        counterRemainingReserved = customerTransactionRepository.countByReservedFalse();
    }

    public void initWallets() {
        String responseInit = walletHttp.initWallets();
        if (responseInit != null) {
            String[] wallets = responseInit.split("\n");
            if (wallets.length == 2) {
                AdminService.hotWalletAddress = BalanceReducerScheduler.hotWalletAddress = KYCService.hotWalletAddress =
                        TronServiceUtil.hotWalletAddress = wallets[0];
                TransactionService.tronWalletAddress = TronServiceUtil.tronWalletAddress = wallets[1];
                log.info("TronServiceUtil.hotWalletAddress\t" + TronServiceUtil.hotWalletAddress);
                log.info("TronServiceUtil.tronWalletAddress\t" + TronServiceUtil.tronWalletAddress);
            } else {
                socketFacade.send(SocketBodyDto.builder().api("InitiatorScheduler").memberIDOrEmail("initWallets")
                        .message("wallets length not 2").build());
            }
        }
    }

    //would run 19:30 every Thursday
    @Scheduled(cron = "0 30 19 ? * THU")
    void cronJob() {
        log.info("cronJob() method");
        readAndCreateNamesFile();
    }

    private void readAndCreateNamesFile() {
        log.info("readAndCreateNamesFile() method");
        StringBuilder names = new StringBuilder();
        try {
            String jsonList = tradeGovClient.getList(createBearerToken(token));
            JSONObject jsonObject = new JSONObject(jsonList);

            if (jsonObject.has("results")) {
                JSONArray obj = jsonObject.getJSONArray("results");
                for (int i = 0; i < obj.length(); i++) {
                    if (obj.getJSONObject(i).has("name")) {
                        String name = obj.getJSONObject(i).getString("name");
                        if (name != null && !name.isEmpty()) {
                            names.append(name.trim().replace(",", "").toLowerCase()).append("\n");
                        }
                    }

                    if (obj.getJSONObject(i).has("alt_names")) {
                        JSONArray alt_names = (JSONArray) obj.getJSONObject(i).get("alt_names");
                        if (alt_names != null && alt_names.length() != 0) {
                            for (int j = 0; j < alt_names.length(); j++) {
                                String alt_name = String.valueOf(alt_names.get(j));
                                names.append(alt_name.trim().replace(",", "").toLowerCase()).append("\n");
                            }
                        }
                    }
                }
            }

            Files.write(Paths.get("/tmp/" + fileName),
                    names.toString().getBytes(StandardCharsets.UTF_8),
                    StandardOpenOption.CREATE,
                    StandardOpenOption.APPEND);
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api("internal readAndCreateNamesFile method").memberIDOrEmail("readAndCreateNamesFile")
                    .message(e.getMessage()).build());
            log.error(e.getMessage());
        }
    }

    public String createBearerToken(String token) {
        return "Bearer ".concat(token);
    }
}
