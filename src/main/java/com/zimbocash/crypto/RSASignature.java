package com.zimbocash.crypto;

import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zimbocash.enums.ServerType;
import com.zimbocash.facade.ISocketFacade;
import com.zimbocash.model.dtos.SocketBodyDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.StringReader;
import java.security.Key;
import java.security.KeyFactory;
import java.security.Security;
import java.security.Signature;
import java.security.spec.RSAPublicKeySpec;

@Component
@Slf4j
@RequiredArgsConstructor
public class RSASignature {
    private static final String ALGORITHM = "RSA";
    public static volatile Key jwtPrivateKey;
    public static volatile Key jwtPublicKey;
    private static volatile Signature signatureRSA;
    private static volatile Signature signatureRed;
    private static volatile Signature signatureGreen;
    private final ObjectMapper objectMapper;
    private final ISocketFacade socketFacade;

    @Value("${aws.secret-name}")
    String secretName;
    @Value("${aws.region}")
    String region;
    @Value("${jsonNode.privateKeyBlue}")
    private String privateKeyBlue;
    @Value("${jsonNode.publicKeyRed}")
    private String publicKeyRed;
    @Value("${jsonNode.publicKeyGreen}")
    private String publicKeyGreen;
    @Value("${jsonNode.privateJwt}")
    private String privateJwt;
    @Value("${jsonNode.publicJwt}")
    private String publicJwt;
    @Value("${jsonNode.bcProvider}")
    private String bcProvider;

    public static boolean verifySignatureFromServers(byte[] message, byte[] signature, ServerType serverType) throws Exception {
        if (serverType.name().equalsIgnoreCase(ServerType.RED.name())) {
            signatureRed.update(message);
            return signatureRed.verify(signature);
        } else {
            signatureGreen.update(message);
            return signatureGreen.verify(signature);
        }
    }

    public static byte[] signRSA(byte[] message) throws Exception {
        signatureRSA.update(message);
        return signatureRSA.sign();
    }

    public String getSecret() {
        AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard().withRegion(region).build();
        String secret;
        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest().withSecretId(secretName);
        GetSecretValueResult getSecretValueResult;
        try {
            getSecretValueResult = client.getSecretValue(getSecretValueRequest);
        } catch (Exception e) {
            socketFacade.send(SocketBodyDto.builder().api("internal getSecret method").memberIDOrEmail("RSASIGNATURE")
                    .message(e.getMessage()).build());
            log.error(e.getMessage());
            return null;
        }
        secret = getSecretValueResult.getSecretString();
        if (secret == null) {
            secret = new String(java.util.Base64.getDecoder().decode(getSecretValueResult.getSecretBinary()).array());
        }

        return secret;
    }

    @PostConstruct
    public void init() throws Exception {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        readSecretManagerAndFill();
    }

    private void readSecretManagerAndFill() throws Exception {
        String secret = getSecret();
        if (secret != null && !secret.isEmpty()) {
            objectMapper.configure(JsonReadFeature.ALLOW_UNESCAPED_CONTROL_CHARS.mappedFeature(), true);
            JsonNode jsonNode = objectMapper.readTree(secret);
            if (jsonNode.has(privateKeyBlue)) {
                PEMKeyPair pemKeyPair = (PEMKeyPair) new PEMParser(new StringReader(jsonNode.get(privateKeyBlue).asText())).readObject();
                JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider(bcProvider);
                signatureRSA = Signature.getInstance(ALGORITHM);
                signatureRSA.initSign(converter.getPrivateKey(pemKeyPair.getPrivateKeyInfo()));
            }

            if (jsonNode.has(publicKeyRed)) {
                SubjectPublicKeyInfo subjectPublicKeyInfo =
                        (SubjectPublicKeyInfo) new PEMParser(new StringReader(jsonNode.get(publicKeyRed).asText())).readObject();
                RSAKeyParameters rsa = (RSAKeyParameters) PublicKeyFactory.createKey(subjectPublicKeyInfo);
                KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
                signatureRed = Signature.getInstance(ALGORITHM);
                signatureRed.initVerify(keyFactory.generatePublic(new RSAPublicKeySpec(rsa.getModulus(), rsa.getExponent())));
            }

            if (jsonNode.has(publicKeyGreen)) {
                SubjectPublicKeyInfo subjectPublicKeyInfo =
                        (SubjectPublicKeyInfo) new PEMParser(new StringReader(jsonNode.get(publicKeyGreen).asText())).readObject();
                RSAKeyParameters rsa = (RSAKeyParameters) PublicKeyFactory.createKey(subjectPublicKeyInfo);
                KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
                signatureGreen = Signature.getInstance(ALGORITHM);
                signatureGreen.initVerify(keyFactory.generatePublic(new RSAPublicKeySpec(rsa.getModulus(), rsa.getExponent())));
            }

            if (jsonNode.has(privateJwt)) {
                PEMKeyPair pemKeyPair = (PEMKeyPair) new PEMParser(new StringReader(jsonNode.get(privateJwt).asText())).readObject();
                JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider(bcProvider);
                jwtPrivateKey = converter.getPrivateKey(pemKeyPair.getPrivateKeyInfo());
            }

            if (jsonNode.has(publicJwt)) {
                SubjectPublicKeyInfo subjectPublicKeyInfo =
                        (SubjectPublicKeyInfo) new PEMParser(new StringReader(jsonNode.get(publicJwt).asText())).readObject();
                RSAKeyParameters rsa = (RSAKeyParameters) PublicKeyFactory.createKey(subjectPublicKeyInfo);
                KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
                jwtPublicKey = keyFactory.generatePublic(new RSAPublicKeySpec(rsa.getModulus(), rsa.getExponent()));
            }
        } else {
            socketFacade.send(SocketBodyDto.builder().api("internal readSecretManagerAndFill method").memberIDOrEmail("readSecretManagerAndFill")
                    .message("secret empty or null").build());
        }
    }
}
