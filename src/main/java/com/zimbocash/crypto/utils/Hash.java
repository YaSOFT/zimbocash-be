package com.zimbocash.crypto.utils;

import com.zimbocash.crypto.Parameter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;

import static java.util.Arrays.copyOfRange;

@Slf4j(topic = "crypto")
@Component
public class Hash {
    public static byte addressPreFixByte = Parameter.CommonConstant.ADD_PRE_FIX_BYTE_MAINNET;
    private Provider CRYPTO_PROVIDER;
    private String HASH_256_ALGORITHM_NAME;
    private String ALGORITHM_NOT_FOUND = "Can't find such algorithm";

    @PostConstruct
    public void init() {
        Provider INSTANCE = new org.spongycastle.jce.provider.BouncyCastleProvider();
        Security.addProvider(INSTANCE);
        CRYPTO_PROVIDER = Security.getProvider("SC");
        HASH_256_ALGORITHM_NAME = "KECCAK-256";
    }

    public byte[] sha3(byte[] input) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance(HASH_256_ALGORITHM_NAME, CRYPTO_PROVIDER);
            digest.update(input);
            return digest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            log.error(ALGORITHM_NOT_FOUND, e);
            throw new RuntimeException(e);
        }

    }


    public byte[] hash(byte[] input, int offset, int length) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.update(input, offset, length);
        return digest.digest();
    }

    public byte[] computeAddress(byte[] pubBytes) {
        return sha3omit12(copyOfRange(pubBytes, 1, pubBytes.length));
    }

    public String encodeTo58(byte[] computedInBytes) throws NoSuchAlgorithmException {
        byte[] hashBytes0 = hash(computedInBytes, 0, computedInBytes.length);
        byte[] hashBytes1 = hash(hashBytes0, 0, hashBytes0.length);
        byte[] computedInBytesCheck = new byte[computedInBytes.length + 4];
        System.arraycopy(computedInBytes, 0, computedInBytesCheck, 0, computedInBytes.length);
        System.arraycopy(hashBytes1, 0, computedInBytesCheck, computedInBytes.length, 4);
        return Base58.encode(computedInBytesCheck);
    }


    /**
     * Calculates RIGTMOST160(SHA3(input)). This is used in address calculations. *
     *
     * @param input - data
     * @return - add_pre_fix + 20 right bytes of the hash keccak of the data
     */
    public byte[] sha3omit12(byte[] input) {
        byte[] hash = sha3(input);
        byte[] address = copyOfRange(hash, 11, hash.length);
        address[0] = addressPreFixByte;
        return address;
    }
}
