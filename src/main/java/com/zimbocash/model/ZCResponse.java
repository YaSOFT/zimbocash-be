package com.zimbocash.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ZCResponse extends CustomObjectResponse {
    private String status;
    @Builder.Default
    private int statusCode = 400;
    private Object object;
    @Builder.Default
    private boolean success = false;
}
