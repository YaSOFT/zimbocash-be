package com.zimbocash.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManualReviewerDecision {
    Integer failedCount;
    Integer notSureCount;
    Integer approvedCount;

    public void addToFailed() {
        failedCount++;
    }

    public void addToNotSure() {
        notSureCount++;
    }

    public void addToApproved() {
        approvedCount++;
    }
}
