package com.zimbocash.model.entity;

import com.zimbocash.enums.AdminAction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The comment for customer when they disabled or enabled by admin
 */
@Entity
@Table(name = "zc_admin_comment")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Cacheable
public class AdminComment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private long id;

    /**
     * The type of action ENABLED or DISABLED
     */
    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private AdminAction adminAction;

    /**
     * Admin memberID
     */
    @Column(name = "add_by_who", length = 20)
    private String addByWho;

    /**
     * Admin comment
     */
    @Column(name = "comment", length = 55)
    private String comment;

}
