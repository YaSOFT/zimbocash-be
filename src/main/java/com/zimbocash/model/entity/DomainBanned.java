package com.zimbocash.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "zc_banned_emails")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Cacheable
public class DomainBanned {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private long id;

    @Column(length = 80, unique = true)
    private String pattern;

    @Column(name = "is_word")
    @Builder.Default
    private boolean word = false;
}
