package com.zimbocash.model.entity;

import com.zimbocash.enums.MissedTransactionStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "missed_transaction")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Cacheable
public class MissedTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private long id;

    @Column(name = "sender_id", columnDefinition = "text")
    private String senderId;

    @Column(name = "receiver_id")
    private String receiverId;

    @Column(name = "amount")
    private double amount;

    @Column(name = "member_id_receiver", columnDefinition = "varchar(80)")
    private String memberIDReceiver;

    @Enumerated(EnumType.STRING)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @Column(length = 20)
    @Builder.Default
    private MissedTransactionStatus status = MissedTransactionStatus.UNPAID;

    @Column(name = "date_recorded")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateRecorded;

    @Column(name = "attempts")
    @Builder.Default
    private int attempts = 0;
}
