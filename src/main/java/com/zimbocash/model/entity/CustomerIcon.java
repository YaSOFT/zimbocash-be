package com.zimbocash.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
// todo remove
@Table(name = "zc_customers_icon")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Deprecated
public class CustomerIcon {
    @Id
    private String id;

    @Column
    @Lob
    private byte[] icon;

    @Column
    private int bytes;
}
