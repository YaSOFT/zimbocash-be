package com.zimbocash.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "zc_customers_rewards")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Cacheable
public class CustomerReward {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private long id;

    //update when signup, then issue and make it to 400 (depends on time of signup)
    @Column(name = "sign_up_reward")
    @Builder.Default
    private double signUpReward = 0;

    //update when signup and issue when telegram verified and id verified and make it to 100
    @Column(name = "telegram_reward")
    @Builder.Default
    private double telegramReward = 0;

    //user b sign up , who_referral_you updates with user a.
    // user b verify his id:
    // if user a verified id get referral_reward = (depends on time when user b signup (i.e date recorded)) and issued it to user a,
    // and update referral_reward_amount= amount(that user a get) for user b
    // when user a verify his id: fetch referral of reward table, and find the referral if it match user a then get the date_recorded of user b and
    // calculate the reward,
    // and check if referral_reward_amount = 0 (0 means not yet issued)
    // for user b, then issue to user a and change referral_reward_amount = amount(that user a get) for user b.
    @Column(name = "who_referral_you", length = 20) //user a
    private String whoReferralYou;

    @Column(name = "referral_reward_amount")
    @Builder.Default
    private double referralRewardIssuedAmount = 0; // for the actual user, i.e user b

    //if b first time to receive payment , i.e first_payment_reward_issued = 0
// when a pay b :
    // if a is verified then
    // (if b verified then issue and change first_payment_reward_issued = 100 (amount that user a will get),
    // sender_first_time_payment to user a in reward table of user b,
    // if b not verified then put a in reward table of b)
    // when b verified then issue to a (means find a from b reward table then issue him
    // and make first_payment_reward_issued = 100 (amount that user a will get) of b),

    //if a is not verified then (put a in reward table of user b)
    // when b is verified then find sender_first_time_payment from b reward table and check first_payment_reward_issued = 0 (amount to user a not yet issued)
    //if a is verified then issue and change sender_first_time_payment and first_payment_reward_issued = 100 (amount that user a will get) of user b table
    // when a is verified find sender_first_time_payment of b and check if b is verified then issue to a
    // and change first_payment_reward_issued = 100 (amount that user a will get) of user b table
    @Column(name = "sender_first_time_payment") //user a
    private String senderFirstTimePayment;

    @Column(name = "first_payment_reward_issued") // working with sender_first_time_payment column
    @Builder.Default
    private double firstPaymentRewardIssued = 0;

    @Column(name = "click_reward_pending")
    private double clickRewardPending;


    //increment +1 each time the user click link tron
    @Column(name = "link_clicks")
    private double linkClicks;
}
