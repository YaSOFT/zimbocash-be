package com.zimbocash.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.zimbocash.enums.TestType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Cacheable;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Cacheable
public class TypesList {
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "test_types_list")
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "varchar(20)", name = "test_type")
    @JsonProperty(value = "test_types_list", access = JsonProperty.Access.WRITE_ONLY)
    List<TestType> testTypes;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

}
