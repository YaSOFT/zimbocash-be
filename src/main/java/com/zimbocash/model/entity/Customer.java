package com.zimbocash.model.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.zimbocash.enums.DocumentType;
import com.zimbocash.enums.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "zc_customers")
@Data
@ToString(exclude = {"customerTransaction", "customerReward", "otps", "customerConfirmation", "review", "telegram", "duplicatedImages", "adminApprovals", "adminComments"})
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Cacheable
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private long id;

    //this column means new user joins in 2021 with new system including hibernate and cache and new logic
    @Column(name = "new_user")
    @Builder.Default
    private boolean newUser = true;

    @Enumerated(EnumType.STRING)
    @Column(name = "document_type")
    @Builder.Default
    private DocumentType documentType = DocumentType.ID_CARD;

    //used for new user when id_failed verification
    @Column(name = "id_failed")
    @Builder.Default
    private boolean idFailed = false;

    //i.e the sender was not yet verified his id and send zash to another user (the sender may be new or old customer)
    @Column(name = "amount_moved_zash_to_you")
    @Builder.Default
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private double movedZASHToYou = 0;

    @Column(name = "terms")
    @Builder.Default
    private boolean termsAccepted = true;

    //main balance for user, updated each time and with tron also
    @Column(name = "balance")
    private double balance;

    @Column(name = "limit_send")
    @Builder.Default
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private double limitSending = 0;

    @Column(name = "limit_receive")
    @Builder.Default
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private double limitReceive = 0;

    @Column(name = "first_name", length = 55)
    private String firstName;

    @Column(name = "last_name", length = 55)
    private String lastName;

    @Column(name = "phone", length = 20, unique = true)
    private String phone;

    @Column(name = "email", unique = true, length = 80)
    private String email;

    @Column(name = "password", columnDefinition = "text")
    @JsonProperty(value = "password", access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Column(name = "date_recorded")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Date dateRecorded;

    @Column(name = "birth_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthDate;

    @Column(name = "member_id", unique = true, length = 30)
    private String memberID;

    @Enumerated(EnumType.STRING)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    @Column(length = 20)
    @Builder.Default
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private UserStatus status = UserStatus.PENDING;

    @Column(name = "phone_verified")
    private boolean phoneVerified;

    @Column(name = "admin")
    private boolean admin;

    @Column(name = "manager")
    private boolean manager;

    //idNumber of user in the IDCARD
    @Column(name = "id_number", length = 80, unique = true)
    private String idNumber;

    @Column(nullable = false)
    @Builder.Default
    //all users can signup and access the system till the system put as scammers and disable it
    private boolean enabled = true;

    @Column(name = "refreshToken", columnDefinition = "text")
    private String refreshToken;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_transaction_id", referencedColumnName = "id")
    private CustomerTransaction customerTransaction;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_reward_id", referencedColumnName = "id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private CustomerReward customerReward;

    @OneToMany(mappedBy = "customer", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonManagedReference
    @Builder.Default
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<OTP> otps = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "comment_customer_id", nullable = false, referencedColumnName = "id")
    @Builder.Default
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private List<AdminComment> adminComments = new ArrayList<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "customer_duplicate_id", nullable = false, referencedColumnName = "id")
    @Builder.Default
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<DuplicatedImage> duplicatedImages = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "customer_conf_id", referencedColumnName = "id")
    private CustomerConfirmation customerConfirmation;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "review_id", referencedColumnName = "id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Review review;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "telegram_id", referencedColumnName = "id")
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Telegram telegram;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "approved_managers")
    @Column(columnDefinition = "varchar(55)", name = "approved_manager")
    @JsonProperty(value = "approved_manager", access = JsonProperty.Access.WRITE_ONLY)
    private Set<String> adminApprovals;
}
