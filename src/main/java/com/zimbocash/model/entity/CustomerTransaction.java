package com.zimbocash.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "zc_customers_tron")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Cacheable
public class CustomerTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private long id;

    @Column(name = "tron_account_address", columnDefinition = "text")
    private String tronAccountAddress;

    @Column(name = "tron_public_key", columnDefinition = "text")
    @JsonProperty(value = "tron_public_key", access = JsonProperty.Access.WRITE_ONLY)
    private String tronPublicKey;

    //todo remove at later stage
    @Column(name = "reserved")
    @Builder.Default
    private boolean reserved = true;
}
