package com.zimbocash.model.entity;

import com.zimbocash.enums.TestAction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "zc_duplicated_image")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Cacheable
public class DuplicatedImage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private long id;

    //the type of uploaded image _front or _liveness
    @Column(name = "type", length = 10)
    private String type;

    @Column(name = "by_who")
    //manager or admin memberID
    private String by_who;

    @Enumerated(EnumType.STRING)
    //manager or admin memberID
    private TestAction action;

    //the memberID of customer in original photo
    @Column(name = "original_id_number", length = 30)
    private String originalIDNumber;
}
