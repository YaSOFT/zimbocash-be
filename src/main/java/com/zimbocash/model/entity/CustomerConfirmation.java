package com.zimbocash.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.zimbocash.enums.TestType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Entity
//confirmation table, working with KYC
@Table(name = "zc_customers_conf")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Cacheable
public class CustomerConfirmation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private long id;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonProperty(value = "words")
    @JoinColumn(name = "CustomerConfirmation_id", referencedColumnName = "id")
    private Set<ConfirmationWord> confirmationWords;

    //key = managerMemberID, value = List(TestType (TEST1|etc....))
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "not_sure")
    @MapKeyColumn(name = "manager_member_id", length = 20)
    @JsonProperty(value = "not_sure", access = JsonProperty.Access.WRITE_ONLY)
    @Builder.Default
    private Map<String, TypesList> notSure = new HashMap<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "declined")
    @MapKeyColumn(name = "manager_member_id", length = 20)
    @JsonProperty(value = "declined", access = JsonProperty.Access.WRITE_ONLY)
    @Builder.Default
    private Map<String, TypesList> decline = new HashMap<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "approved")
    @MapKeyColumn(name = "manager_member_id", length = 20)
    @JsonProperty(value = "approved", access = JsonProperty.Access.WRITE_ONLY)
    @Builder.Default
    private Map<String, TypesList> approved = new HashMap<>();

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "TestFailsMachine")
    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "varchar(20)", name = "type_failed")
    @JsonProperty(value = "TestFailsMachine", access = JsonProperty.Access.WRITE_ONLY)
    private Set<TestType> testFailsMachine;

    //when KYC is ok then true only
    //confirmed false means either it was declined by manager/admin roles or still not yet verified the id
    @Column(name = "confirmed")
    @Builder.Default
    private boolean confirmed = false;

    @Column(name = "eligible_duplication")
    @Builder.Default
    private boolean eligibleForManageDuplication = false;

    @Column(name = "required_to_upload_id")
    @Builder.Default
    private boolean requiredToUploadID = true;

    @Column(name = "required_to_upload_liveness")
    @Builder.Default
    private boolean requiredToUploadLiveness = true;

    @Column(name = "attempts")
    @Builder.Default
    private int attempts = 0;

    @Column(name = "confirmed_by_admin")
    @Builder.Default
    private boolean confirmedByAdmin = false;

    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Date createdDate;
}
