package com.zimbocash.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.zimbocash.enums.TargetType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "zc_otp")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Cacheable
public class OTP {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private long id;

    //the type of otp, either email, phone, telegram
    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private TargetType target;

    @Column(name = "otp")
    private int otp;

    @Column(name = "issue_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date issueDate;

    //one customer can have many otp, either email, phone , telegram
    @ManyToOne
    @JoinColumn(name = "customer_otp_id", nullable = false, referencedColumnName = "id")
    @JsonBackReference
    @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    private Customer customer;
}
