package com.zimbocash.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SocketBodyDto {
    @JsonProperty(required = true, value = "memberID_Email")
    private String memberIDOrEmail;
    @JsonProperty(required = true, value = "Message")
    private String message;
    @JsonProperty(value = "API")
    private String api;
}
