package com.zimbocash.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zimbocash.enums.SortDirection;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomerPaginationDto {
    @NotNull
    private Integer page;
    @NotNull
    private Integer pageSize;
    private String searchTerm;
    private String sortColumn;
    private SortDirection sortDirection;
}
