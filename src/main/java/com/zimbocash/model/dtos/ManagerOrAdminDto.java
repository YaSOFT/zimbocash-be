package com.zimbocash.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Builder
public class ManagerOrAdminDto {
    @NotEmpty
    @Email
    private String email;
    private String firstName;
    private String lastName;
    private String idNumber;
    @Builder.Default
    private Boolean admin = false;
    @Builder.Default
    private Boolean manager = true;
    private String phone;
    @Builder.Default
    private Double balance = 0.0;
    @Builder.Default
    private Boolean approved = true;
}
