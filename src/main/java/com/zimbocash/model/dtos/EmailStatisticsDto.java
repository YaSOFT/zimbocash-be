package com.zimbocash.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zimbocash.model.DateRange;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class EmailStatisticsDto {
    private DateRange date;
    @NotNull
    private Integer count;
}
