package com.zimbocash.model.dtos;

import com.zimbocash.enums.AdminAction;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ApproveOrUnblockDto {
    private String memberId;
    private AdminAction action;
}
