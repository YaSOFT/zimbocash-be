package com.zimbocash.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TransferDto {
    @NotEmpty
    private String memberId;
    @NotNull
    private Double amount;
    @NotNull
    private Boolean showName;
    private String paymentMessage;
}
