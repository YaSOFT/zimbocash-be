package com.zimbocash.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Cacheable;

@Deprecated
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Cacheable
public class TempObject {
    private String privateKey;
    private String oldAddress;
    private String oldPublicKey;
    private String email;
}
