package com.zimbocash.model.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class SmsList {
    @JsonProperty("sms")
    private Sms sms;
    @JsonProperty("error")
    private EconetError error;

    static class Sms {
        @JsonProperty("smsclientid")
        private String smsClientId;
        @JsonProperty("messageid")
        private String messageId;
        @JsonProperty("mobile-no")
        private String mobileNumber;
    }

    static class EconetError {
        @JsonProperty("smsclientid")
        private String smsClientId;
        @JsonProperty("error-code")
        private String errorCode;
        @JsonProperty("error-description")
        private String errorDescription;
        @JsonProperty("error-action")
        private String errorAction;
    }
}
