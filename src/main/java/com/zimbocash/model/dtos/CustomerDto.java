package com.zimbocash.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomerDto {
    @NotEmpty
    @Pattern(regexp = "^\\s*[a-zA-Z]{2,}\\s*[a-zA-Z]{0,}\\s*[a-zA-Z]{0,}\\s*$")
    private String firstName;
    @NotEmpty
    @Pattern(regexp = "^\\s*[a-zA-Z]{2,}\\s*[a-zA-Z]{0,}\\s*[a-zA-Z]{0,}\\s*$")
    private String lastName;
    @NotEmpty
    @Pattern(regexp = "\\d{2}-((\\d{6})|(\\d{7}))([a-zA-Z]{1})-\\d{2}(\\s*)((CIT|REG|ALIEN)*)(\\s*)((?i)(M|F))")
    private String idNumber;
    private String phone;
    @NotEmpty
    @Pattern(regexp = "^(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[012])-((?:19|20)[0-9][0-9])$")
    private String birthDate;
}
