package com.zimbocash.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zimbocash.enums.TestAction;
import com.zimbocash.enums.TestType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Builder
public class CustomerKYCManageDto {
    @NotEmpty
    private String memberID;
    @NotNull
    //keys = idDataToCustomerInfo|facialMatch|livenessMatch|photoshop;
    //values = APPROVE|DECLINE|NOT_SURE
    private Map<TestType, TestAction> testAction;
}
