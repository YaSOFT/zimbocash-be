package com.zimbocash.model.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.zimbocash.util.Constants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class ESolutionMessageDto {
    private String originator;
    private String destination;
    private String messageText;
    private String messageId;
    private String messageReference;
    @Builder.Default
    private String messageDate = Constants.ESOLUTION_SIMPLE_DATE_FORMAT.format(new Date());
    private Status status;
    private long charge;
    private String narrative;
    private boolean scheduled;

    public enum Status {
        FAILED, PENDING, DELIVERED, SENT, UPLOADING_IN_PROGRESS
    }
}
