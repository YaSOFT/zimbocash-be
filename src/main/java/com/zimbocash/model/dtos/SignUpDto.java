package com.zimbocash.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zimbocash.commons.ConditionalOneExist;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ConditionalOneExist(firstSelected = "phone", othersSelected = {"idNumber"})
public class SignUpDto {
    @NotEmpty
    @Email
    private String email;
    @Pattern(regexp = "^(\\+263){1}((6)|(7)|(8)){1}[0-9]{8}$")
    private String phone;
    private String referral;
    @Pattern(regexp = "\\d{2}-((\\d{6})|(\\d{7}))([a-zA-Z]{1})-\\d{2}(\\s*)(\\s*)")
    private String idNumber;
    @NotEmpty
    private String responseCaptcha;
}
