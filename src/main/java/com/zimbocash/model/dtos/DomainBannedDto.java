package com.zimbocash.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class DomainBannedDto {
    private String pattern;
    private boolean ban;
    private boolean word;
}
