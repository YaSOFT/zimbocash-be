package com.zimbocash.model.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class OTPDto {
    @NotEmpty
    @Pattern(regexp = "^(\\+263){1}((6)|(7)|(8)){1}[0-9]{8}$")
    private String phone;
    private Integer otp;
}
