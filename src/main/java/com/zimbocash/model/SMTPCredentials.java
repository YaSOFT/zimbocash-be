package com.zimbocash.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SMTPCredentials extends CustomObjectResponse {
    private String port;
    private String host;
    private String smtpUsername;
    private String smtpPassword;
}
