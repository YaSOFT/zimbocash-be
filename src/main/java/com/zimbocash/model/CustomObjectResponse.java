package com.zimbocash.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "customObjectResponseBuilder")
public class CustomObjectResponse {
    @JsonProperty
    @Builder.Default
    private long timestamp = new Date().getTime();
}
