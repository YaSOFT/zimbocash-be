package com.zimbocash.model;

import com.zimbocash.model.entity.Review;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReviewListRS extends CustomObjectResponse {
    @Builder.Default
    private List<Review> reviews = new ArrayList<>();
    @Builder.Default
    private int total = 0;
    @Builder.Default
    private boolean success = false;
}
