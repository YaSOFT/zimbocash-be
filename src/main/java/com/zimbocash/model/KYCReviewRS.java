package com.zimbocash.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KYCReviewRS extends CustomObjectResponse {

    private byte[] idFrontImage;

    private byte[] idBackImage;

    private byte[] alivenessImage;
}
