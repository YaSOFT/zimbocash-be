package com.zimbocash.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.InputStream;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MultipartBody extends CustomObjectResponse {
    @NotNull
    public InputStream file;
}
