package com.zimbocash.model;

import com.zimbocash.model.entity.Customer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerListRS extends CustomObjectResponse {
    @Builder.Default
    private List<Customer> customers = new ArrayList<>();
    @Builder.Default
    private long total = 0;
    @Builder.Default
    private boolean success = false;
}
