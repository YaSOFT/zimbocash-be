package com.zimbocash.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ErrorResponse {
    @Value("${exception.badRequest}")
    private String summary;
    private String type;
    private String[] details;

    public ErrorResponse(String summary, String... details) {
        super();
        this.summary = summary;
        this.details = details;
    }

}
