package com.zimbocash.commons;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

public class ConditionalOneExistValidator implements ConstraintValidator<ConditionalOneExist, Object> {
    private ConditionalOneExist conditional;

    @Override
    public void initialize(ConditionalOneExist conditional) {
        this.conditional = conditional;
    }


    @Override
    public boolean isValid(Object objectSource, ConstraintValidatorContext context) {

        AtomicBoolean valid = new AtomicBoolean(true);

        BeanWrapperImpl wrapper = new BeanWrapperImpl(objectSource);

        boolean firstExist = !((String) Optional.ofNullable(wrapper.getPropertyValue(conditional.firstSelected().trim())).orElse("")).isEmpty();
        boolean otherExist = Arrays.stream(conditional.othersSelected())
                .filter(s -> wrapper.getPropertyValue(s) != null)
                .filter(s ->
                        !((String) wrapper.getPropertyValue(s)).trim().isEmpty()).anyMatch(s -> true);

        if ((!firstExist && !otherExist) || (firstExist && otherExist)) {
            valid.set(false);
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(conditional.message()).addConstraintViolation();
        }

        return valid.get();
    }

}
