package com.zimbocash.commons;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class CustomResponseDto {
    private String ServiceConnectionKey;
    @Builder.Default
    private boolean Success = false;
    private Integer errorCode;
    private String errorMessage;
    private List<String> InternalAuths;
    private String DetailedMessage;
    private String transactionId;
    @Builder.Default
    private int statusCode = 400;
    private String jwtRefreshToken;
    private String jwtAccessToken;
    @Builder.Default
    private String message = "success";

    public static CustomResponseDto ok() {
        return CustomResponseDto.builder().Success(true).build();
    }
}
