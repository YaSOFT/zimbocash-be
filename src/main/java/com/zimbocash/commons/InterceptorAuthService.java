package com.zimbocash.commons;

import com.zimbocash.config.security.AuthenticationInterceptor;
import com.zimbocash.enums.Roles;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class InterceptorAuthService {
    public static final String cantAccessOtherDetails = "oops! you can't access other user details";

    public static void evaluate(String memberID) {
        if (!memberID.equals(AuthenticationInterceptor.getLoggedInUserMemberId())) {
            if (AuthenticationInterceptor.getRole().equalsIgnoreCase(Roles.USER.getLevel()) || AuthenticationInterceptor.getRole().equalsIgnoreCase
                    (Roles.ANONYMOUS.getLevel())) {
                throw new ResponseStatusException(HttpStatus.NOT_IMPLEMENTED, cantAccessOtherDetails);
            }
        }
    }
}
