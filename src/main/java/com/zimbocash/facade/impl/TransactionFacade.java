package com.zimbocash.facade.impl;

import com.zimbocash.facade.ITransactionFacade;
import com.zimbocash.model.dtos.TransferDto;
import com.zimbocash.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@RequiredArgsConstructor
@Service
public class TransactionFacade implements ITransactionFacade {
    private final TransactionService transactionService;

    @Override
    public Object transfer(TransferDto transferDto, String name) {
        return transactionService.transfer(transferDto, name);
    }

    @Override
    public Object sendOTP(String memberID, String phone) {
        return transactionService.sendOTP(memberID, phone);
    }

    @Override
    public Object verifyOTP(String phone, Integer otp) {
        return transactionService.verifyOTP(phone, otp);
    }

    @Override
    public Object getBalance(String name) {
        return transactionService.getBalance(name);
    }

}
