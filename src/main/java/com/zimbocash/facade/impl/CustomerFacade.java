package com.zimbocash.facade.impl;

import com.zimbocash.external.impl.InternalRestClient;
import com.zimbocash.facade.ICustomerFacade;
import com.zimbocash.facade.ISocketFacade;
import com.zimbocash.model.ZCResponse;
import com.zimbocash.model.dtos.ClickDto;
import com.zimbocash.model.dtos.FeedbackDto;
import com.zimbocash.model.dtos.SignUpDto;
import com.zimbocash.model.dtos.SocketBodyDto;
import com.zimbocash.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Slf4j
@Component
public class CustomerFacade implements ICustomerFacade {
    private final static ZCResponse ZC_RESPONSE_BAD_REQUEST = ZCResponse.builder().build();
    private final CustomerService customerService;
    private final ISocketFacade socketFacade;
    private final InternalRestClient internalRestClient;

    @Override
    public Object signIn(String email, String password) {
        return customerService.signIn(email, password);
    }

    @Override
    public Object signUp(SignUpDto signUpDto) {
//        validate reCaptcha
        if (!internalRestClient.verifyReCaptcha(signUpDto.getResponseCaptcha())) {
            socketFacade.send(SocketBodyDto.builder().api("POST /signUp method").memberIDOrEmail(signUpDto.toString())
                    .message("verifyReCaptcha issue").build());
            return ZC_RESPONSE_BAD_REQUEST;
        }
        return customerService.signUp(signUpDto);
    }

    @Override
    public Object forgetPassword(String email) {
        return customerService.forgetPassword(email);
    }

    @Override
    public Object resetPassword(String email, String password, String newPassword) {
        return customerService.resetPassword(email, password, newPassword);
    }

    @Override
    public Object getCustomerDetailsByMemberID(String memberID) {
        return customerService.getCustomerDetailsByMemberID(memberID);
    }

    @Override
    public Object submitClick(ClickDto clickDto) {
        return customerService.submitClick(clickDto);
    }

    @Override
    public Object updateCustomer(String phone, String memberID) {
        return customerService.updateCustomerAndIdentity(phone, memberID);
    }

    @Override
    public Object receiveClickRewards(String name) {
        return customerService.receiveClickRewards(name);
    }

    @Override
    public Object getTronBalance(String address) {
        return customerService.getTronBalance(address);
    }

    @Override
    public Object checkMemberID(String memberID) {
        return customerService.isMemberIDExist(memberID);
    }

    @Override
    public Object sendFeedback(FeedbackDto feedback, String memberID) {
        return customerService.sendFeedback(memberID, feedback);
    }

    @Override
    public Object refreshToken(String refreshToken, String memberID) {
        return customerService.refreshToken(refreshToken, memberID);
    }

    @Override
    public Object chatbotResult(String message) {
        return customerService.getChatbotResponse(message);
    }

    @Override
    public Object moveWallet(String memberID, String receiverMemberID) {
        return customerService.moveWallet(memberID, receiverMemberID);
    }
}
