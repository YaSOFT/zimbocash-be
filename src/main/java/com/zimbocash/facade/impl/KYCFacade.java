package com.zimbocash.facade.impl;

import com.zimbocash.enums.DocumentType;
import com.zimbocash.facade.IKYCFacade;
import com.zimbocash.model.dtos.CustomerDto;
import com.zimbocash.service.KYCService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class KYCFacade implements IKYCFacade {
    private final KYCService kycService;

    @Override
    public Object updateCustomerKYC(CustomerDto customerDto, String name) {
        return kycService.updateCustomerKYC(customerDto, name);
    }

    @Override
    public Object uploadId(String name, DocumentType documentType) {
        return kycService.uploadId(name, documentType);
    }

    @Override
    public Object uploadLiveness(String name, DocumentType documentType) {
        return kycService.uploadLiveness(name, documentType);
    }

    @Override
    public Object getImageUrl(String name, DocumentType documentType) {
        return kycService.getImageUrl(name, documentType);
    }
}
