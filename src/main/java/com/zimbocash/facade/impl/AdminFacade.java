package com.zimbocash.facade.impl;

import com.zimbocash.commons.CustomResponseDto;
import com.zimbocash.enums.TestAction;
import com.zimbocash.facade.IAdminFacade;
import com.zimbocash.model.CustomerListRS;
import com.zimbocash.model.ZCResponse;
import com.zimbocash.model.dtos.ApproveOrUnblockDto;
import com.zimbocash.model.dtos.CustomerKYCManageDto;
import com.zimbocash.model.dtos.CustomerPaginationDto;
import com.zimbocash.model.dtos.DomainBannedDto;
import com.zimbocash.model.dtos.ManagerOrAdminDto;
import com.zimbocash.model.entity.Customer;
import com.zimbocash.service.AdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;


@RequiredArgsConstructor
@Component
public class AdminFacade implements IAdminFacade, IAdminFacade.KYCAuthentication {
    private final AdminService adminService;

    @Override
    public CustomResponseDto adminUpdateCustomer(Customer customer) {
        return adminService.adminUpdateCustomer(customer);
    }

    @Override
    public Object getCustomerList(CustomerPaginationDto customerPaginationDto) {
        return adminService.getCustomerList(customerPaginationDto);
    }

    @Override
    public Object getReviews() {
        return adminService.getReviews();
    }

    @Override
    public Object getDetails(String memberID) {
        return adminService.getDetails(memberID);
    }

    @Override
    public Object disable(String email, String comment) {
        return adminService.takeActionForUser(email, false, comment);
    }

    @Override
    public Object banOrUnBanDomain(DomainBannedDto domainBannedDto) {
        return adminService.banOrUnBanDomain(domainBannedDto);
    }

    @Override
    public Object enable(String email, String comment) {
        return adminService.takeActionForUser(email, true, comment);
    }

    @Override
    public Object reset(String memberID) {
        return adminService.reset(memberID);
    }

    @Override
    public Object createOrUpdateManagers(ManagerOrAdminDto managerOrAdminDtoList, String memberID) {
        return adminService.createOrUpdateManagers(managerOrAdminDtoList, memberID);
    }

    @Override
    public Object deleteManager(String email) {
        return adminService.disableManager(email);
    }

    @Override
    public Object listAllManagers(int page, int pageSize) {
        return adminService.getAllManagers(page, pageSize);
    }

    @Override
    public Object getBannedDomains() {
        return adminService.getBannedDomains();
    }

    @Override
    public CustomerListRS listBlockedUsers(CustomerPaginationDto customerPaginationDto) {
        return adminService.getBlockedUsers(customerPaginationDto);
    }

    @Override
    public ZCResponse getIdAndSelfieByUserMemberId(String idNumber) {
        return adminService.getIdAndSelfieByUserMemberId(idNumber);
    }

    @Override
    public ZCResponse approveToUnblock(ApproveOrUnblockDto approveOrUnblockDto, String managerMemberID) {
        return adminService.approveToUnblock(approveOrUnblockDto, managerMemberID);
    }

    @Override
    public Object takeActionKYC(CustomerKYCManageDto customerKYCManageDto, String managerMemberID) {
        return adminService.takeActionKYC(customerKYCManageDto, managerMemberID);
    }

    @Override
    public Object getKYCDuplicated(String mangerMemberID) {
        return adminService.getNextKYCDuplicated(mangerMemberID);
    }

    @Override
    public Object updateDuplications(String memberID, long zcdiID, TestAction action, String mangerMemberID) {
        return adminService.updateDuplications(memberID, zcdiID, action, mangerMemberID);
    }

    @Override
    public Object getNext(String mangerMemberID) {
        return adminService.getNext(mangerMemberID);
    }
}
