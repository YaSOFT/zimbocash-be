package com.zimbocash.facade.impl;

import com.zimbocash.facade.IAdminDashboardFacade;
import com.zimbocash.model.DateRange;
import com.zimbocash.model.dtos.EmailStatisticsDto;
import com.zimbocash.service.AnalyticDashboardService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class AdminDashboardFacade implements IAdminDashboardFacade {
    private final AnalyticDashboardService analyticDashboardService;

    @Override
    public Object getAccountTypeStatistics() {
        return analyticDashboardService.getAccountTypeMap();
    }

    @Override
    public Object getOtpStatistics() {
        return analyticDashboardService.getOtpTypeMap();
    }

    @Override
    public Object getUserVerificationStatistics(DateRange dateRange) {
        return analyticDashboardService.getUserVerifiedTypeMap(dateRange);
    }

    @Override
    public Object getTransactionStatistics() {
        return analyticDashboardService.getTransactionCountMap();
    }

    @Override
    public Object getLoginStatistics(DateRange dateRange) {
        return analyticDashboardService.getLoginAuditMap(dateRange);
    }

    @Override
    public ResponseEntity getCsvForVerifiedEmails() {
        return analyticDashboardService.getCsv();
    }

    @Override
    public Object getManualReviewerStatistics() {
        return analyticDashboardService.getManualReviewerStatisticMap();
    }

    @Override
    public Object getEmailStatistics(EmailStatisticsDto emailStatistics) {
        return analyticDashboardService.getEmailWithCountMap(emailStatistics);
    }
}
