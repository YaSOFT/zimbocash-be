package com.zimbocash.facade.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.zimbocash.commons.CustomResponseDto;
import com.zimbocash.facade.ITelegramFacade;
import com.zimbocash.service.TelegramService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class TelegramFacade implements ITelegramFacade {
    private final TelegramService telegramService;

    @Override
    public CustomResponseDto requestTelegram(JsonNode jsonNode) {
        return telegramService.requestTelegram(jsonNode);
    }

    @Override
    public Object sendTelegramOTP(String name) {
        return telegramService.sendTelegramOTP(name);
    }

    @Override
    public Object verifyTelegramOTP(String otp, String name) {
        return telegramService.verifyTelegramOTP(otp, name);
    }

    @Override
    public Object disableTelegram(String name) {
        return telegramService.disableTelegram(name);
    }
}
