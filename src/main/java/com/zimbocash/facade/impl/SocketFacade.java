package com.zimbocash.facade.impl;

import com.zimbocash.facade.ISocketFacade;
import com.zimbocash.model.dtos.SocketBodyDto;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SocketFacade implements ISocketFacade {
    private final SimpMessagingTemplate simpMessagingTemplate;

    @Override
    public void send(SocketBodyDto socketBodyDto) {
        simpMessagingTemplate.convertAndSend("/topic/messages", socketBodyDto);
    }
}
