package com.zimbocash.facade;

import com.zimbocash.model.dtos.SocketBodyDto;

@FunctionalInterface
public interface ISocketFacade {
    void send(SocketBodyDto socketBody);
}
