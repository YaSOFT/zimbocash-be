package com.zimbocash.facade;

import com.fasterxml.jackson.databind.JsonNode;
import com.zimbocash.commons.CustomResponseDto;

public interface ITelegramFacade {
    CustomResponseDto requestTelegram(JsonNode jsonNode);

    Object sendTelegramOTP(String name);

    Object verifyTelegramOTP(String otp, String name);

    Object disableTelegram(String name);
}
