package com.zimbocash.facade;

import com.zimbocash.model.dtos.TransferDto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public interface ITransactionFacade {
    Object transfer(TransferDto transferDto, String name);

    Object sendOTP(String memberID, @NotEmpty @Pattern(regexp = "^(\\+263){1}((6)|(7)|(8)){1}[0-9]{8}$") String phone);

    Object verifyOTP(@NotEmpty @Pattern(regexp = "^(\\+263){1}((6)|(7)|(8)){1}[0-9]{8}$") String phone, @NotNull Integer otp);

    Object getBalance(String name);
}
