package com.zimbocash.facade;

import com.zimbocash.enums.Stage;
import com.zimbocash.model.dtos.ClickDto;
import com.zimbocash.model.dtos.FeedbackDto;
import com.zimbocash.model.dtos.SignUpDto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public interface ICustomerFacade {
    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    static double getSignUpReward(Date date) {
        switch (getStage(date)) {
            case ZERO:
                return 400;
            case ONE:
                return 100000;
            case TWO:
                return 50000;
            case THREE:
                return 25000;
            case FOUR:
                return 12500;
            case FIVE:
                return 6250;
            case SIX:
                return 3125;
            case SEVEN:
                return 1562.5;
            case EIGHT:
                return 781.25;
            case NINE:
                return 400;
            default:
                return 0;
        }
    }

    static Stage getStage(Date date) {
        if (date == null) {
            date = new Date();
        }
        try {
            if (date.before(formatter.parse("2019-04-01"))) {
                return Stage.ONE;
            } else if (date.before(formatter.parse("2019-07-01"))) {
                return Stage.TWO;
            } else if (date.before(formatter.parse("2019-10-01"))) {
                return Stage.THREE;
            } else if (date.before(formatter.parse("2020-01-01"))) {
                return Stage.FOUR;
            } else if (date.before(formatter.parse("2020-04-01"))) {
                return Stage.FIVE;
            } else if (date.before(formatter.parse("2020-07-01"))) {
                return Stage.SIX;
            } else if (date.before(formatter.parse("2020-10-01"))) {
                return Stage.SEVEN;
            } else if (date.before(formatter.parse("2021-01-01"))) {
                return Stage.EIGHT;
            } else if (date.after(formatter.parse("2020-12-31"))) {
                return Stage.NINE;
            }
        } catch (ParseException ignored) {
        }
        return Stage.ZERO;
    }

    Object signIn(@Email @NotEmpty String email, @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-zA-Z])(.*?).{12,}$") @NotEmpty String password);

    Object signUp(SignUpDto signUpDto);

    Object submitClick(ClickDto clickDto);

    Object updateCustomer(@NotEmpty @Pattern(regexp = "^(\\+263){1}((6)|(7)|(8)){1}[0-9]{8}$") String phone, String name);

    Object receiveClickRewards(String name);

    Object getTronBalance(String address);

    Object checkMemberID(String memberID);

    Object sendFeedback(FeedbackDto feedback, String memberID);

    Object refreshToken(String refreshToken, String memberID);

    Object chatbotResult(String message);

    Object getCustomerDetailsByMemberID(String memberID);

    Object resetPassword(@Email @NotEmpty String email,
                         @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-zA-Z])(.*?).{12,}$") @NotEmpty String password,
                         @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-zA-Z])(.*?).{12,}$") @NotEmpty String newPassword);


    Object forgetPassword(@Email @NotEmpty String email);

    Object moveWallet(String memberID, String receiverMemberID);
}
