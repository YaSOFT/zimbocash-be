package com.zimbocash.facade;

import com.zimbocash.model.DateRange;
import com.zimbocash.model.dtos.EmailStatisticsDto;
import org.springframework.http.ResponseEntity;

public interface IAdminDashboardFacade {
    Object getAccountTypeStatistics();

    Object getOtpStatistics();

    Object getUserVerificationStatistics(DateRange dateRange);

    Object getTransactionStatistics();

    Object getLoginStatistics(DateRange dateRange);

    ResponseEntity getCsvForVerifiedEmails();

    Object getManualReviewerStatistics();

    Object getEmailStatistics(EmailStatisticsDto emailStatistics);
}
