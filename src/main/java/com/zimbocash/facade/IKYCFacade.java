package com.zimbocash.facade;

import com.zimbocash.enums.DocumentType;
import com.zimbocash.model.dtos.CustomerDto;

public interface IKYCFacade {
    Object updateCustomerKYC(CustomerDto customerDto, String name);

    Object uploadId(String name, DocumentType documentType);

    Object uploadLiveness(String name, DocumentType documentType);

    Object getImageUrl(String name, DocumentType documentType);
}
