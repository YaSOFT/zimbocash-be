package com.zimbocash.facade;

import com.zimbocash.commons.CustomResponseDto;
import com.zimbocash.enums.TestAction;
import com.zimbocash.model.CustomerListRS;
import com.zimbocash.model.ZCResponse;
import com.zimbocash.model.dtos.ApproveOrUnblockDto;
import com.zimbocash.model.dtos.CustomerKYCManageDto;
import com.zimbocash.model.dtos.CustomerPaginationDto;
import com.zimbocash.model.dtos.DomainBannedDto;
import com.zimbocash.model.dtos.ManagerOrAdminDto;
import com.zimbocash.model.entity.Customer;

public interface IAdminFacade {
    CustomResponseDto adminUpdateCustomer(Customer customer);

    Object getCustomerList(CustomerPaginationDto query);

    Object getReviews();

    Object getDetails(String memberID);

    Object disable(String email, String comment);

    Object banOrUnBanDomain(DomainBannedDto domainBannedDto);

    Object enable(String email, String comment);

    Object reset(String memberID);

    Object createOrUpdateManagers(ManagerOrAdminDto managerOrAdminDtoList, String memberID);

    Object deleteManager(String email);

    Object listAllManagers(int page, int pageSize);

    CustomerListRS listBlockedUsers(CustomerPaginationDto query);

    ZCResponse getIdAndSelfieByUserMemberId(String idNumber);

    ZCResponse approveToUnblock(ApproveOrUnblockDto approveOrUnblockDto, String memberId);

    Object getBannedDomains();

    interface KYCAuthentication {
        Object getNext(String mangerMemberID);

        Object takeActionKYC(CustomerKYCManageDto customerKYCManageDto, String mangerMemberID);

        Object getKYCDuplicated(String mangerMemberID);

        Object updateDuplications(String memberID, long duplicatedMemberID, TestAction action, String mangerMemberID);
    }
}
